package com.teknei.bid.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.teknei.bid.R;
import com.teknei.bid.activities.ShowContractActivity;

/**
 * Created by rgarciav on 24/01/2018.
 */

public class GetContractDialog extends Dialog implements View.OnClickListener {

    private final String CLASS_NAME = "GetContractDialog";

    Button okButton;
    Button retryButton;
    Button cancelButton;

    TextView txvTitle;
    TextView txvMessage;
    Activity activityOrigin;

    String titleIn;
    String menssageIn;

    int actionIn;

    public GetContractDialog(Activity context, String title, String message, int action) {
        super(context);

        activityOrigin = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.alert_dialog);

        this.titleIn = title;
        this.menssageIn = message;
        this.actionIn = action;

        txvTitle = (TextView) findViewById(R.id.alert_title);
        txvMessage = (TextView) findViewById(R.id.alert_message);


        okButton = (Button) findViewById(R.id.ok_buttom);
        okButton.setOnClickListener(this);

        retryButton = (Button) findViewById(R.id.retry_buttom);
        retryButton.setOnClickListener(this);

        cancelButton = (Button) findViewById(R.id.cancel_buttom);
        cancelButton.setOnClickListener(this);

        txvTitle.setText(titleIn);
        txvMessage.setText(menssageIn);

        okButton.setVisibility(View.GONE);
        retryButton.setVisibility(View.VISIBLE);
        cancelButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        dismiss();

        switch (v.getId()) {
            case R.id.retry_buttom:
            case R.id.ok_buttom:
                ((ShowContractActivity) activityOrigin).getContractShow();
                break;

            case R.id.cancel_buttom:
                break;
        }
    }
}
