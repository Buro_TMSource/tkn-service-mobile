package com.teknei.bid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.identy.AppUtils;
import com.identy.IdentySdk;
import com.identy.WSQCompression;
import com.identy.encryption.FileCodecBase64;
import com.identy.enums.Finger;
import com.identy.enums.FingerDetectionMode;
import com.identy.enums.Hand;
import com.identy.enums.Template;
import com.teknei.bid.R;
import com.teknei.bid.asynctask.FingersSend;
import com.teknei.bid.dialogs.DialogConfirmCaptureFinger;
import com.teknei.bid.services.CipherFingerServices;
import com.teknei.bid.tools.Base64;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.MissingFingerUtils;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FingerIDKaralundiActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private static final String TAG = "FingerIDKaralundi";

    private FingerDetectionMode[] detectionModes;

    private String operationID;
    private String idEnterprice;
    private String customerType;

    protected String  WSQfileName    = "ImageCaptured";
    protected boolean flagIndexLeft  = false;
    protected boolean flagIndexRight = false;

    boolean enableSpoofCheck        = true;
    boolean displayboxes            = true;
    boolean displayFingerDrawable   = false;
    boolean nfiqCheck               = false;

    String mode = "demo";
    WSQCompression compression = WSQCompression.WSQ_5_1;

    //Left Hand
    File imageFilePinkyLeft;
    File imageFileRingLeft;
    File imageFileMiddleLeft;
    File imageFileIndexLeft;
    File imageFileThumbLeft;
    //Right Hand
    File imageFilePinkyRight;
    File imageFileRingRight;
    File imageFileMiddleRight;
    File imageFileIndexRight;
    File imageFileThumbRight;

    File   fileImage;

    //Left Hand
    String base64PinkyLeft;
    String base64RingLeft;
    String base64MiddleLeft;
    String base64IndexLeft;
    String base64ThumbLeft;
    //Right Hand
    String base64PinkyRight;
    String base64RingRight;
    String base64MiddleRight;
    String base64IndexRight;
    String base64ThumbRight;

    //Uso para MSOShower
    List<File> fingersFileArray = null;

    File fileJson;
    List<File> fileList;

    private Button btnContinue;
    private Button btnTakeFingers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_idkaralundi);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ico_bid);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            invalidateOptionsMenu();
        }

        operationID = SharedPreferencesUtils.readFromPreferencesString(
                FingerIDKaralundiActivity.this, SharedPreferencesUtils.OPERATION_ID, "");

        idEnterprice = SharedPreferencesUtils.readFromPreferencesString(
                FingerIDKaralundiActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, "");

        customerType = SharedPreferencesUtils.readFromPreferencesString(
                FingerIDKaralundiActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, "");

        btnTakeFingers = (Button) findViewById(R.id.fi_b_capture_fingerprint);
        btnTakeFingers.setOnClickListener(this);

        btnContinue    = (Button) findViewById(R.id.fi_b_continue);
        btnContinue.setOnClickListener(this);

        detectionModes = new FingerDetectionMode[]{FingerDetectionMode.L4F, FingerDetectionMode.R4F};

        _reinitUI();

        fileList = new ArrayList<File>();

    }

    @Override
    public void onClick(View view) {

        updateIntent();

        switch (view.getId()) {

            case R.id.fi_b_capture_fingerprint:

                try {
                    final ArrayList<Template> requiredtemplates = new ArrayList<>();

                    requiredtemplates.add(Template.WSQ);
                    requiredtemplates.add(Template.RAW);
                    requiredtemplates.add(Template.PNG);
                    requiredtemplates.add(Template.ISO_19794_4);
                    requiredtemplates.add(Template.ISO_19794_2);

                    IdentySdk.newInstance(FingerIDKaralundiActivity.this, "com.teknei.bid2019-01-24.lic", new IdentySdk.InitializationListener<IdentySdk>() {
                        @Override
                        public void onInit(IdentySdk d) {
                            try {
                                ArrayList<Finger> qc = new ArrayList<Finger>();
                                qc.add(Finger.THUMB);
                                qc.add(Finger.INDEX);
                                qc.add(Finger.MIDDLE);
                                qc.add(Finger.RING);
                                qc.add(Finger.LITTLE);

                                d.set4FintroShow(false);
                                d.setThumbIntroShow(true);

                                d.setDisplayImages(mode.equals("demo"))
                                        //.setDisplayImages(false).setFingerPrintDrawable(R.drawable.printColor128, displayFingerDrawable)
                                        //.setDisplayImages(false).setFingerPrintDrawable(R.drawable.printcolor128, true)
                                        .setMode(mode)
                                        //.setEncryption(IdentyEncrytion.AES,"car")
                                        .setAS(enableSpoofCheck)
                                        .setDetectionMode(detectionModes)
                                        .setRequiredTemplates(requiredtemplates)
                                        .displayImages(mode.equals("demo"))
                                        .setDisplayBoxes(displayboxes)
                                        .setWSQCompression(compression)
                                        //.setEncryption(new IdentyEncrytion(),"bla")
                                        .setCalculateNFIQ(true)
                                        .qcList(qc)
                                        .capture();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, previewListener);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.fi_b_continue:

                if (validateIndexFingers()) {

                    DialogConfirmCaptureFinger dialogConfirm = new DialogConfirmCaptureFinger(this, getString(R.string.dccf_title), getString(R.string.dccf_msg_confirm_capture));
                    dialogConfirm.setCancelable(false);
                    dialogConfirm.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogConfirm.show();

                }
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.gc();
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean validateIndexFingers() {
        boolean bitMapTake;

        if ( imageFileIndexRight==null || imageFileIndexLeft==null) {

            bitMapTake = false;
            Toast.makeText(FingerIDKaralundiActivity.this, "Debe capturar ambos dedos indices", Toast.LENGTH_SHORT).show();

        } else {

            bitMapTake = true;

        }

        return bitMapTake;
    }

    public String buildJSON() {

        //Construimos el JSON
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emprId", idEnterprice);
            jsonObject.put("customerType", customerType);
            jsonObject.put("operationId", operationID+"");
            jsonObject.put("contentType", "image/wsq");

            jsonObject = addBase64Fingers(jsonObject);

            if (imageFileThumbLeft != null) {
                jsonObject.put("dedo1I", true);
                //fingersFileArray.add(imageFileThumbLeft);
            } else {
                jsonObject.put("dedo1I", false);
            }

            if (imageFileThumbRight != null) {
                jsonObject.put("dedo1D", true);
                //fingersFileArray.add(imageFileThumbRight);
            } else {
                jsonObject.put("dedo1D", false);
            }

            if (imageFileIndexLeft != null) {
                jsonObject.put("dedo2I", true);
                //fingersFileArray.add(imageFileIndexLeft);
            } else {
                jsonObject.put("dedo2I", false);
            }

            if (imageFileIndexRight != null) {
                jsonObject.put("dedo2D", true);
                //fingersFileArray.add(imageFileIndexRight);
            } else {
                jsonObject.put("dedo2D", false);
            }

            if (imageFileMiddleLeft != null) {
                jsonObject.put("dedo3I", true);
                //fingersFileArray.add(imageFileMiddleLeft);
            } else {
                jsonObject.put("dedo3I", false);
            }

            if (imageFileMiddleRight != null) {
                jsonObject.put("dedo3D", true);
                //fingersFileArray.add(imageFileMiddleRight);
            } else {
                jsonObject.put("dedo3D", false);
            }

            if (imageFileRingLeft != null) {
                jsonObject.put("dedo4I", true);
                //fingersFileArray.add(imageFileRingLeft);
            } else {
                jsonObject.put("dedo4I", false);
            }

            if (imageFileRingRight != null) {
                jsonObject.put("dedo4D", true);
                //fingersFileArray.add(imageFileRingRight);
            } else {
                jsonObject.put("dedo4D", false);
            }

            if (imageFilePinkyLeft != null) {
                jsonObject.put("dedo5I", true);
                //fingersFileArray.add(imageFilePinkyLeft);
            } else {
                jsonObject.put("dedo5I", false);
            }

            if (imageFilePinkyRight != null) {
                jsonObject.put("dedo5D", true);
                //fingersFileArray.add(imageFilePinkyRight);
            } else {
                jsonObject.put("dedo5D", false);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Writer output;
            fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "fingers" + ".json");
            if (fileJson.exists()) {
                fileJson.delete();
                fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "fingers" + ".json");
            }
            Log.d ("FINGER WATSON ",jsonObject.toString());
            output = new BufferedWriter(new FileWriter(fileJson));
            output.write(jsonObject.toString());
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private JSONObject addBase64Fingers(JSONObject jsonObject) {

        if (base64PinkyLeft != null && !base64PinkyLeft.equals("")) {
            try {
                jsonObject.put("ll", base64PinkyLeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64RingLeft != null && !base64RingLeft.equals("")) {
            try {
                jsonObject.put("lr", base64RingLeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64MiddleLeft != null && !base64MiddleLeft.equals("")) {
            try {
                jsonObject.put("lm", base64MiddleLeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64IndexLeft != null && !base64IndexLeft.equals("")) {
            try {
                jsonObject.put("li", base64IndexLeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64ThumbLeft != null && !base64ThumbLeft.equals("")) {
            try {
                jsonObject.put("lt", base64ThumbLeft);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //Right arm
        if (base64PinkyRight != null && !base64PinkyRight.equals("")) {
            try {
                jsonObject.put("rl", base64PinkyRight);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64RingRight != null && !base64RingRight.equals("")) {
            try {
                jsonObject.put("rr", base64RingRight);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64MiddleRight != null && !base64MiddleRight.equals("")) {
            try {
                jsonObject.put("rm", base64MiddleRight);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64IndexRight != null && !base64IndexRight.equals("")) {
            try {
                jsonObject.put("ri", base64IndexRight);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (base64ThumbRight != null && !base64ThumbRight.equals("")) {
            try {
                jsonObject.put("rt", base64ThumbRight);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    public void sendPetition() {
        String token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");
        String fingerOperation = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.FINGERS_OPERATION, "");

        if (fingerOperation.equals("")) {
            fileList.clear();
            String localTime = PhoneSimUtils.getLocalDateAndTime();

            String jsonString = buildJSON();

            fileList.add(fileJson);

            if (imageFileIndexLeft != null) {
                fileList.add(imageFileIndexLeft);
            }

            if (imageFileIndexRight != null) {
                fileList.add(imageFileIndexRight);
            }

            Log.d("ArrayList Files", "Files:" + fileList.size());
            new FingersSend(FingerIDKaralundiActivity.this, token, jsonString, fileList, ApiConstants.TYPE_ACT_BASIC).execute();

        } else {
            goNext();
        }
    }

    public void deleteFile () {

        if (fileJson != null && fileJson.exists()) {
            fileJson.delete();
        }

        if (imageFilePinkyLeft != null && imageFilePinkyLeft.exists()) {
            imageFilePinkyLeft.delete();
        }

        if (imageFileRingLeft != null && imageFileRingLeft.exists()) {
            imageFileRingLeft.delete();
        }

        if (imageFileMiddleLeft != null && imageFileMiddleLeft.exists()) {
            imageFileMiddleLeft.delete();
        }

        if (imageFileIndexLeft != null && imageFileIndexLeft.exists()) {
            imageFileIndexLeft.delete();
        }

        if (imageFileThumbLeft != null && imageFileThumbLeft.exists()) {
            imageFileThumbLeft.delete();
        }

        if (imageFilePinkyRight != null && imageFilePinkyRight.exists()) {
            imageFilePinkyRight.delete();
        }

        if (imageFileRingRight != null && imageFileRingRight.exists()) {
            imageFileRingRight.delete();
        }

        if (imageFileMiddleRight != null && imageFileMiddleRight.exists()) {
            imageFileMiddleRight.delete();
        }

        if (imageFileIndexRight != null && imageFileIndexRight.exists()) {
            imageFileIndexRight.delete();
        }

        if (imageFileThumbRight != null && imageFileThumbRight.exists()) {
            imageFileThumbRight.delete();
        }

        if (fileImage != null && fileImage.exists()) {
            fileImage.delete();
        }
    }

    public void goNext() {
        deleteFile();

        Intent i = new Intent(FingerIDKaralundiActivity.this, FaceEnrollKaralundiActivity.class);
        startActivity(i);
        finish();
    }

    private void _reinitUI() {

        if (!MissingFingerUtils.getLeftMissingFingers(FingerIDKaralundiActivity.this).isEmpty() && MissingFingerUtils.getLeftMissingFingers(FingerIDKaralundiActivity.this).size() < 4) {
            ((CheckBox) findViewById(R.id.radLeftFour)).setText(Html.fromHtml("Izquierda <b> " + MissingFingerUtils.getLeftMissingFingers(FingerIDKaralundiActivity.this).size() + " </b>dedos"));// );
        } else {
            ((CheckBox) findViewById(R.id.radLeftFour)).setText("Izquierda 4 dedos");
        }

        if (!MissingFingerUtils.getRightMissingFingers(FingerIDKaralundiActivity.this).isEmpty() && MissingFingerUtils.getRightMissingFingers(FingerIDKaralundiActivity.this).size() < 4) {
            ((CheckBox) findViewById(R.id.radRightFourFingers)).setText(Html.fromHtml("Derecha <b> " + MissingFingerUtils.getRightMissingFingers(FingerIDKaralundiActivity.this).size() + "</b>dedos"));
        } else {
            ((CheckBox) findViewById(R.id.radRightFourFingers)).setText("Derecha 4 dedos");
        }

    }

    private boolean isRight4FSelected() {
        return isSelected((CheckBox) findViewById(R.id.radRightFourFingers));
    }

    private boolean isLeft4FSelected() {
        return isSelected((CheckBox) findViewById(R.id.radLeftFour));
    }

    private boolean isRightThumbSelected() {
        return isSelected((CheckBox) findViewById(R.id.radRightThumb));
    }

    private boolean isLeftThumbSelected() {
        return isSelected((CheckBox) findViewById(R.id.radLeftThumb));
    }

    private boolean isSelected(CheckBox box) {
        return box.isChecked();
    }

    private void updateIntent() {
        ArrayList<FingerDetectionMode> modes = new ArrayList<>();

        if (isLeft4FSelected()) {

            List<FingerDetectionMode> fingers = MissingFingerUtils.getLeftMissingFingers(FingerIDKaralundiActivity.this);
            if (fingers.isEmpty() || fingers.size() == 4) {
                modes.add(FingerDetectionMode.L4F);
            } else {
                modes.addAll(fingers);
            }
        }

        if (isRight4FSelected()) {
            List<FingerDetectionMode> fingers = MissingFingerUtils.getRightMissingFingers(FingerIDKaralundiActivity.this);
            if (fingers.isEmpty() || fingers.size() == 4) {
                modes.add(FingerDetectionMode.R4F);
            } else {
                modes.addAll(fingers);
            }
        }

        if (isLeftThumbSelected()) {
            modes.add(FingerDetectionMode.LEFT_THUMB);
        }

        if (isRightThumbSelected()) {
            modes.add(FingerDetectionMode.RIGHT_THUMB);
        }

        detectionModes = modes.toArray(new FingerDetectionMode[modes.size()]);
    }

    IdentySdk.IdentyResponseListener previewListener = new IdentySdk.IdentyResponseListener() {

        @Override
        public void onResponse(IdentySdk.IdentyResponse response) {

            Log.e(TAG, "onResponse");

            JSONObject jo = response.toJson();
            Log.e(TAG, "Entro a enroll Listener preview");

            String finger = "";
            int    fingerSelect = 0;

            try {
                FileOutputStream o = new FileOutputStream(AppUtils.createExternalDirectory("json_output").toString() + "/preview_latest_output"+System.currentTimeMillis()+".txt");
                o.write(jo.toString().getBytes());
                o.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (Map.Entry<Pair<Hand, Finger>, IdentySdk.FingerOutput> o : response.getPrints().entrySet()) {

                Pair<Hand, Finger> handFinger = o.getKey();

                IdentySdk.FingerOutput fingerOutput = o.getValue();

                fileImage = new File(ApiConstants.FOLDER_IMGE + File.separator + WSQfileName + ".wsq");

                if (fileImage.exists()) {
                    fileImage.delete();
                    fileImage = new File(ApiConstants.FOLDER_IMGE + File.separator + WSQfileName + ".wsq");
                }

                try {

                    Log.e("MainActivity", "File: " + fileImage.getName());

                    FileOutputStream outPut = new FileOutputStream(fileImage);

                    String base64Str = fingerOutput.getTemplates().get(Template.WSQ);

                    Log.e("FingerIDKaralundi", "StringImage: " + base64Str);

                    outPut.write(FileCodecBase64.decode(base64Str));

                    outPut.close();

                    if (handFinger.first.toString().equals("left")) {

                        if (handFinger.second.toString().equals("thumb")) {
                            finger = "I1";
                            fingerSelect = 6;
                            base64ThumbLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("index")) {
                            flagIndexLeft = true;
                            finger = "I2";
                            fingerSelect = 7;
                            base64IndexLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("middle")) {
                            finger = "I3";
                            fingerSelect = 8;
                            base64MiddleLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("ring")) {
                            finger = "I4";
                            fingerSelect = 9;
                            base64RingLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("little")) {
                            finger = "I5";
                            fingerSelect = 10;
                            base64PinkyLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));
                        }

                    } else {

                        if (handFinger.second.toString().equals("thumb")) {
                            finger = "D1";
                            fingerSelect = 1;
                            base64ThumbRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("index")) {
                            flagIndexRight = true;
                            finger = "D2";
                            fingerSelect = 2;
                            base64IndexRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("middle")) {
                            finger = "D3";
                            fingerSelect = 3;
                            base64MiddleRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("ring")) {
                            finger = "D4";
                            fingerSelect = 4;
                            base64RingRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("little")) {
                            finger = "D5";
                            fingerSelect = 5;
                            base64PinkyRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                File file = new File(ApiConstants.FOLDER_IMGE + File.separator + "finger_" + finger + "_" + operationID + ".jpg");

                if (file.exists()) {
                    file.delete();
                    file = new File(ApiConstants.FOLDER_IMGE + File.separator + "finger_" + finger + "_" + operationID + ".jpg");
                }

                try {

                    FileOutputStream outPut = new FileOutputStream(file);

                    String base64Str = fingerOutput.getTemplates().get(Template.WSQ);

                    Log.e("MainActivity", "StringImage2: " + base64Str);

                    outPut.write(FileCodecBase64.decode(base64Str));

                    outPut.close();

                    if (handFinger.first.toString().equals("left")) {

                        if (handFinger.second.toString().equals("thumb")) {
                            imageFileThumbLeft  = file;

                        } else if (handFinger.second.toString().equals("index")) {
                            imageFileIndexLeft  = file;

                        } else if (handFinger.second.toString().equals("ring")) {
                            imageFileRingLeft   = file;

                        } else if (handFinger.second.toString().equals("middle")) {
                            imageFileMiddleLeft = file;

                        } else if (handFinger.second.toString().equals("little")) {
                            imageFilePinkyLeft  = file;

                        }

                    } else {

                        if (handFinger.second.toString().equals("thumb")) {
                            imageFileThumbRight  = file;

                        } else if (handFinger.second.toString().equals("index")) {
                            imageFileIndexRight  = file;

                        } else if (handFinger.second.toString().equals("ring")) {
                            imageFileRingRight   = file;

                        } else if (handFinger.second.toString().equals("middle")) {
                            imageFileMiddleRight = file;

                        } else if (handFinger.second.toString().equals("little")) {
                            imageFilePinkyRight  = file;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onErrorResponse(IdentySdk.IdentyError error) {

            Toast.makeText(getApplicationContext(), "No se encontro componente", Toast.LENGTH_LONG).show();

        }
    };

    void saveBooleanState(String key, boolean obj) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, obj);
        editor.commit();
    }

    void saveStringState(String key, String obj) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, obj);
        editor.commit();
    }

}
