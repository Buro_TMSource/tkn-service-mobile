package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rgarciav on 20/03/2018.
 */

public class DetailSearchDTO {

    @SerializedName("apellidoPaterno")
    private String apellidoPaterno;

    @SerializedName("fecha")
    private String fecha;

    @SerializedName("curpIdentificado")
    private String curpIdentificado;

    @SerializedName("id")
    private Integer id;

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("email")
    private String email;

    @SerializedName("curp")
    private String curp;

    @SerializedName("tels")
    private List<PhoneDTO> tels = null;

    @SerializedName("apellidoMaterno")
    private String apellidoMaterno;

    @SerializedName("curpCapturado")
    private String curpCapturado;

    @SerializedName("status")
    private String status;

    @SerializedName("curpDocument")
    private String curpDocument;

    @SerializedName("idType")
    private List<Integer> idType;

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCurpIdentificado() {
        return curpIdentificado;
    }

    public void setCurpIdentificado(String curpIdentificado) {
        this.curpIdentificado = curpIdentificado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public List<PhoneDTO> getTels() {
        return tels;
    }

    public void setTels(List<PhoneDTO> tels) {
        this.tels = tels;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCurpCapturado() {
        return curpCapturado;
    }

    public void setCurpCapturado(String curpCapturado) {
        this.curpCapturado = curpCapturado;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurpDocument() {
        return curpDocument;
    }

    public void setCurpDocument(String curpDocument) {
        this.curpDocument = curpDocument;
    }

    public List<Integer> getIdType() {
        return idType;
    }

    public void setIdType(List<Integer> idType) {
        this.idType = idType;
    }

}
