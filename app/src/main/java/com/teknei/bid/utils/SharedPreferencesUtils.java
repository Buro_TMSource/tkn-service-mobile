package com.teknei.bid.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

import com.teknei.bid.activities.IcarScanActivity;

import java.io.File;


public class SharedPreferencesUtils {
    public static final String ID_DEVICE            = "id_device";
    public static final String PREF_FILE_NAME       = "pref_teknei";
    public static final String TOKEN_APP            = "token_app";
    public static final String USERNAME             = "username";
    public static final String ID_CLIENT            = "clientId";
    public static final String CLIENT_CURP          = "clientCurp";

    public static final String TYPE_PERSON          = "type_person";
    public static final String TYPE_ID              = "type_id";

    public static final String OPERATION_ID         = "operation_id";

    public static final String INIT_OPE_FORM        = "init_ope_form";
    public static final String SCAN_SAVE_ID         = "id_scan_save_val";
    public static final String SCAN_SAVE_ID_ADD     = "id_add_scan_save";
    public static final String FACE_OPERATION       = "face_operation";
    public static final String FINGERS_OPERATION    = "fingers_operation";
    public static final String ID_SCAN              = "id_scan_val";
    public static final String DOCUMENT_OPERATION   = "document_operation";
    public static final String BANK_ACCOUNT         = "bank_account";
    public static final String CERT_PASSWORD        = "cert_password";
    public static final String OTP_VALIDATION       = "otp_validation";
    public static final String ACCEPT_CLAUSES       = "accept_clauses";
    public static final String SHOW_CONTRACT        = "show_contract";
    public static final String SIGN_CONTRACT        = "sign_contract";

    public static final String OPE_ACCOUNT          = "ope_account";
    public static final String PAY_OPERATION        = "pay_operation";


    public static final String URL_ID_SCAN          = "url_id_scan_settings";
    public static final String LICENSE_ID_SCAN      = "license_id_scan_settings";
    public static final String URL_TEKNEI           = "url_teknei_settings";

    //MOBBSIGN
    public static final String URL_MOBBSIGN         = "url_mobbsign";
    public static final String MOBBSIGN_LICENSE     = "mobbsign_license";

    //MOBBSIGN
    public static final String URL_AUTHACCESS       = "url_authaccess";

    //JSON de Formulario inicial
    public static final String JSON_INIT_FORM       = "json_init_form";

    //JSON de respuesta MobbScan frontal y trasera
    public static final String JSON_CREDENTIALS_RESPONSE = "json_mabbscan_front";

    //TimeStamp para tiempos de guardado
    public static final String TIMESTAMP_CREDENTIALS = "timestamp_credential";
    public static final String TIMESTAMP_FACE = "timestamp_face";
    public static final String TIMESTAMP_DOCUMENT = "timestamp_document";
    public static final String TIMESTAMP_FINGERPRINTS = "timestamp_fingerprints";

    //Empresa
    public static final String ID_ENTERPRICE    = "id_enterprice";
    public static final String CUSTOMER_TYPE    = "customer_type";
    public static final String CUSTOM_COMPANY   = "confType";
    public static final String TIMEOUT_SERVICES = "timeout";

    //Lector de huellas digitales
    public static final String FINGERPRINT_READER = "fingerprint_reader";

    //Coincidencia de captura
    public static final String SIMILITUD_NAPI = "similitud_napi";
    public static final String ByPasAPI = "ByPasAPI";
    public static final String JSON_Data = "JSON_Data";
    public static final String ERROR_NAPI_OCR_ID   = "ERROR_NAPI_OCR_ID";
    public static final String ERROR_NAPI_OCR_DOM  = "ERROR_NAPI_OCR_DOM";
    public static final String SUCCESS_STORAGE_ID  = "UNKNOW";
    public static final String SUCCESS_STORAGE_DOM = "UNKNOW";


    public static void saveToPreferencesString(Context contex, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = contex.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferencesString(Context contex, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = contex.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, preferenceValue);
    }
    public static void saveToPreferencesBoolean(Context contex, String preferenceName, boolean preferenceValue) {
        SharedPreferences sharedPreferences = contex.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(preferenceName, preferenceValue);
        editor.apply();
    }

    public static boolean readFromPreferencesBoolean(Context contex, String preferenceName, boolean preferenceValue) {
        SharedPreferences sharedPreferences = contex.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(preferenceName, preferenceValue);
    }

    public static void deleteFromPreferences(Context contex, String preferenceName) {
        SharedPreferences sharedPreferences = contex.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(preferenceName);
        editor.apply();
    }
    public static void cleanSharedPreferencesOperation(Activity context) {
        //DeleteFiles
        //ICAR FILES
        String operationID = SharedPreferencesUtils.readFromPreferencesString(context, SharedPreferencesUtils.OPERATION_ID, "");
        File fIdFront = new File(ApiConstants.FOLDER_IMGE + File.separator + "icar_front" + operationID + ".jpg");
        if (fIdFront.exists()) {
            fIdFront.delete();
        }
        File fIdBack = new File(ApiConstants.FOLDER_IMGE + File.separator + "icar_back" + operationID + ".jpg");
        if (fIdBack.exists()) {
            fIdBack.delete();
        }
        File fileIdJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "json" + ".json");
        if (fileIdJson.exists()) {
            fileIdJson.delete();
        }
        //FACE FILES
        File faceJPG = new File(ApiConstants.FOLDER_IMGE + File.separator + "face_" + operationID + ".jpg");
        if (faceJPG.exists()) {
            faceJPG.delete();
        }
        File fileFaceJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "rostro" + ".json");
        if (fileFaceJson.exists()) {
            fileFaceJson.delete();
        }
        //DOCUMENT FILES
        File documentJPG = new File(ApiConstants.FOLDER_IMGE + File.separator + "document_"+operationID+".jpg");
        if(documentJPG.exists()){
            documentJPG.delete();
        }
        File fileDocumentJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "document" + ".json");
        if (fileDocumentJson.exists()) {
            fileDocumentJson.delete();
        }
        //FINGERS FILES
        String fingersNames[] = new String[]{"I5","I4","I3","I2","I1","D5","D4","D3","D2","D1"};
        for (int i=0; i<fingersNames.length;i++) {
            File fileFingerJPG = new File(ApiConstants.FOLDER_IMGE + File.separator + "finger_" + fingersNames[i] + "_" + operationID + ".jpg");
            if (fileFingerJPG.exists()) {
                fileFingerJPG.delete();
            }
        }
        File fileFingerJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "fingers" + ".json");
        if(fileFingerJson.exists()){
            fileFingerJson.delete();
        }
        //CONTRACT FILE
        File fileContract = new File(ApiConstants.FOLDER_IMGE + File.separator + "contract_" + operationID + ".pdf");
        if(fileContract.exists()){
            fileContract.delete();
        }
        //Delete Preferences
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.OPERATION_ID);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.TYPE_ID);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.CLIENT_CURP);

        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.INIT_OPE_FORM);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.SCAN_SAVE_ID);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.SCAN_SAVE_ID_ADD);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.FACE_OPERATION);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.FINGERS_OPERATION);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.ID_SCAN);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.DOCUMENT_OPERATION);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.BANK_ACCOUNT);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.CERT_PASSWORD);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.OTP_VALIDATION);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.ACCEPT_CLAUSES);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.SHOW_CONTRACT);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.SIGN_CONTRACT);

        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.OPE_ACCOUNT);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.PAY_OPERATION);

        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.JSON_INIT_FORM);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE);

        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.TIMESTAMP_CREDENTIALS);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.TIMESTAMP_FACE);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.TIMESTAMP_DOCUMENT);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.TIMESTAMP_FINGERPRINTS);

        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.ByPasAPI);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.JSON_Data);

        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.ERROR_NAPI_OCR_ID);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.ERROR_NAPI_OCR_DOM);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.SUCCESS_STORAGE_ID);
        SharedPreferencesUtils.deleteFromPreferences(context,SharedPreferencesUtils.SUCCESS_STORAGE_DOM);

    }
}