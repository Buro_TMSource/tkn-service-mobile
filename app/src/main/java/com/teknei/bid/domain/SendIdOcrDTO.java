package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

public class SendIdOcrDTO {
    @SerializedName("id")
    private String id;

    @SerializedName("idReverso")
    private String idReverso;

    @SerializedName("contentType")
    String contentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdReverso() {
        return idReverso;
    }

    public void setIdReverso(String idReverso) {
        this.idReverso = idReverso;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = "image/jpg";
    }
}
