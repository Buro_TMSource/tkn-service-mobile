package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;

import com.teknei.bid.activities.JsonActivity;
import com.teknei.bid.dialogs.ProgressDialog;

public class GetIneValidation extends AsyncTask {
    private ProgressDialog pd = null;
    private long endTime = 0;
    private Activity activityOrigin;

    public GetIneValidation(Activity activity){
        this.activityOrigin = activity;
    }


    @Override
    protected void onPreExecute() {
        endTime = System.currentTimeMillis() + 1500;
        pd = new ProgressDialog( activityOrigin, "Realizando consulta a INE");
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.show();
    }

    @Override
    protected Void doInBackground(Object[] objects) {
        while (System.currentTimeMillis() < endTime)
        {
            //Spending 2 sec to display progressDialog
        }

        if (pd != null) {
            pd.dismiss();
        }

        ((JsonActivity)activityOrigin).goNext();
        return null;
    }

}
