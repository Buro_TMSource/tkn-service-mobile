package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.activities.SigningContractKaralundiActivity;
import com.teknei.bid.activities.UserKaralundiVerifiActivity;
import com.teknei.bid.dialogs.MessageDialog;
import com.teknei.bid.dialogs.ProgressDialog;
import com.teknei.bid.domain.FingerLoginDTO;
import com.teknei.bid.response.ResponseServicesBID;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendFingerToAuth extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "SendFingerToAuth";

    private long                endTime;
    private String              errorMessage;
    private Integer             responseStatus = 0;

    private Activity activityOrigin;
    private ProgressDialog progressDialog;
    private FingerLoginDTO fingerDTO;
    private boolean firmar = false;

    public SendFingerToAuth(Activity context, FingerLoginDTO fingerDTO) {
        this.activityOrigin = context;
        this.fingerDTO = fingerDTO;
    }

    public SendFingerToAuth(Activity context, FingerLoginDTO fingerDTO, boolean firmar) {
        this.activityOrigin = context;
        this.fingerDTO = fingerDTO;
        this.firmar = firmar;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activityOrigin,activityOrigin.getString(R.string.message_figerprints_auth));
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();

        endTime = System.currentTimeMillis() + 1500;
        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);
    }

    @Override
    protected Void doInBackground(String... params) {
        if (PhoneSimUtils.isNetDisponible(activityOrigin)) {

            Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
            while (System.currentTimeMillis() < endTime) {
                //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
            }
            Log.i("Wait", "timer finish : " + System.currentTimeMillis());

            String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

            int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

            Log.v(CLASS_NAME, "endpoint     :" + endPoint);

            BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

            Call<ResponseServicesBID> call;

            call = api.unauthEnrollmentBiometricSearchCustomerIdCyphered(fingerDTO);

            call.enqueue(new Callback<ResponseServicesBID>() {

                @Override
                public void onResponse(Call<ResponseServicesBID> call, Response<ResponseServicesBID> response) {

                    progressDialog.dismiss();

                    Log.d(CLASS_NAME, response.code() + " ");

                    responseStatus = response.code();

                    if ((responseStatus >= 200 && responseStatus < 300)) {

                        if(firmar)
                            ((SigningContractKaralundiActivity) activityOrigin).resultSuccessToAuth();
                        else
                            ((UserKaralundiVerifiActivity) activityOrigin).resultSuccessToAuth();

                    } else {

                        if (responseStatus >= 300 && responseStatus < 400) {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);
                            if(firmar)
                                ((SigningContractKaralundiActivity) activityOrigin).hidePanelTakeFinger();
                            else
                                ((UserKaralundiVerifiActivity) activityOrigin).hidePanelTakeFinger();

                        } else if (responseStatus >= 400 && responseStatus < 500) {

                            if (responseStatus == 404) {

                                errorMessage =  activityOrigin.getString(R.string.msg_error_access_finger);

                            } else if (responseStatus == 409) {

                                errorMessage =  activityOrigin.getString(R.string.msg_error_access_finger_2);

                            } else if (responseStatus == 422) {

                                errorMessage =  activityOrigin.getString(R.string.msg_error_access_finger_2);

                            } else {

                                errorMessage =  activityOrigin.getString(R.string.message_ws_response_400);

                            }
                            if(firmar)
                                ((SigningContractKaralundiActivity) activityOrigin).resultFailToAuth();
                            else
                                ((UserKaralundiVerifiActivity) activityOrigin).resultFailToAuth();

                        } else if (responseStatus >= 500 && responseStatus < 600) {

                            errorMessage =  "Ocurrió un problema con el servidor";
                            if(firmar)
                                ((SigningContractKaralundiActivity) activityOrigin).hidePanelTakeFinger();
                            else
                                ((UserKaralundiVerifiActivity) activityOrigin).hidePanelTakeFinger();

                        }

                        Log.i(CLASS_NAME, "Error Api " + errorMessage);

                        MessageDialog dialogoAlert;

                        dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();

                    }
                }

                @Override
                public void onFailure(Call<ResponseServicesBID> call, Throwable t) {
                    progressDialog.dismiss();

                    t.printStackTrace();

                    if(firmar)
                        ((SigningContractKaralundiActivity) activityOrigin).hidePanelTakeFinger();
                    else
                        ((UserKaralundiVerifiActivity) activityOrigin).hidePanelTakeFinger();

                    MessageDialog dialogoAlert;
                    dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice),
                            activityOrigin.getString(R.string.msg_error_conexion));
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();

                }
            });
        } else {
            progressDialog.dismiss();
            if(firmar)
                ((SigningContractKaralundiActivity) activityOrigin).hidePanelTakeFinger();
            else
                ((UserKaralundiVerifiActivity) activityOrigin).hidePanelTakeFinger();

            ((BaseActivity) activityOrigin).showErrorConection();
        }
        return null;
    }
}
