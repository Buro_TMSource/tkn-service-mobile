package com.teknei.bid.activities;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;
import com.teknei.bid.R;
import com.teknei.bid.asynctask.GetContract;
import com.teknei.bid.domain.RequestSignContractDTO;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

public class ShowContractActivity extends BaseActivity
        implements OnPageChangeListener, OnLoadCompleteListener, View.OnClickListener {

    private static final String CLASS_NAME = "ShowContractActivity";
    public  String simpleNameFile;

    PDFView pdfView;
    Button btnContinuar = null;

    Integer pageNumber = 0;

    private File    file;
    private String tipoContrato = "";
    private String nombre = "";
    private String domicilio = "";
    private String jsonDom = "";
    private String jsonINE = "";
    private String curp = "";
    private String huella = "";
    private JSONObject jsonObject = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_contract);

        jsonINE = SharedPreferencesUtils.readFromPreferencesString(ShowContractActivity.this, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, "{}");
        jsonDom = SharedPreferencesUtils.readFromPreferencesString(ShowContractActivity.this, SharedPreferencesUtils.DOCUMENT_OPERATION,"{}");
        tipoContrato = getIntent().getExtras().getString("tipoContrato");

        if (tipoContrato.equals("firmado"))
            huella = getIntent().getExtras().getString("fingerPrintString");

        pdfView      = findViewById(R.id.sc_pdf_contract);
        btnContinuar = findViewById(R.id.sc_pdf_b_continue);
        btnContinuar.setOnClickListener(this);
        getContractShow();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sc_pdf_b_continue:

                if (tipoContrato.equals("sinFirma")) {
                    Intent i = new Intent(ShowContractActivity.this, OTPValidationMailActivity.class);
                    startActivity(i);

                }else if (tipoContrato.equals("firmado")) {
                    Intent i = new Intent(ShowContractActivity.this, ResultOperationActivity.class);
                    i.putExtra("actividad","finalizar");
                    startActivity(i);
                }


                break;
        }
    }

    @Override
    public void goNext() { }

    @Override
    public void deleteFile () {
        if (file != null && file.exists()) {
            file.delete();
        }
    }

    public void getContractShow () {

            new GetContract(ShowContractActivity.this, buildObject()).execute();
    }

    public void displayFromAsset(String assetFileName) {
        simpleNameFile = assetFileName;

        file = new File(ApiConstants.FOLDER_IMGE + File.separator, simpleNameFile);
        pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(true)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @Override
    public void onBackPressed() {
    }

    public RequestSignContractDTO buildObject() {

        JSONObject jsonIneObject = null;
        JSONObject jsonDomObject = null;

        try {
            jsonIneObject = new JSONObject(jsonINE);
            jsonDomObject = new JSONObject(jsonDom);
            jsonObject = new JSONObject();
            nombre = jsonIneObject.getString("appat") + " " + jsonIneObject.getString("apmat") + " " + jsonIneObject.getString("name");
            domicilio = jsonDomObject.getString("street")  + ", " +  jsonDomObject.getString("suburb") + ", " + jsonDomObject.getString("locality");
            curp = jsonIneObject.getString("curp");

            jsonObject.put("nombre",nombre);
            jsonObject.put("curp",curp);
            jsonObject.put("domicilio",domicilio);
            jsonObject.put("huella",huella);
        }catch (JSONException ex){
            Log.e("ShowContratActivity", ex.getMessage());
        }

        RequestSignContractDTO requestSignContractDTO = new RequestSignContractDTO();
        if(!nombre.equals(""))
            requestSignContractDTO.setNombre(nombre);
        else
            requestSignContractDTO.setNombre("--");
        if(!curp.equals(""))
            requestSignContractDTO.setCurp(curp);
        else
            requestSignContractDTO.setCurp("--");
        if(!domicilio.equals(""))
            requestSignContractDTO.setDomicilio(domicilio);
        else
            requestSignContractDTO.setDomicilio("--");

        requestSignContractDTO.setHuella(huella);
        requestSignContractDTO.setJsonObject(jsonObject);
        if (tipoContrato.equals("firmado"))
            requestSignContractDTO.setFirmar(true);
        else
            requestSignContractDTO.setFirmar(false);

        return requestSignContractDTO;
    }

}
