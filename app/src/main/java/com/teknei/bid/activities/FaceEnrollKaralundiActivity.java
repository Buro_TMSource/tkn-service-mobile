package com.teknei.bid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facetec.zoom.sdk.AuditTrailType;
import com.facetec.zoom.sdk.ZoomCustomization;
import com.facetec.zoom.sdk.ZoomSDK;
import com.facetec.zoom.sdk.ZoomVerificationActivity;
import com.facetec.zoom.sdk.ZoomVerificationResult;
import com.teknei.bid.R;
import com.teknei.bid.asynctask.FaceFileSend;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.domain.FaceCompareNapiDTO;
import com.teknei.bid.tools.Base64;
import com.teknei.bid.tools.FrontCapture;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class FaceEnrollKaralundiActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "KaralundiActivity";

    private static final String PREF_NAME          = "prefsFile";
    private static final String MFA_ALERT_KEY      = "mfaAlertKey";
    private static final String LANGUAGE_ALERT_KEY = "languageAlertKey";

    private static final String userId                    = "myAppUser";
    private static final String zoomAppToken              = "dPDu0XcLyvAcTrdRs4fRkIfP4q3Bw7mP";
    private static final String MY_SECRET_DATA_TO_STORE   = "A piece of secret data to be secured by ZoOm, only to be unlocked on successful authentication!";
    private static final String encryptionSecretForUserID = "MY_APP_OR_USER_ENCRYPTION_KEY";

    final int QUALITY_COMPRESION = 100;

    ArrayList<Bitmap> auditTrailResult;

    Handler handler;

    private ZoomCustomization customization;

    private boolean shownInitFailedDialog    = false;

    private Button enrollButton;
    private Button buttonContinue;

    private ImageView imagePhoto;

    private byte[] photoBuffer;
    private File imageFile;
    private File fileJson;
    private List<File> fileList;
    private String jsonString = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_enroll_karalundi);
        SharedPreferencesUtils.saveToPreferencesString(FaceEnrollKaralundiActivity.this, SharedPreferencesUtils.FACE_OPERATION, "");

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ico_bid);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            invalidateOptionsMenu();
        }

        handler   = new Handler();
        fileList  = new ArrayList<File>();
        imageFile = null;

        imagePhoto   = (ImageView) findViewById(R.id.imageViewPhoto);
        enrollButton = (Button) findViewById(R.id.fek_btn_take_photo);
        enrollButton.setOnClickListener(this);

        buttonContinue = (Button) findViewById(R.id.fek_btn_continue);
        buttonContinue.setOnClickListener(this);

        enrollButton.setEnabled(false);

        setTestAppStyles();
        checkAnimationStatus();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        enrollButton.setEnabled(true);

        if(resultCode == RESULT_OK) {

            if (requestCode == ZoomSDK.REQUEST_CODE_VERIFICATION) {

                ZoomVerificationResult zoomEnrollmentResult = data.getParcelableExtra(ZoomSDK.EXTRA_VERIFY_RESULTS);

                if (zoomEnrollmentResult.getFaceMetrics() != null && zoomEnrollmentResult.getFaceMetrics().getAuditTrail().size() > 0 ) {

                    auditTrailResult = zoomEnrollmentResult.getFaceMetrics().getAuditTrail();

                    Bitmap bitmap;

                    try {

                        int value = auditTrailResult.size();

                        bitmap = auditTrailResult.get(value-1);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY_COMPRESION, baos);

                        imagePhoto.setImageBitmap(bitmap);

                        imagePhoto.setVisibility(View.VISIBLE);

                        photoBuffer = baos.toByteArray();;

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(FaceEnrollKaralundiActivity.this, "Falla al procesar fotografía, intentelo de nuevo.", Toast.LENGTH_LONG).show();
                    }


                    String operationID = SharedPreferencesUtils.readFromPreferencesString(FaceEnrollKaralundiActivity.this, SharedPreferencesUtils.OPERATION_ID, "");

                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "face_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "face_" + operationID + ".jpg");
                    }

                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imageFile = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imageFile = null;
                    }

                } else {
                    Log.d (TAG,"---------------" + zoomEnrollmentResult.getFaceMetrics().getAuditTrail().size() + "---------------" );
                    Toast.makeText(FaceEnrollKaralundiActivity.this, "Falla al guardar fotografía, intentelo de nuevo.", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(FaceEnrollKaralundiActivity.this, "Falla al capturar fotografía, intentelo de nuevo.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(FaceEnrollKaralundiActivity.this, requestCode +" Falla al capturar fotografía, intentelo de nuevo.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.fek_btn_take_photo:

                Intent enrollmentIntent = new Intent(this, ZoomVerificationActivity.class);

                enrollmentIntent.putExtra(ZoomSDK.EXTRA_RETRIEVE_ZOOM_BIOMETRIC, false);

                startActivityForResult(enrollmentIntent, ZoomSDK.REQUEST_CODE_VERIFICATION);

                break;

            case R.id.fek_btn_continue:
                if (validatePictureTake()) {
                    sendPetition();
                }
                break;
        }
    }

    @Override
    public void sendPetition() {
        //String faceOperation = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.FACE_OPERATION, "");
        String faceOperation = "ok";
        if (faceOperation.equals("")) {
            jsonString = buildJSON();
            fileList.clear();
            fileList.add(fileJson);
            fileList.add(imageFile);

            FaceCompareNapiDTO faceCompareNapiDTO = new FaceCompareNapiDTO();
            faceCompareNapiDTO.setCaptura(Base64.encode(photoBuffer));
            faceCompareNapiDTO.setCredencial(Base64.encode(FrontCapture.getInstance().getBufferFrontCapture()));
            faceCompareNapiDTO.setTipo("imagen");
            faceCompareNapiDTO.setLimiteInferior("10");
            new FaceFileSend(FaceEnrollKaralundiActivity.this, faceCompareNapiDTO).execute();

        } else {
            goNext();
        }
    }

    @Override
    public void goNext() {
        //deleteFile();
        String ByPasAPI = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.ByPasAPI, "");
        String token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");
        if (ByPasAPI == "SI" )
            new FaceFileSend(FaceEnrollKaralundiActivity.this, token, jsonString, fileList).execute();

        Intent i = new Intent(FaceEnrollKaralundiActivity.this, EnrollmentResumeActivity.class);
        startActivity(i);


    }

    @Override
    public void deleteFile () {

        if (fileJson != null && fileJson.exists()) {
            fileJson.delete();
        }

        if (imageFile != null && imageFile.exists()) {
            imageFile.delete();
        }
    }

    //menu actions
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.i_close_operation_menu) {
            AlertDialog dialogoAlert;
            dialogoAlert = new AlertDialog(FaceEnrollKaralundiActivity.this, getString(R.string.message_close_operation_title), getString(R.string.message_close_operation_alert), ApiConstants.ACTION_CANCEL_OPERATION);
            dialogoAlert.setCancelable(false);
            dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogoAlert.show();
        }
        if (id == R.id.i_log_out_menu) {
            AlertDialog dialogoAlert;
            dialogoAlert = new AlertDialog(FaceEnrollKaralundiActivity.this, getString(R.string.message_title_logout), getString(R.string.message_message_logout), ApiConstants.ACTION_LOG_OUT);
            dialogoAlert.setCancelable(false);
            dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogoAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }

    public void checkAnimationStatus() {
        SharedPreferences preferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);

        boolean zoomIntroStatus = preferences.getBoolean("ShowZoomIntro", false);
        customization.showZoomIntro = zoomIntroStatus;

        boolean preEnrollmentScreenSwitchStatus = preferences.getBoolean("ShowPreEnrollmentScreen", false);
        customization.showPreEnrollmentScreen = preEnrollmentScreenSwitchStatus;

        boolean userLockedScreenSwitchStatus = preferences.getBoolean("ShowUserLockedScreen", false);
        customization.showUserLockedScreen = userLockedScreenSwitchStatus;

        boolean successScreenSwitchStatus = preferences.getBoolean("ShowSuccessScreen", false);
        customization.showSuccessScreen = successScreenSwitchStatus;

        boolean failureScreenSwitchStatus = preferences.getBoolean("ShowFailureScreen", false);
        customization.showFailureScreen = failureScreenSwitchStatus;

        boolean cancelButtonOnTopRightStatus = preferences.getBoolean("CancelButtonOnTopRight", false);
        if(cancelButtonOnTopRightStatus) {
            customization.cancelButtonLocation = ZoomCustomization.CancelButtonLocation.TOP_RIGHT;
        }
        else {
            customization.cancelButtonLocation = ZoomCustomization.CancelButtonLocation.TOP_LEFT;
            customization.cancelButtonImage = 0;
        }
    }

    public void setSDKThemeZoom(View view) {
        // Use default theme
        customization = new ZoomCustomization();

        customization.mainBackgroundColor = R.drawable.zoom_custom;
        customization.mainForegroundColor = Color.parseColor("#FFFFFF");

        customization.tabUnselectedBackground = R.drawable.zoom_custom;
        customization.tabUnselectedForegroundColor = Color.parseColor("#ffffff");
        customization.tabSelectedBackgroundColor = Color.parseColor("#33ffffff");
        customization.tabSelectedForegroundColor = Color.parseColor("#ffffff");
        customization.tabSuccessBackgroundColor = Color.parseColor("#ffffff");
        customization.tabSuccessForeground = Color.parseColor("#000000");

        customization.resultsScreenBackground = R.drawable.zoom_custom;
        customization.resultsScreenForegroundColor = Color.parseColor("#FFFFFF");

        customization.progressBackgroundColor = Color.parseColor("#006DFF");
        customization.progressForegroundColor = Color.parseColor("#FFFFFF");

        customization.progressSpinnerColor1 = Color.parseColor("#006DFF");
        customization.progressSpinnerColor2 = Color.parseColor("#006DFF");

        customization.brandingLogo = R.drawable.log_bid;

        //Obtain intro animation settings
        checkAnimationStatus();

        initializeZoom();
    }

    public void showInitFailedMessage() {
        if(shownInitFailedDialog == false) {
            shownInitFailedDialog = true;
            Toast.makeText(this, "Falla al inicializar componentes.", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean validatePictureTake() {
        boolean bitMapTake = false;
        if (imageFile != null) {
            bitMapTake = true;
        } else {
            Toast.makeText(this, "Debes capturar una fotografía del rostro del usuario para continuar.", Toast.LENGTH_SHORT).show();
        }
        return bitMapTake;
    }

    public String buildJSON() {
        String operationID  = SharedPreferencesUtils.readFromPreferencesString(FaceEnrollKaralundiActivity.this, SharedPreferencesUtils.OPERATION_ID, "23");
        String idEnterprice = SharedPreferencesUtils.readFromPreferencesString(FaceEnrollKaralundiActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, "default");
        String customerType = SharedPreferencesUtils.readFromPreferencesString(FaceEnrollKaralundiActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, "default");

        Log.d("FaceScanActivity"," Operation  id " + operationID);
        Log.d("FaceScanActivity"," Enterprice id " + idEnterprice);
        Log.d("FaceScanActivity"," Customer Type " + customerType);

        //Construimos el JSON
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emprId", idEnterprice);
            jsonObject.put("customerType", customerType);
            jsonObject.put("operationId", Integer.valueOf(operationID));
            jsonObject.put("contentType", "image/jpeg");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Writer output;
            fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "rostro" + ".json");
            if (fileJson.exists()) {
                fileJson.delete();
                fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "rostro" + ".json");
            }
            output = new BufferedWriter(new FileWriter(fileJson));
            output.write(jsonObject.toString());
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private void setTestAppStyles(){
        Typeface iconFonts = Typeface.createFromAsset(FaceEnrollKaralundiActivity.this.getAssets(),
                "fonts/zoom-sdk-icon.ttf");

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setSDKThemeZoom(null);
    }

    private void initializeZoom() {

        Log.d (TAG, "---- INICIALIZANDO ZOOM ----");
        ZoomSDK.setCustomization(customization);

        ZoomSDK.initialize(getApplicationContext(), zoomAppToken, mInitializeCallback);

        ZoomSDK.setAuditTrailType(AuditTrailType.HEIGHT_640);

    }

    private final ZoomSDK.InitializeCallback mInitializeCallback = new ZoomSDK.InitializeCallback() {
        @Override
        public void onCompletion(final boolean successful) {
            if (successful) {
                FaceEnrollKaralundiActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        enrollButton.setEnabled(true);
                    }
                });
            }
            else {

                FaceEnrollKaralundiActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                            showInitFailedMessage();
                    }
                });
            }
        }
    };
}
