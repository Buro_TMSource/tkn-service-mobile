package com.teknei.bid.activities;
//Cambiar esta linea de código

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.teknei.bid.R;
import com.teknei.bid.asynctask.StartOperation;
import com.teknei.bid.domain.StartOperationDTO;
import com.teknei.bid.utils.PermissionsUtils;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class SelectActivity extends BaseActivity implements View.OnClickListener {

    private String token;
    private String employee;
    private String idEnterprice;
    private String customerType;

    String   phoneID;

    StartOperationDTO startOperationDTO;

    private int    customCOmpany;

    Button btnRegistry;

    //Widgets
    EditText etMail,etPhone;


    /*Fin de datetime picker*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        etMail = findViewById(R.id.et_mostrar_fecha_picker);
        etPhone =  findViewById(R.id.et_CustomerData_email);
        


        token         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");
        employee      = SharedPreferencesUtils.readFromPreferencesString(SelectActivity.this, SharedPreferencesUtils.USERNAME, "default");
        idEnterprice  = SharedPreferencesUtils.readFromPreferencesString(SelectActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, "default");
        customerType  = SharedPreferencesUtils.readFromPreferencesString(SelectActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, "");

        customCOmpany = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.CUSTOM_COMPANY, ""));

        btnRegistry = findViewById(R.id.btn_register);
        

        btnRegistry.setOnClickListener(this);
        

        //Check Permissions For Android 6.0 up
        PermissionsUtils.checkPermissionPhoneState(this);
        //Hide keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_register:
                if(validateDataActivity())
                    sendPetition();
                break;
        }
        
    }



    @Override
    public void sendPetition() {
        String jsonString = buildJSON();
        new StartOperation(SelectActivity.this, token, jsonString, startOperationDTO).execute();
    }

    @Override
    public void goNext() {
        String operationID = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");
        Toast.makeText(SelectActivity.this, operationID, Toast.LENGTH_LONG).show();
        Intent i = new Intent(SelectActivity.this, SelectIdTypeActivity.class);
        startActivity(i);
    }

    public String buildJSON() {


        String name = "cliente";
        String app1 = "apellido1";
        String app2 = "apellido2";

        String curp = String.valueOf(etPhone.getText());
        String mail = String.valueOf(etMail.getText());

        String phone = curp;

        String numContract = String.valueOf(System.currentTimeMillis());

        String employee     = SharedPreferencesUtils.readFromPreferencesString(SelectActivity.this, SharedPreferencesUtils.USERNAME, "default");
        String idEnterprice = SharedPreferencesUtils.readFromPreferencesString(SelectActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, "default");
        String customerType = SharedPreferencesUtils.readFromPreferencesString(SelectActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, "default");

        phoneID = PhoneSimUtils.getImei(this);

        startOperationDTO = new StartOperationDTO( customCOmpany , Long.parseLong(idEnterprice),
                Long.parseLong(customerType), employee, phoneID, curp, mail, phone);

        startOperationDTO.setEmail(mail);
        startOperationDTO.setPhoneNumber(phone);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emprId", idEnterprice);
            jsonObject.put("customerType", customerType);
            jsonObject.put("deviceId", phoneID);
            jsonObject.put("employee", employee);
            jsonObject.put("curp", curp);
            jsonObject.put("email", mail);
            jsonObject.put("nombre", name);
            jsonObject.put("primerApellido", app1);
            jsonObject.put("segundoApellido", app2);
            jsonObject.put("telefono", phone);
            jsonObject.put("refContrato", numContract);
            //***Almacena Json con los datos del formulario
            SharedPreferencesUtils.saveToPreferencesString(SelectActivity.this, SharedPreferencesUtils.JSON_INIT_FORM, jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    private boolean validateDataActivity () {
        boolean flag = true;
        if (etPhone.getText().toString().equals("")) {
            etPhone.setError("El campo teléfono no puede estar vacío");
            flag = false;
        }

        if (etMail.getText().toString().equals("")) {
            etMail.setError("El campo correo no puede estar vacío");
            flag = false;
        }
        return flag;
    }


}





