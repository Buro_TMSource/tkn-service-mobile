package com.teknei.bid.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;

import com.teknei.bid.R;

/**
 * Created by Desarrollo on 10/07/2017.
 */

public class ApiConstants {

    public static final int TYPE_ACT_BASIC = 0;
    public static final int TYPE_ACT_LOCAL = 1;

    public static final int ACTION_LOG_OUT                  = 0;
    public static final int ACTION_TRY_AGAIN                = 1;
    public static final int ACTION_CANCEL_OPERATION         = 2;
    public static final int ACTION_BLOCK_CANCEL_OPERATION   = 3;
    public static final int ACTION_GO_NEXT                  = 4;
    public static final int ACTION_TRY_AGAIN_CANCEL         = 5;
    public static final int ACTION_GO_STEP                  = 6;
    public static final int ACTION_TRY_AGAIN_CONTINUE       = 7;

    public static final int ACTION_LOG_OUT_LOCAL                = 10;
    public static final int ACTION_TRY_AGAIN_LOCAL              = 11;
    public static final int ACTION_CANCEL_OPERATION_LOCAL       = 12;
    public static final int ACTION_BLOCK_CANCEL_OPERATION_LOCAL = 13;
    public static final int ACTION_GO_NEXT_LOCAL                = 14;
    public static final int ACTION_TRY_AGAIN_CANCEL_LOCAL       = 15;
    public static final int ACTION_GO_STEP_LOCAL                = 16;
    public static final int ACTION_TRY_AGAIN_CONTINUE_LOCAL     = 17;

    public static final int ACTION_TRY_AGAIN_CONTINUE_DOC       =  8;
    public static final int ACTION_TRY_GFO_AGAIN_CANCEL         =  9;
    public static final int ACTION_CANCEL_CONTINUE              = 20;

    public static final int ACTION_FINISH_SING_CONTRACT_GO_NEXT             = 0;
    public static final int ACTION_FINISH_SING_CONTRACT_TRY_AGAIN           = 1;
    public static final int ACTION_FINISH_SING_CONTRACT_CANCEL              = 2;
    public static final int ACTION_FINISH_SING_CONTRACT_TRY_AGAIN_CANCEL    = 3;
    public static final int ACTION_FINISH_SING_CONTRACT_TRY_AGAIN_CONTINUE  = 4;

    public static final int ACTION_SEND_FINGER_CONTRACT     = 30;
    public static final int ACTION_CONFIRM_FINGER_CONTRACT  = 31;

    public static final int TYPE_INE      = 1;
    public static final int TYPE_PASSPORT = 2;

    public static final int TYPE_OPERATOR = 2;
    public static final int TYPE_CUSTOMER = 1;

    public static final int CUSTOM_SABADEL = 2;
    public static final int CUSTOM_HSBC    = 1;

    public static final int SABADELL_FLOW_01    =   1;
    public static final int SABADELL_FLOW_02    =   2;
    public static final int SABADELL_FLOW_03    =   3;
    public static final int SABADELL_FLOW_04    =   4;
    public static final int SABADELL_FLOW_05    =   5;
    public static final int SABADELL_FLOW_06    =   6;
    public static final int SABADELL_FLOW_07    =   7;
    public static final int SABADELL_FLOW_08    =   8;
    public static final int SABADELL_FLOW_09    =   9;
    public static final int SABADELL_FLOW_10    =  10;
    public static final int SABADELL_FLOW_11    =  11;
    public static final int SABADELL_FLOW_12    =  12;
    public static final int SABADELL_FLOW_13    =  13;
    public static final int SABADELL_FLOW_14    =  14;
    public static final int SABADELL_FLOW_15    =  15;
    public static final int SABADELL_FLOW_16    =  16;
    public static final int SABADELL_FLOW_17    =  17;
    public static final int SABADELL_FLOW_18    =  18;

    public static final String LOG_IN_USER  = "login";
    public static final String LOG_OUT_USER = "logout";

    public static final String FOLDER_NAME  = "/.morpho";
    public static final String FOLDER_IMGE  = Environment.getExternalStorageDirectory() + "/.bid_img";
    public static final String ICAR_KEY     = "7ZhJGVSVZbt01g9ju2aTF+d0WLmT81bbeMoPtBkdaRBikz2VszGiQholjIQFPlHju3fNW+5evzvUx1q2gSHCPDV6x4bOtSeyO14bRRKOLXMuXZLBqYDKoa+AljtP8Os5oTS2aM4ZAN7NqWMu4LYgE1cvxa/jM0Ibq0lFS3h3ChBert6G+DGEHQU18JgMsayI4ThTvSQ/21orOrDXnQ1GA/90x0rpT3VHYksWa82GFbk=";


    public static final String METHOD_CANCEL_OPERATION          = "rest/v3/enrollment/status/cancel?operationId=";
    public static final String METHOD_START_OPERATION           = "rest/v3/enrollment/status/start";
    public static final String METHOD_CREDENTIALS               = "rest/v3/enrollment/credentials/credential";
    public static final String METHOD_FACE                      = "rest/v3/enrollment/facial/face";
    public static final String METHOD_DOCUMENT                  = "rest/v3/enrollment/address/comprobanteParsed";
    public static final String METHOD_FINGERS                   = "rest/v3/enrollment/biometric/minucias/";
    public static final String METHOD_PAY_CONFIRM               = "rest/v3/enrollment/status/end";
    public static final String METHOD_GET_CONTRACT              = "rest/v3/enrollment/contract/contrato/";
    public static final String METHOD_SEND_CONTRACT             = "rest/v3/enrollment/contract/contrato/add/";
    public static final String METHOD_CHECK_PENDING_OPERATION   = "rest/v3/enrollment/client/detail/step";
    public static final String METHOD_GET_PENDING_OPERATION     = "rest/v3/enrollment/client/detail/detail";
    public static final String METHOD_GET_TIMESTAMP             = "rest/v3/enrollment/client/detail/search/cusomer/ts";

    //Strings values
    public static final String STRING_INE       = "INE";
    public static final String STRING_IFE       = "IFE";
    public static final String STRING_PASSPORT  = "PASAPORTE";

    //String Fields Contract
    public static final String SOLI_PROD        = "SOLI_PROD";       //Solicitud del producto
    public static final String DATO_GRAL        = "DATO_GRAL";       //Datos generales del solicitante
    public static final String DATO_BENE        = "DATO_BENE";       //Datos de beneficiarios
    public static final String APP_MOVI         = "APP_MOVI";       //Contrato de banca electronica (App movil)
    public static final String CTA_RELA         = "CTA_RELA";       //Contrato de deposito bancario de Dinero a la Vista (Cuenta relacion)
    public static final String PAGA_SABA        = "PAGA_SABA";       //Contrato de deposito bancario de Dinero a Plazo Fijo (Pagare Sabadell)
    public static final String AUTO_INFO_CRED   = "AUTO_INFO_CRED";  //Autorizacion para consultar su informacion crediticia
    public static final String AUTO_COMP_INFO   = "AUTO_COMP_INFO";  //Autorizacion para compartir informacion
    public static final String AUTO_FINE_MERC   = "AUTO_FINE_MERC";  //Autorizacion para fines de mercadeo
    public static final String CLAU_CONT_MULT   = "CLAU_CONT_MULT";  //Clausulado del Contrato Multiple Cuenta Relacion
    public static final String FORM_CTA_DEST    = "FORM_CTA_DEST";   //Formato de registro de cuenta destino
    public static final String FORM_FATC        = "FORM_FATC";       //Formato FATCA

    //Icar INE/IFE Values
    public static final String ICAR_NAME                = "name";
    public static final String ICAR_SURNAME             = "surname";
    public static final String ICAR_FIRST_SURNAME       = "firstSurname";
    public static final String ICAR_SECOND_SURNAME      = "secondSurname";
    public static final String ICAR_ADDRESS             = "address";
    public static final String ICAR_MRZ                 = "MRZ";
    public static final String ICAR_OCR                 = "CRC_SECTION";
    public static final String ICAR_VALIDITY            = "dateOfExpiry";
    public static final String ICAR_CURP                = "curp";
    public static final String ICAR_PASSPORT_VALIDITY   = "dateOfExpiry";

    public static final String ICAR_STREET    = "street";
    public static final String ICAR_SUBURB    = "suburb";
    public static final String ICAR_ZIPCODE   = "zipCode";
    public static final String ICAR_LOCALITY  = "locality";
    public static final String ICAR_STATE     = "state";

    public static final String  CAP_INI    = "CAP-INI";     //	Captura de información identificativa inicial	CLIENTE
    public static final String  CAP_CRE	   = "CAP-CRE";     //  Captura de credencial o identificación oficial(OCR de credencial y Edición de datos)	CLIENTE
    public static final String  CAP_2CRE   = "CAP-2CRE";    //	Captura de segunda credencial o identificación oficial	CLIENTE
    public static final String  CAP_VALINE = "CAP-VALINE";  //	Captura de Validación INE – Portal web (pantallazo)	CLIENTE
    public static final String  CAP_DAC    = "CAP-DAC";     //	Captura de huellas dactilares	CLIENTE
    public static final String  CAP_FAC    = "CAP-FAC";     //	Captura facial	CLIENTE
    public static final String  CAP_COMDO  = "CAP-COMDO";	//  Captura de Comprobante de domicilio(OCR del comprobante y Edición de datos)	CLIENTE
    public static final String  CAP_CD	   = "CAP-CD";      //  Captura de información cuenta destino	CLIENTE
    public static final String  CAP_COD    = "CAP-COD";     // 	Captura de código secreto cliente (contraseña)	CLIENTE
    public static final String  GEN_CERTNO = "GEN-CERTNO";  //	Generación de certificado no reconocido	CLIENTE
    public static final String  CONS_CONT  = "CONS-CONT";   // 	Construcción de contrato	CLIENTE
    public static final String  ENV_REGOTP = "ENV-REGOTP";  //	Envío y registro de OTP	CLIENTE
    public static final String  ACE_TC     = "ACE-TC";	    //  Pantalla informativa (Aceptación de términos y condiciones)
    public static final String  AUT_OTP    = "AUT-OTP";	    //  Autenticación de OTP	CLIENTE
    public static final String  AUT_DAC    = "AUT-DAC";     // 	Autenticación dactilar 	CLIENTE
    public static final String  FIR_CONT   = "FIR-CONT";    // 	Firma de Contrato a través de certificado no reconocido	CLIENTE
    public static final String  EST_TIEMCO = "EST-TIEMCO";  // 	Estampa de tiempo sobre contrato (NOM 151)	CLIENTE
    public static final String  GEN_CREOPE = "GEN-CREOPE";  //	Generación de credenciales agente (usuario y password)	OPERADOR
    public static final String  ByPasAPI = "SI";  //	SI= Interactua con los servicios de BID, NO= No interactua con los servicios de BID
    public static final String  GoToVideoCall = "NO";  //	SI= Pasa al activity correspondiente de videollamada, NO= Finaliza en el activity donde es utilizado



    //Excepciones en captura identificativa (INE/IFE/PASAPORTE)
    private static final int errorResponse10001 = 10001;  //ICAR. ERROR AL DIGITALIZAR O VERIFICAR EL DOCUMENTO IDENTIFICATIVO.
    private static final int errorResponse10002 = 10002;  //TAS. ERROR AL ALMACENAR EL DOCUMENTO IDENTIFICATIVO EN EXPEDIENTE DIGITAL.');
    private static final int errorResponse10003 = 10003;  //B.D. ERROR AL PERSISTIR PROCESO IDENTIFICATIVO SOBRE BASE DE DATOS.');

    //Captura Facial
    private static final int errorResponse20001 = 20001;  //TAS. ERROR AL ALMACENAR FOTOGRAFÍA FACIAL EN EXPEDIENTE DIGITAL.');
    private static final int errorResponse20002 = 20002;  //B.D. ERROR AL PERSISTIR PROCESO DE CAPTURA FACIAL SOBRE BASE DE DATOS.');

    //Comprobante de Domicilio
    private static final int errorResponse30001 = 30001; //KOFAX. ERROR AL DIGITALIZAR O VERIFICAR EL COMPROBANTE DE DOMICILIO.');
    private static final int errorResponse30002 = 30002; //TAS. ERROR AL ALMACENAR COMPROBANTE DE DOMICILIO EN EXPEDIENTE DIGITAL.');
    private static final int errorResponse30003 = 30003; //B.D. ERROR AL PERSISTIR PROCESO DE COMPROBANTE DE DOMICILIO SOBRE BASE DE DATOS.');

    //Registro Biométrico
    private static final int errorResponse40001 = 40001; //'TAS. ERROR AL ALMACENAR MINUCIAS EN EXPEDIENTE DIGITAL.');
    private static final int errorResponse40002 = 40002; //'MBSS. ERROR AL ALMACENA MINUCIAS DACTILARES Y FACIALES EN MOTOR BIOMÉTRICO.');
    private static final int errorResponse40003 = 40003; //'MBSS. ERROR, BIOMETRICOS DACTILARES O FACIALES DUPLICADOS.');
    private static final int errorResponse40004 = 40004; //'B.D. ERROR AL PERSISTIR PROCESO DE REGISTRO BIOMÉTRICO SOBRE BASE DE DATOS.

    public final static String PDF_PATH = Environment.getExternalStorageDirectory().getPath() + "/pdfContrats";
    public final static String SCANAUX = "okCredentials"; //BANDERA DE EXITO EN ESCANEO DE IDENTIFIACION OFICIAL
    public final static String SUCCESS_STORAGE_ID = "SUCCESS"; //BANDERA DE EXITO DE ALMACENAMIENTO DE IDENTIFIACION OFICIAL
    public final static String SUCCESS_STORAGE_DOM = "SUCCESS"; //BANDERA DE EXITO DE ALMACENAMIENTO DE COMPROBANTE DE DOMICILIO
    public final static String document_storage_success = "DOCUMENTO ALMACENADO CORRECTAMENTE";
    public final static String document_storage_failure = "DOCUMENTO NO ALMACENADO";


    public static String managerErrorServices (int msgError, Activity activityOrigin) {

        String errorMessage;

        switch (msgError) {

            case errorResponse10001:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_10001);
                break;

            case errorResponse10002:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_10002);
                break;

            case errorResponse10003:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_10003);
                break;

            case errorResponse20001:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_20001);
                break;

            case errorResponse20002:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_20002);
                break;

            case errorResponse30001:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_30001);
                break;

            case errorResponse30002:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_30002);
                break;

            case errorResponse30003:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_30003);
                break;

            case errorResponse40001:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_40001);
                break;

            case errorResponse40002:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_40002);
                break;

            case errorResponse40003:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_40003);
                break;

            case errorResponse40004:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_40004);
                break;
            default:
                errorMessage = msgError + " - " + activityOrigin.getString(R.string.message_ws_response_fail);
                break;
        }

        return errorMessage;
    }

    public static String managerErrorServices (String message) {

        return message;
    }

}
