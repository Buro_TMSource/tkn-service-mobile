package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.teknei.bid.R;
import com.teknei.bid.utils.ApiConstants;


public class kycActivity extends BaseActivity implements View.OnClickListener {


    private Button btn_kyc_next;
    private RadioButton rd_ppexp_si;
    private RadioButton rd_ppexp_no;
    private RadioButton rd_ingresosadicionales_si;
    private RadioButton rd_ingresosadicionales_no;

    private RadioButton rd_cuentaconauto_si;
    private RadioButton rd_cuentaconauto_no;

    private RadioButton rd_destinocredito_emergencia;
    private RadioButton rd_destinocredito_consumo;
    private RadioButton rd_destinocredito_pagootroscreditos;
    private RadioButton rd_destinocredito_capitaltrabajo;

    private RadioButton rd_porcuentapropia;
    private RadioButton rd_porcuentadetercero;

    private RadioButton rd_bajo;
    private RadioButton rd_alto;


    LinearLayout et_Ppexp_llayout;
    LinearLayout et_kyc_group4;

    LinearLayout et_kyc_group6;
    LinearLayout et_kyc_group7;
    LinearLayout et_kyc_group10;

    EditText et_Ppexp_FullName;
    EditText et_group4_especificar;
    EditText et_group6_marca;
    EditText et_group10_nombredeltercero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc);


        btn_kyc_next    = (Button) findViewById(R.id.btn_kyc_next);

          rd_ppexp_si= (RadioButton) findViewById(R.id.rd_ppexp_si);
          rd_ppexp_no= (RadioButton) findViewById(R.id.rd_ppexp_no);

          rd_ingresosadicionales_si= (RadioButton) findViewById(R.id.rd_ingresosadicionales_si);
          rd_ingresosadicionales_no= (RadioButton) findViewById(R.id.rd_ingresosadicionales_no);

          rd_cuentaconauto_si= (RadioButton) findViewById(R.id.rd_cuentaconauto_si);
          rd_cuentaconauto_no= (RadioButton) findViewById(R.id.rd_cuentaconauto_no);

          rd_destinocredito_emergencia= (RadioButton) findViewById(R.id.rd_destinocredito_emergencia);
          rd_destinocredito_consumo= (RadioButton) findViewById(R.id.rd_destinocredito_consumo);
          rd_destinocredito_pagootroscreditos= (RadioButton) findViewById(R.id.rd_destinocredito_pagootroscreditos);
          rd_destinocredito_capitaltrabajo= (RadioButton) findViewById(R.id.rd_destinocredito_capitaltrabajo);

          rd_porcuentapropia= (RadioButton) findViewById(R.id.rd_porcuentapropia);
          rd_porcuentadetercero= (RadioButton) findViewById(R.id.rd_porcuentadetercero);

          rd_bajo= (RadioButton) findViewById(R.id.rd_bajo);
          rd_alto= (RadioButton) findViewById(R.id.rd_alto);


         et_Ppexp_llayout= (LinearLayout) findViewById(R.id.et_Ppexp_llayout);
         et_kyc_group4= (LinearLayout) findViewById(R.id.et_kyc_group4);

         et_kyc_group6= (LinearLayout) findViewById(R.id.et_kyc_group6);
         et_kyc_group7= (LinearLayout) findViewById(R.id.et_kyc_group7);
         et_kyc_group10= (LinearLayout) findViewById(R.id.et_kyc_group10);

         et_Ppexp_FullName=(EditText)findViewById(R.id.et_Ppexp_FullName);
        et_group4_especificar=(EditText)findViewById(R.id.et_group4_especificar);
        et_group6_marca=(EditText)findViewById(R.id.et_group6_marca);
        et_group10_nombredeltercero=(EditText)findViewById(R.id.et_group10_nombredeltercero);




        btn_kyc_next.setOnClickListener(this);
        rd_ppexp_si.setOnClickListener(this);
        rd_ppexp_no.setOnClickListener(this);
        rd_ingresosadicionales_si.setOnClickListener(this);
        rd_ingresosadicionales_no.setOnClickListener(this);
        rd_cuentaconauto_si.setOnClickListener(this);
        rd_cuentaconauto_no.setOnClickListener(this);
        rd_destinocredito_emergencia.setOnClickListener(this);
        rd_destinocredito_consumo.setOnClickListener(this);
        rd_destinocredito_pagootroscreditos.setOnClickListener(this);
        rd_destinocredito_capitaltrabajo.setOnClickListener(this);
        rd_porcuentapropia.setOnClickListener(this);
        rd_porcuentadetercero.setOnClickListener(this);
        rd_bajo.setOnClickListener(this);
        rd_alto.setOnClickListener(this);

        et_Ppexp_llayout.setOnClickListener(this);
        et_kyc_group4.setOnClickListener(this);

        et_kyc_group6.setOnClickListener(this);
        et_kyc_group7.setOnClickListener(this);
        et_kyc_group10.setOnClickListener(this);

        et_Ppexp_llayout.setVisibility(View.GONE);
        et_kyc_group4.setVisibility(View.GONE);
        et_kyc_group6.setVisibility(View.GONE);
        et_kyc_group7.setVisibility(View.GONE);
        et_kyc_group10.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rd_ppexp_si:
                et_Ppexp_llayout.setVisibility(View.VISIBLE);
                et_Ppexp_FullName.requestFocus();
                break;
            case R.id.rd_ppexp_no:
                et_Ppexp_llayout.setVisibility(View.GONE);
                break;

            case R.id.rd_ingresosadicionales_si:
                et_kyc_group4.setVisibility(View.VISIBLE);
                et_group4_especificar.requestFocus();
                break;
            case R.id.rd_ingresosadicionales_no:
                et_kyc_group4.setVisibility(View.GONE);
                break;

            case R.id.rd_cuentaconauto_si:
                et_kyc_group6.setVisibility(View.VISIBLE);
                et_kyc_group7.setVisibility(View.VISIBLE);
                et_group6_marca.requestFocus();
                break;
            case R.id.rd_cuentaconauto_no:
                et_kyc_group6.setVisibility(View.GONE);
                et_kyc_group7.setVisibility(View.GONE);
                break;

            case R.id.rd_porcuentadetercero:
                et_kyc_group10.setVisibility(View.VISIBLE);
                et_group10_nombredeltercero.requestFocus();
                break;
            case R.id.rd_porcuentapropia:
                et_kyc_group10.setVisibility(View.GONE);
                break;

            case R.id.btn_kyc_next:
                goNext();
                break;


        }


    }

    @Override
    public void goNext() {

            if(ApiConstants.GoToVideoCall.equals("SI")) {
                /*Enviar a videollamada */
                  startActivity(new Intent(kycActivity.this, VideoCallActivity.class));
            }else{
                Intent i = new Intent(kycActivity.this, ShowContractActivity.class);
                i.putExtra("tipoContrato","sinFirma");
                startActivity(i);
            }

    }




}
