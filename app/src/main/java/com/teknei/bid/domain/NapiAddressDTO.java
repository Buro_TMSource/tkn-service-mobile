package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

public class NapiAddressDTO {

    @SerializedName("comprobante")
    private String comprobante;

    public String getComprobante() {
        return comprobante;
    }

    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }
}
