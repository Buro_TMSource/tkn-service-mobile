package com.teknei.bid.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.identy.enums.FingerDetectionMode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MissingFingerUtils {

    public static void saveStringSet(Set<String> obj, Context ctx) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String key = "missing_fingers_v1";
        if (obj.isEmpty()) {
            editor.remove(key);
            editor.commit();
            return;
        }
        editor.putStringSet(key, obj);
        editor.commit();
    }


    public static Set<String> retriveStringSet(Context ctx) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String key = "missing_fingers_v1";

        return sharedPreferences.getStringSet(key, new HashSet<String>());
    }

    public static List<FingerDetectionMode> getLeftMissingFingers(Context ctx) {
        Set<String> selections = retriveStringSet(ctx);
        List<FingerDetectionMode> modes = new ArrayList<>();
        if (selections.contains(FingerDetectionMode.LEFT_INDEX.toString())) {
            modes.add(FingerDetectionMode.LEFT_INDEX);
        }
        if (selections.contains(FingerDetectionMode.LEFT_MIDDLE.toString())) {
            modes.add(FingerDetectionMode.LEFT_MIDDLE);
        }
        if (selections.contains(FingerDetectionMode.LEFT_RING.toString())) {
            modes.add(FingerDetectionMode.LEFT_RING);
        }
        if (selections.contains(FingerDetectionMode.LEFT_LITTLE.toString())) {
            modes.add(FingerDetectionMode.LEFT_LITTLE);
        }
        return modes;

    }
    public static List<FingerDetectionMode> getRightMissingFingers(Context ctx) {
        Set<String> selections = retriveStringSet(ctx);
        List<FingerDetectionMode> modes = new ArrayList<>();

        if (selections.contains(FingerDetectionMode.RIGHT_INDEX.toString())) {
            modes.add(FingerDetectionMode.RIGHT_INDEX);

        }
        if (selections.contains(FingerDetectionMode.RIGHT_MIDDLE.toString())) {
            modes.add(FingerDetectionMode.RIGHT_MIDDLE);

        }
        if (selections.contains(FingerDetectionMode.RIGHT_RING.toString())) {
            modes.add(FingerDetectionMode.RIGHT_RING);

        }
        if (selections.contains(FingerDetectionMode.RIGHT_LITTLE.toString())) {
            modes.add(FingerDetectionMode.RIGHT_LITTLE);

        }
        return modes;
    }



}
