package com.teknei.bid.response;

import com.google.gson.annotations.SerializedName;

public class NapiFacialResponse {

    @SerializedName("estatus")
    private String estatus;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("similitud")
    private String similitud;



    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getSimilitud() {
        return similitud;
    }

    public void setSimilitud(String similitud) {
        this.similitud = similitud;
    }


}
