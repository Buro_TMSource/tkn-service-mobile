package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.dialogs.ProgressDialog;
import com.teknei.bid.domain.FaceCompareNapiDTO;
import com.teknei.bid.response.NapiFacialResponse;
import com.teknei.bid.response.ResponseServicesBID;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.services.NapiEndPointServices;
import com.teknei.bid.tools.FrontCapture;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaceFileSend extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "FaceFileSend";

    private String token;
    private String jsonS;
    private List<File> imageF;

    private boolean isNapi = true;

    private Activity activityOrigin;
    private String errorMessage;
    private boolean responseOk = false;
    private ProgressDialog progressDialog;

    private Integer responseStatus = 0;

    private long endTime;

    private ResponseServicesBID responseFace;
    private FaceCompareNapiDTO faceCompareNapiDTO;

    public FaceFileSend(Activity context, FaceCompareNapiDTO jsonFile){
        this.activityOrigin = context;
        this.faceCompareNapiDTO = jsonFile;
        this.isNapi = true;
    }

    public FaceFileSend(Activity context, String tokenOld, String jsonString, List<File> imageFile) {
        this.activityOrigin = context;
        this.token = tokenOld;
        this.jsonS = jsonString;
        this.imageF = imageFile;
        this.isNapi = false;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(
                activityOrigin,
                activityOrigin.getString(R.string.message_face_scan_check));
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();

        endTime = System.currentTimeMillis() + 1500;
        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);
    }

    @Override
    protected Void doInBackground(String... params) {
        if (PhoneSimUtils.isNetDisponible(activityOrigin)) {
            if(isNapi){
                callServiceNapi();
            }else {
                callServicesBid();
            }

        } else {
            progressDialog.dismiss();

            ((BaseActivity) activityOrigin).showErrorConection();
        }
        return null;
    }

    private void callServiceNapi(){
        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());

        String endPoint = "https://www.napi.buroidentidad.com/";
        int timeOut = 120;
        String authorization = Credentials.basic("buroidentidad","_9x3Ro.Xp");

        NapiEndPointServices api = RetrofitSingleton.getInstance().build(endPoint, timeOut).create(NapiEndPointServices.class);

        Call<NapiFacialResponse> call = api.compareFacial("application/json", authorization, faceCompareNapiDTO);

        call.enqueue(new Callback<NapiFacialResponse>() {
            @Override
            public void onResponse(Call<NapiFacialResponse> call, Response<NapiFacialResponse> response) {
                progressDialog.dismiss();

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                NapiFacialResponse napiFacialResponse = new NapiFacialResponse();

                if (response.body() != null && response.isSuccessful())
                    napiFacialResponse = response.body();
                if (responseStatus >= 200 && responseStatus < 300){
                    if (napiFacialResponse.getEstatus().equals("OK")) {
                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.FACE_OPERATION, "ok");
                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SIMILITUD_NAPI, napiFacialResponse.getSimilitud());

                        Log.d("COMPARACIÓN FACIAL:", napiFacialResponse.getSimilitud() + " - ");

                        FrontCapture.getInstance().setSimilitud(napiFacialResponse.getSimilitud());

                        errorMessage = napiFacialResponse.getMensaje();
                        ((BaseActivity)activityOrigin).goNext();

                    } else {
                        errorMessage = napiFacialResponse.getMensaje();

                        Log.i(CLASS_NAME, "Face: " + errorMessage);
                        ((BaseActivity)activityOrigin).goNext();

                    }
                }else if (responseStatus >= 400 && responseStatus < 500) {

                    if (responseStatus == 422) {

                        if (response.body() == null) {

                            errorMessage = activityOrigin.getString(R.string.msg_error_capture_face_duplicate);

                        } else {

                            if (responseFace.getErrorMessage().length() > 6) {

                                errorMessage = activityOrigin.getString(R.string.msg_error_capture_face);

                            } else {

                                errorMessage = ApiConstants.managerErrorServices(Integer.parseInt(responseFace.getErrorMessage()), activityOrigin);

                            }
                        }

                    } else {

                        errorMessage = activityOrigin.getString(R.string.message_ws_response_400);

                    }

                    Log.i(CLASS_NAME, "Face: " + errorMessage);

                } else {

                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                    }

                    Log.i(CLASS_NAME, "Face: " + errorMessage);

                }

            }

            @Override
            public void onFailure(Call<NapiFacialResponse> call, Throwable t) {
                progressDialog.dismiss();

                t.printStackTrace();

                AlertDialog dialogoAlert;
                dialogoAlert = new AlertDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice),
                        activityOrigin.getString(R.string.msg_error_conexion), ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                dialogoAlert.setCancelable(false);
                dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogoAlert.show();
            }
        });
    }


    private void callServicesBid(){
        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());

        String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

        Log.v("token             :", "token        :" + token);
        Log.v("NUM FILES         :", "NUM FILES    :" + imageF.size());
        Log.v("json SEND NO File :", "json no file :" + jsonS);
        Log.v("END POINT         :", "endpoint     :" + endPoint);

        BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

        MultipartBody.Part jsonBody =
                MultipartBody.Part.createFormData("json", imageF.get(0).getName(),
                        RequestBody.create(MediaType.parse("application/json"), imageF.get(0)));

        MultipartBody.Part jsonFront =
                MultipartBody.Part.createFormData("file", imageF.get(1).getName(),
                        RequestBody.create (MediaType.parse("image/jpg"), imageF.get(1)));

        Call<ResponseServicesBID> call = api.enrollmentFacialFace(token, jsonBody, jsonFront);

        call.enqueue(new Callback<ResponseServicesBID>() {

            @Override
            public void onResponse(Call<ResponseServicesBID> call, Response<ResponseServicesBID> response) {
                progressDialog.dismiss();

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (response.body() != null) {
                    responseFace = response.body();
                    responseOk   = responseFace.isResultOK();
                    errorMessage = responseFace.getErrorMessage();
                }

                if (responseStatus >= 200 && responseStatus < 300) {

                    if (responseOk) {

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.FACE_OPERATION, "ok");

                        AlertDialog dialogoAlert;
                        dialogoAlert = new AlertDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage, ApiConstants.ACTION_GO_NEXT);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();
                        ((BaseActivity) activityOrigin).deleteFile();

                    } else {
                        Log.i(CLASS_NAME, "Face: " + errorMessage);
                        AlertDialog dialogoAlert;
                        dialogoAlert = new AlertDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), responseStatus+" "+errorMessage, ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();
                    }

                } else if (responseStatus >= 400 && responseStatus < 500) {

                    if (responseStatus == 422) {

                        if (response.body() == null) {

                            errorMessage = activityOrigin.getString(R.string.msg_error_capture_face_duplicate);

                        } else {

                            responseFace = response.body();

                            if (responseFace.getErrorMessage().length() > 6) {

                                errorMessage = activityOrigin.getString(R.string.msg_error_capture_face);

                            } else {

                                errorMessage = ApiConstants.managerErrorServices(Integer.parseInt(responseFace.getErrorMessage()), activityOrigin);

                            }
                        }

                    } else {

                        errorMessage = activityOrigin.getString(R.string.message_ws_response_400);

                    }

                    Log.i(CLASS_NAME, "Face: " + errorMessage);

                    AlertDialog dialogoAlert;
                    dialogoAlert = new AlertDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), responseStatus +" "+ errorMessage, ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();

                } else {

                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                    }

                    Log.i(CLASS_NAME, "Face: " + errorMessage);

                    AlertDialog dialogoAlert;
                    dialogoAlert = new AlertDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage, ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseServicesBID> call, Throwable t) {
                progressDialog.dismiss();

                t.printStackTrace();

                AlertDialog dialogoAlert;
                dialogoAlert = new AlertDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice),
                        activityOrigin.getString(R.string.msg_error_conexion), ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                dialogoAlert.setCancelable(false);
                dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogoAlert.show();

            }
        });
    }
}
