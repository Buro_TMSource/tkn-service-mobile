package com.teknei.bid.services;

import com.teknei.bid.domain.DetailSearchDTO;
import com.teknei.bid.domain.FieldSearchDetailDTO;
import com.teknei.bid.domain.FieldsContractDTO;
import com.teknei.bid.domain.FingerLoginDTO;
import com.teknei.bid.domain.MailVerificationOTPDTO;
import com.teknei.bid.domain.RequestFeldContractDTO;
import com.teknei.bid.domain.ResendOtpDTO;
import com.teknei.bid.domain.StartOperationDTO;
import com.teknei.bid.domain.ValidateOtpDTO;
import com.teknei.bid.response.ResponseDocument;
import com.teknei.bid.response.ResponseServicesBID;
import com.teknei.bid.response.ResponseStartOpe;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by rgarciav on 25/10/2017.
 */

public interface BIDEndPointServices {

    @POST("rest/v3/enrollment/status/start")
    Call<ResponseStartOpe> enrollmentStatusStart (@Header("Authorization") String authorization,
                                                  @Body StartOperationDTO startOperationDTO);

    @Multipart
    @POST("rest/v3/enrollment/credentials/credential")
    Call<ResponseServicesBID> enrollmentCredential (@Header("Authorization") String authorization,
                                                    @Part MultipartBody.Part jsonFile,
                                                    @Part MultipartBody.Part imageFileFront,
                                                    @Part MultipartBody.Part imageFileBack);

    @Multipart
    @POST("rest/v3/enrollment/facial/face")
    Call<ResponseServicesBID> enrollmentFacialFace (@Header("Authorization") String authorization,
                                                    @Part MultipartBody.Part jsonFile,
                                                    @Part MultipartBody.Part imageFile);

    @Multipart
    @POST("rest/v3/enrollment/address/comprobanteParsed")
    Call<ResponseDocument> enrollmentAddressComprobanteParsed (@Header("Authorization") String authorization,
                                                               @Part MultipartBody.Part jsonFile,
                                                               @Part MultipartBody.Part imageFile);


    @POST ("rest/v3/enrollment/mail/mail/verification/otp")
    Call<String> enrollmentMailVerificationOTP (@Header("Authorization") String authorization,
                                                @Body MailVerificationOTPDTO validate);

    @POST ("rest/v3/enrollment/mail/validateOTP")
    Call<String> enrollmentMailValidateOTP (@Header("Authorization") String authorization,
                                                         @Body ValidateOtpDTO validate);



    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Multipart
    @POST("rest/v3/enrollment/biometric/minuciasCyphered")
    Call<ResponseServicesBID> enrollmentBiometricMinuciasCyphered (@Header("Authorization") String authorization,
                                                                   @Part MultipartBody.Part jsonFile);

    @POST("rest/v3/enrollment/biometric/search/customerIdCyphered")
    Call<ResponseServicesBID> enrollmentBiometricSearchCustomerIdCyphered
                                                                   (@Header("Authorization") String authorization,
                                                                    @Body FingerLoginDTO jsonFile);

    @POST("rest/unauth/enrollment/biometric/search/customerIdCyphered")
    Call<ResponseServicesBID> unauthEnrollmentBiometricSearchCustomerIdCyphered
                                                                   (@Body FingerLoginDTO jsonFile);

    @POST ("rest/v3/enrollment/contract/acceptancePerCustomer")
    Call<List<FieldsContractDTO>> enrollmentGetContractAcceptance ( @Header("Authorization") String authorization,
                                                                    @Body RequestFeldContractDTO operationId);

    @GET ("rest/v3/enrollment/contractCert/certContract/{idOperation}")
    Call<ResponseBody> enrollmentCertContract (@Header("Authorization") String authorization,
                                               @Path  ("idOperation") String id);

    @GET ("rest/unauth/enrollment/contractCert/certContractDemo/{nombre}/{curp}/{firma}/{domicilio}")
    Call<ResponseBody> enrollmentGetDemoContrat (@Header("Content-Type") String type,
                                                 @Path("nombre") String nombre,
                                                 @Path  ("curp") String curp,
                                                 @Path  ("firma") boolean isFirmar,
                                                 @Path ("domicilio") String domicilio);


    ////////////////////////////////////////////////////////////////////////////////////////////////

    @POST ("rest/v3/enrollment/mail/mail/verification/otp/resend")
    Call<ResponseBody> enrollmentMailVerificationOTPResend  (@Header("Authorization") String authorization,
                                                       @Body ResendOtpDTO valueDTO);

    ////////////////////////////////////////////////////////////////////////////////////////////////


    @POST ("rest/unauth/enrollment/client/detail/detail")
    Call<DetailSearchDTO> enrollmentClientDetail (@Body FieldSearchDetailDTO valueDTO);

}