package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.teknei.bid.BaseAction;
import com.teknei.bid.R;
import com.teknei.bid.dialogs.MessageDialog;
import com.teknei.bid.domain.DetailSearchDTO;
import com.teknei.bid.utils.SharedPreferencesUtils;

public class BaseActivity extends AppCompatActivity implements BaseAction {

    @Override
    protected void onResume() {
        super.onResume();
        String token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");
        if (token.equals("")) {
            if (this instanceof SplashActivity) {
                /// ....
            } else {
                Intent end = new Intent(getApplicationContext(), SplashActivity.class);
                end.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(end);
                finish();
            }
        }
    }

    @Override
    public void cancelOperation() {
        String operationID = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");
        String token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");

        if (!operationID.equals("")) {
            SharedPreferencesUtils.cleanSharedPreferencesOperation(this);
        }

        String operationIDAux = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");

        if (operationIDAux.equals("")) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void logOut() {
        String operationID = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");
        String token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");

        if (!operationID.equals("")) {
            SharedPreferencesUtils.cleanSharedPreferencesOperation(this);
        }

        if (!token.equals("")) {
            return;
        }

        if (token.equals("") && operationID.equals("")) {
            finish();
        }
    }

    @Override
    public void goNext() { }

    @Override
    public void sendPetition() { }

    @Override
    public void deleteFile () { }

    @Override
    public void showObtainedData (DetailSearchDTO value) { }

    @Override
    public void showErrorConection () {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MessageDialog dialogoAlert;
                dialogoAlert = new MessageDialog (BaseActivity.this, getString(R.string.message_ws_notice), "Se esta presentando inestabilidad en la conexión. Por favor, verifique su punto de red.");
                dialogoAlert.setCancelable(false);
                dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogoAlert.show();
            }
        });
    }
}
