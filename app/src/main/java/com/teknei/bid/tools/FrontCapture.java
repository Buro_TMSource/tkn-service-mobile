package com.teknei.bid.tools;

public class FrontCapture {

    private byte[] bufferFrontCapture;
    private boolean isSend = false;
    private String similitud;

    private static FrontCapture instance;

    public static FrontCapture getInstance() {
        if (instance == null) {
            instance = new FrontCapture();
        }
        return instance;
    }

    private FrontCapture() {
    }

    public byte[] getBufferFrontCapture() {
        return bufferFrontCapture;
    }

    public void setBufferFrontCapture(byte[] bufferFrontCapture){
        this.bufferFrontCapture = bufferFrontCapture;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public String getSimilitud() {
        return similitud;
    }

    public void setSimilitud(String similitud) {
        this.similitud = similitud;
    }
}
