package com.teknei.bid.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;

import com.teknei.bid.R;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.PermissionsUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

public class SelectIdTypeActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    Button ifeCButton;
    Button ifeDButton;
    Button ineButton;
    Button passportButton;
    Button licenseButton;

    boolean credentialProvider = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_id_type);


        ifeCButton = (Button) findViewById(R.id.b_ife_c_select_id);
        ifeDButton = (Button) findViewById(R.id.b_ife_d_select_id);
        ineButton = (Button) findViewById(R.id.b_ine_select_id);
        passportButton = (Button) findViewById(R.id.b_passport_select_id);
        licenseButton = (Button) findViewById(R.id.b_licence_select_id);

        passportButton.setEnabled(false);
        licenseButton.setEnabled(false);

        ifeCButton.setOnClickListener(this);
        ifeDButton.setOnClickListener(this);
        ineButton.setOnClickListener(this);
        passportButton.setOnClickListener(this);
        licenseButton.setOnClickListener(this);


        //Mostrar solo INE
        ifeDButton.setVisibility(View.GONE);
        ifeCButton.setText(getString(R.string.ife_select_id_type));


        //Check Permissions For Android 6.0 up
        PermissionsUtils.checkPermissionCamera(this);
        PermissionsUtils.checkPermissionReadWriteExternalStorage(this);
    }

    @Override
    public void onClick(View view) {
        Intent i;
        /*Ir por Icar siempre*/
        i = new Intent(this, IcarScanActivity.class);

        //Create the bundle
        Bundle bundle = new Bundle();

        SharedPreferencesUtils.saveToPreferencesString(SelectIdTypeActivity.this, SharedPreferencesUtils.TYPE_ID,ApiConstants.STRING_IFE);
        bundle.putString("id_type", ApiConstants.STRING_IFE);

        switch (view.getId()) {
            case R.id.b_ife_c_select_id:


                if (credentialProvider) {
                    SharedPreferencesUtils.saveToPreferencesString(SelectIdTypeActivity.this, SharedPreferencesUtils.TYPE_ID,ApiConstants.STRING_IFE);
                    bundle.putString("id_type", ApiConstants.STRING_IFE);
                }
                //Add the bundle to the intent
                i.putExtras(bundle);
                break;
            case R.id.b_ife_d_select_id:
                if (credentialProvider) {
                    SharedPreferencesUtils.saveToPreferencesString(SelectIdTypeActivity.this, SharedPreferencesUtils.TYPE_ID,ApiConstants.STRING_IFE);
                    bundle.putString("id_type", ApiConstants.STRING_IFE);
                }
                //Add the bundle to the intent
                i.putExtras(bundle);
                break;
            case R.id.b_ine_select_id:
                if (credentialProvider) {
                    SharedPreferencesUtils.saveToPreferencesString(SelectIdTypeActivity.this, SharedPreferencesUtils.TYPE_ID,ApiConstants.STRING_INE);
                    bundle.putString("id_type", ApiConstants.STRING_INE);
                }
                //Add the bundle to the intent
                i.putExtras(bundle);
                break;
            case R.id.b_passport_select_id:
                if (credentialProvider) {
                    SharedPreferencesUtils.saveToPreferencesString(SelectIdTypeActivity.this, SharedPreferencesUtils.TYPE_ID,ApiConstants.STRING_PASSPORT);
                    bundle.putString("id_type", ApiConstants.STRING_PASSPORT);
                }
                //Add the bundle to the intent
                i.putExtras(bundle);
                break;
            case R.id.b_licence_select_id:
                break;
        }
        startActivity(i);
    }

    //menu actions
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.i_close_operation_menu) {
            AlertDialog dialogoAlert;
            dialogoAlert = new AlertDialog(SelectIdTypeActivity.this, getString(R.string.message_close_operation_title), getString(R.string.message_close_operation_alert), ApiConstants.ACTION_CANCEL_OPERATION);
            dialogoAlert.setCancelable(false);
            dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogoAlert.show();
        }
        if (id == R.id.i_log_out_menu) {
            AlertDialog dialogoAlert;
            dialogoAlert = new AlertDialog(SelectIdTypeActivity.this, getString(R.string.message_title_logout), getString(R.string.message_message_logout), ApiConstants.ACTION_LOG_OUT);
            dialogoAlert.setCancelable(false);
            dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogoAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (isChecked) {
            credentialProvider = true;

        } else {
            credentialProvider = true;//para que inicie icarscan activity; false si es idScan
            ifeDButton.setVisibility(View.VISIBLE);
            ifeCButton.setText(getString(R.string.ife_c_select_id_type));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.CAMERA_REQUEST_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    SelectIdTypeActivity.this.onBackPressed();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case PermissionsUtils.WRITE_READ_EXTERNAL_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //Check AGAIN Permissions For Android 6.0 up
                    PermissionsUtils.checkPermissionWriteExternalStorage(SelectIdTypeActivity.this);
                }

                if (grantResults.length > 1
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //Check AGAIN Permissions For Android 6.0 up
                    PermissionsUtils.checkPermissionReadExternalStorage(SelectIdTypeActivity.this);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onBackPressed() {
    }
}
