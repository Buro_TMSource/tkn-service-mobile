package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teknei.bid.R;
import com.teknei.bid.asynctask.GetContract;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

import java.io.File;

public class ResultOperationActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "ResultOperationActivity";

    Button finishOperation;
    Button tryAgainOperation;
    Button contractGenerate;
    Button contractSing;
    TextView tvOperationResult;

    ProgressBar progressBar;
    ImageView imageView;
    private String actividad = "";

    private String operationID = "";
    private int    typePerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actividad = getIntent().getExtras().getString("actividad");
        setContentView(R.layout.activity_result_operation);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.result_operation_activity_name));
            invalidateOptionsMenu();
        }

        operationID = SharedPreferencesUtils.readFromPreferencesString(this,SharedPreferencesUtils.OPERATION_ID,"");
        typePerson  = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TYPE_PERSON, ""));

        finishOperation   = (Button) findViewById(R.id.b_end_result_operation);
        tryAgainOperation = (Button) findViewById(R.id.b_end_result_operation);
        contractGenerate  = (Button) findViewById(R.id.b_contract_generate_result_operation);
        contractSing      = (Button) findViewById(R.id.b_contract_sign_result_operation);

        tvOperationResult = (TextView) findViewById(R.id.tv_operation_result);

        progressBar = findViewById(R.id.send_filenet_pb);
        imageView = findViewById(R.id.ok_img_view);

        finishOperation.setOnClickListener(this);
        tryAgainOperation.setOnClickListener(this);
        contractGenerate.setOnClickListener(this);
        contractSing.setOnClickListener(this);

        tvOperationResult.setText(operationID);

        if (actividad.equals("finalizar")) {
            finishOperation.setText("FINALIZAR PROCESO");
        }
        else {
            finishOperation.setText("CONTINUAR");
        }


        if (typePerson == ApiConstants.TYPE_OPERATOR) {
            contractGenerate.setVisibility(View.GONE);
            contractSing.setVisibility(View.GONE);
        }

        esperar();


    }

    private void changeImage(){
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
    }

    private void esperar(){
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                changeImage();
            }
        },
        5000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.b_end_result_operation:

                if (actividad.equals("finalizar")) {

                    SharedPreferencesUtils.cleanSharedPreferencesOperation(ResultOperationActivity.this);

                    File folderImge   = new File(ApiConstants.FOLDER_IMGE);

                    if (!folderImge.exists()) {
                        Log.d(TAG, "----> Eliminación de directorio destino " + folderImge.getPath());
                        folderImge.delete();
                    }
                    cleanBegining();
                    Intent end;
                    if(ApiConstants.GoToVideoCall.equals("SI"))
                        end = new Intent(ResultOperationActivity.this, SelectActivity.class);
                    else
                        end = new Intent(ResultOperationActivity.this, SelectTaskActivity.class);

                    startActivity(end);
                    finish();

                }else {
                    Intent end = new Intent(ResultOperationActivity.this, kycActivity.class);
                    startActivity(end);
                }
                break;

            case R.id.b_try_again_result_operation:
                break;

            case R.id.b_contract_generate_result_operation:
                //Add the bundle to the intent
                String token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");
                operationID = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");
                new GetContract(ResultOperationActivity.this, token, Integer.valueOf(operationID)).execute();
                break;

            case R.id.b_contract_sign_result_operation:

                break;
        }
    }

    @Override
    public void goNext() {
    }

    private void cleanBegining(){
        String uuid      = PhoneSimUtils.getSerialNuber();
        SharedPreferencesUtils.cleanSharedPreferencesOperation(this);
        saveSharedPreferenceByDefault();

        SharedPreferencesUtils.saveToPreferencesString
                (ResultOperationActivity.this,SharedPreferencesUtils.TYPE_PERSON, ApiConstants.TYPE_CUSTOMER+"");

        SharedPreferencesUtils.saveToPreferencesString
                (ResultOperationActivity.this,SharedPreferencesUtils.CUSTOMER_TYPE,ApiConstants.TYPE_CUSTOMER+"");

        SharedPreferencesUtils.saveToPreferencesString
                (ResultOperationActivity.this,SharedPreferencesUtils.CUSTOM_COMPANY,ApiConstants.CUSTOM_HSBC+"");

        SharedPreferencesUtils.saveToPreferencesString
                (ResultOperationActivity.this,SharedPreferencesUtils.ByPasAPI,ApiConstants.ByPasAPI+"");

        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this,SharedPreferencesUtils.ID_DEVICE,uuid);
    }

    public void saveSharedPreferenceByDefault() {
        String urlIdScan         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_ID_SCAN, getString(R.string.default_url_id_scan));
        String licenseIdScan     = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.LICENSE_ID_SCAN, getString(R.string.default_license_id_scan));
        String urlTeknei         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_TEKNEI, getString(R.string.default_url_teknei));
        String urlMobbSign       = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_MOBBSIGN, getString(R.string.default_url_mobbsign));
        String licenseMobbSign   = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.MOBBSIGN_LICENSE, getString(R.string.default_license_mobbsign));
        String urlAuthAccess     = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_AUTHACCESS, getString(R.string.default_url_oauthaccess));
        String fingerprintReader = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.FINGERPRINT_READER, getString(R.string.default_fingerprint_reader));
        String idCompany         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.ID_ENTERPRICE, getString(R.string.default_id_company));
        String timeout           = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TIMEOUT_SERVICES, getString(R.string.default_timeout));

        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.URL_ID_SCAN, urlIdScan);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.LICENSE_ID_SCAN, licenseIdScan);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.URL_TEKNEI, urlTeknei);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.URL_MOBBSIGN, urlMobbSign);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.MOBBSIGN_LICENSE, licenseMobbSign);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.URL_AUTHACCESS, urlAuthAccess);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.FINGERPRINT_READER, fingerprintReader);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, idCompany);
        SharedPreferencesUtils.saveToPreferencesString(ResultOperationActivity.this, SharedPreferencesUtils.TIMEOUT_SERVICES, timeout);
    }

    @Override
    public void onBackPressed() { }
}
