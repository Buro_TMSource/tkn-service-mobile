package com.teknei.bid.asynctask;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.activities.IcarScanActivity;
import com.teknei.bid.dialogs.ProgressDialog;
import com.teknei.bid.domain.SendIdOcrDTO;
import com.teknei.bid.response.OcrNapiResponse;
import com.teknei.bid.response.ResponseServicesBID;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.services.NapiEndPointServices;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CredentialsCaptured extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "CredentialsCaptured";

    private String token;
    private String jsonS;
    private List<File> jsonF;
    private String idType;
    boolean resolution = false;

    private Activity activityOrigin;
    private JSONObject responseJSONObject;
    private String errorMessage;
    private boolean responseOk = false;
    private ProgressDialog progressDialog;

    private boolean hasConecction = false;
    private boolean isNapi = true;
    private File jsonFile;
    private Integer responseStatus = 0;
    private SendIdOcrDTO sendIdOcrDTO;

    private long endTime;

    private ResponseServicesBID responseCredential;

    private Boolean dataExist;
    private String  msgError;

    public CredentialsCaptured (Activity context, File jsonFile, SendIdOcrDTO sendIdOcrDTO) {
        this.activityOrigin      = context;
        this.jsonFile = jsonFile;
        this.isNapi   = true;
        this.sendIdOcrDTO = sendIdOcrDTO;
    }

    public CredentialsCaptured(Activity context, String tokenOld, String jsonString, List<File> jsonFile, String idTypeIn) {
        this.activityOrigin = context;
        this.token = tokenOld;
        this.jsonS = jsonString;
        this.jsonF = jsonFile;
        this.idType = idTypeIn;
        this.isNapi=false;
    }

    @Override
    protected void onPreExecute() {

        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);

        ConnectivityManager check = (ConnectivityManager) activityOrigin.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = check.getAllNetworkInfo();
        for (int i = 0; i < info.length; i++) {
            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                hasConecction = true;
            }
        }
    }

    @Override
    protected Void doInBackground(String... params) {


        if (PhoneSimUtils.isNetDisponible(activityOrigin)) {

            Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
            while (System.currentTimeMillis() < endTime) {
                //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
            }
            Log.i("Wait", "timer finish : " + System.currentTimeMillis());

            if(isNapi){
                callServicesNapi();
            }else {
                callServicesBid();
            }



        } else {

            ((BaseActivity) activityOrigin).showErrorConection();
        }

        return null;
    }

    // Call Service Napi
    public void callServicesNapi () {

        Call<OcrNapiResponse> call;

        String endPoint = "https://www.napi.buroidentidad.com/";

        int    timeOut  = Integer.parseInt("120");

        Log.v("END POINT         :", "endpoint     :" + endPoint);

        NapiEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(NapiEndPointServices.class);

        String stringAuthorization = Credentials.basic("buroidentidad","_9x3Ro.Xp");

        call = api.servicesOCRgetData("application/json",stringAuthorization, sendIdOcrDTO);

        Log.d(CLASS_NAME,         call.request().toString() + "----------------------");

        call.enqueue(new Callback<OcrNapiResponse>() {

            @Override
            public void onResponse(Call<OcrNapiResponse> call, Response<OcrNapiResponse> response) {

                OcrNapiResponse responseOcr = new OcrNapiResponse();
                responseStatus = response.code();
                if(response.body() != null){
                    responseOcr = response.body();
                }

                Log.e("RESULTADO OCR:", new Gson().toJson(response.body()));

                if (responseStatus >= 200 && responseStatus < 300) {

                    dataExist = response.isSuccessful();
                    msgError = response.body().getEstatus();

                    if( msgError == null ){
                        responseOk = true;

                        String messageComplete = "";
                        String messageResp = "";
                        String jsonResult = "";
                        String auxCurp = "";
                        String name = "";
                        String apPat = "";
                        String apMat = "";
                        String address = "";
                        String mrz = "";
                        String ocr = "";
                        String validity = "";
                        String curp = "";
                        String street   = "";
                        String suburb   = "";
                        String zipCode = "";
                        String locality = "";
                        String state    = "";

                        String subTipo= "";
                        if(responseOcr.getSubTipo() != null){
                            subTipo= responseOcr.getSubTipo();
                        }

                        Log.e("RESULTADO OCR:", "OBTENIENDO RESULTADOS-> " + subTipo);

                        switch (subTipo){
                            case "B":
                                Log.e("RESULTADO OCR:", "TIPO B");
                                name = responseOcr.getNombres();
                                apPat = responseOcr.getPrimerApellido();
                                apMat = responseOcr.getSegundoApellido();
                                address = responseOcr.getCalle();
                                ocr = responseOcr.getOcr();
                                //validity = responseOcr.getVigencia();
                                curp = responseOcr.getCurp();

                                street = responseOcr.getCalle();
                                suburb = responseOcr.getColonia();
                                locality = responseOcr.getLocalidad();
                                state = responseOcr.getEstado();

                                break;
                            case "C" :
                                Log.e("RESULTADO OCR:", "TIPO C");
                                name = responseOcr.getNombres();
                                apPat = responseOcr.getPrimerApellido();
                                apMat = responseOcr.getSegundoApellido();
                                address = responseOcr.getCalle();
                                ocr = responseOcr.getOcr();
                                validity = responseOcr.getVigencia();
                                curp = responseOcr.getCurp();

                                street = responseOcr.getCalle();
                                suburb = responseOcr.getColonia();
                                locality = responseOcr.getLocalidad();
                                state = responseOcr.getEstado();

                                break;
                            case "D":
                                Log.e("RESULTADO OCR:", "TIPO D");
                                name = responseOcr.getNombres();
                                apPat = responseOcr.getPrimerApellido();
                                apMat = responseOcr.getSegundoApellido();
                                address = responseOcr.getCalle();
                                ocr = responseOcr.getOcr();
                                validity = responseOcr.getVigencia();
                                curp = responseOcr.getCurp();

                                street = responseOcr.getCalle();
                                suburb = responseOcr.getColonia();
                                locality = responseOcr.getLocalidad();
                                state = responseOcr.getEstado();


                                break;

                        }

                        if(!(curp.equals(""))  && !(name.equals(""))){
                            resolution = true;
                        }

                        if (resolution){
                            auxCurp = curp;
                            String jsonString = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, "{}");

                            try{
                                JSONObject jsonData = new JSONObject(jsonString);

                                jsonData.put("name",  name);
                                jsonData.put("appat", apPat);
                                jsonData.put("apmat", apMat);

                                if (!curp.equals(""))
                                    jsonData.put("curp", curp);

                                jsonData.put("mrz", mrz);
                                jsonData.put("ocr", ocr);
                                jsonData.put("address", address);

                                jsonData.put("street", street);
                                jsonData.put("suburb", suburb);
                                jsonData.put("zipCode", zipCode);
                                jsonData.put("locality", locality);
                                jsonData.put("state", state);

                                if(!validity.equals("")){
                                    String[] splited = validity.split("-");
                                    if(splited.length > 0){
                                        jsonData.put("validity", splited[0]);
                                    }
                                }else {
                                    jsonData.put("validity", validity);
                                }

                                SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.
                                        JSON_CREDENTIALS_RESPONSE, jsonData.toString());

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }


                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SCAN_SAVE_ID, ApiConstants.SCANAUX);

                        if(!auxCurp.equals("")){
                            String jsonFormString = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.JSON_INIT_FORM, "{}");

                            JSONObject jsonFormObject = null;
                            String formCurp = "";

                            try {
                                jsonFormObject = new JSONObject(jsonFormString);
                                formCurp = jsonFormObject.optString("curp");
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }


                    } else {
                        errorMessage = response.body().getMensaje();
                        Log.i("Message credentials", "credentials: " + errorMessage);
                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_ID, errorMessage);
                    }

                } else {

                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage = activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 400 && responseStatus < 500) {

                        if (responseStatus == 404) {

                            errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                        } else {
                            errorMessage = activityOrigin.getString(R.string.message_ws_response_400);
                        }

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        if (responseStatus == 504) {

                            errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                        } else {

                            errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                        }
                    }
                    SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_ID, errorMessage);

                }
            }

            @Override
            public void onFailure(Call<OcrNapiResponse> call, Throwable t) {
                t.printStackTrace();
                SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_ID, activityOrigin.getString(R.string.msg_error_conexion));
            }
        });
    }

    public void callServicesBid(){

        Call<ResponseServicesBID> call;

        String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

        Log.v("token             :", "token        :" + token);
        Log.v("NUM FILES         :", "NUM FILES    :" + jsonF.size());
        Log.v("json SEND NO File :", "json no file :" + jsonS);
        Log.v("END POINT         :", "endpoint     :" + endPoint);

        BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

        MultipartBody.Part jsonBody =
                MultipartBody.Part.createFormData("json", jsonF.get(0).getName(),
                        RequestBody.create(MediaType.parse("application/json"), jsonF.get(0)));

        MultipartBody.Part jsonFront =
                MultipartBody.Part.createFormData("file", jsonF.get(1).getName(),
                        RequestBody.create (MediaType.parse("image/jpg"), jsonF.get(1)));

        if (idType.equals(ApiConstants.STRING_PASSPORT)) {

            call = api.enrollmentCredential(token, jsonBody, jsonFront, null);

        } else {

            MultipartBody.Part jsonBack =
                    MultipartBody.Part.createFormData("file", jsonF.get(2).getName(),
                            RequestBody.create(MediaType.parse("image/jpg"), jsonF.get(2)));

            call = api.enrollmentCredential(token, jsonBody, jsonFront, jsonBack);

        }

        call.enqueue(new Callback<ResponseServicesBID>() {

            @Override
            public void onResponse(Call<ResponseServicesBID> call, Response<ResponseServicesBID> response) {

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (response.body() != null) {
                    responseCredential = response.body();
                }

                if ((responseStatus >= 200 && responseStatus < 300)) {

                    dataExist = responseCredential.isResultOK();
                    msgError  = responseCredential.getErrorMessage();

                    if (dataExist) {

                        responseOk = true;

                        String messageComplete = "";
                        String messageResp = "";
                        String jsonResult = "";
                        String auxCurp = "";

                        try {
                            messageComplete = responseCredential.getErrorMessage();
                            String messageSplit[] = messageComplete.split("\\|");
                            messageResp = messageSplit[0];

                            if (messageSplit.length > 1) {
                                String name = "";
                                String apPat = "";
                                String apMat = "";
                                String address = "";
                                String mrz = "";
                                String ocr = "";
                                String validity = "";
                                String curp = "";
                                String street   = "";
                                String suburb   = "";
                                String zipCode = "";
                                String locality = "";
                                String state    = "";

                                jsonResult = messageSplit[1];

                                Log.d("Response Message", "json:" + jsonResult);

                                if (jsonF.size() == 1) {
                                    //MOBBSCAN Unicamente INE *************************************************************************
                                    JSONObject respJSON = new JSONObject(jsonResult);
                                    JSONObject dataObjectJSON = respJSON.getJSONObject("document");
                                    try {
                                        name = dataObjectJSON.getString("name");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        apPat = dataObjectJSON.getString("firstSurname");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        apMat = dataObjectJSON.getString("secondSurname");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        street = dataObjectJSON.getString("call");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        suburb = dataObjectJSON.getString("col");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        zipCode = dataObjectJSON.getString("cp");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        locality = dataObjectJSON.getString("muni");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        state = dataObjectJSON.getString("esta");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        mrz = dataObjectJSON.getString("mrz");
                                        Log.w("MRZ", "MRZ : " + mrz);
                                        String mrzSplit3 = "";
                                        if (mrz.length() > 31) {
                                            mrzSplit3 = mrz.substring(0, 30);
                                        }
                                        Log.w("MRZ split", "MRZ split 3: " + mrzSplit3);
                                        String firstSplit2[] = mrzSplit3.split("\\<\\<");
                                        if (firstSplit2.length > 1) {
                                            ocr = firstSplit2[1];
                                            Log.w("MRZ OCR", "OCR: " + ocr);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        validity = dataObjectJSON.getString("dateOfExpiry");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (jsonF.size() == 3 || jsonF.size() == 2) {
                                    //ICAR
                                    JSONObject respJSON = new JSONObject(jsonResult);
                                    JSONObject dataObjectJSON = respJSON.getJSONObject("document");
                                    switch (idType) {
                                        case ApiConstants.STRING_INE:
                                            name     = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_NAME);
                                            apPat    = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_FIRST_SURNAME);
                                            apMat    = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_SECOND_SURNAME);
                                            address  = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_ADDRESS);
                                            mrz      = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_MRZ);
                                            ocr      = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_OCR);
                                            validity = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_VALIDITY);
                                            curp     = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_CURP);

                                            street   = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_STREET);
                                            suburb   = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_SUBURB);
                                            zipCode  = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_ZIPCODE);
                                            locality = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_LOCALITY);
                                            state    = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_STATE);

                                            break;

                                        case ApiConstants.STRING_IFE:
                                            name    = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_NAME);
                                            apPat   = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_FIRST_SURNAME);
                                            apMat   = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_SECOND_SURNAME);
                                            address = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_ADDRESS);
                                            mrz     = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_MRZ);  //IFE Tipo B y C no tiene mrz
                                            ocr     = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_OCR);

                                            validity = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_VALIDITY); //IFE Tipo B no tiene vigencia
                                            curp     = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_CURP); //IFE no tiene dato curp

                                            street   = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_STREET);
                                            suburb   = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_SUBURB);
                                            zipCode  = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_ZIPCODE);
                                            locality = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_LOCALITY);
                                            state    = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_STATE);

                                            break;

                                        case ApiConstants.STRING_PASSPORT:
                                            name = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_NAME);
                                            String completeSurname = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_SURNAME);

                                            if (completeSurname != null && !completeSurname.equals("")) {
                                                String[] splited = completeSurname.split("\\s+");
                                                if (splited.length > 0) {
                                                    apPat = splited[0];
                                                    if (splited.length > 1) {
                                                        apMat = splited[1];
                                                    }
                                                }
                                            }

                                            mrz      = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_MRZ);  //
                                            ocr      = mrz;
                                            validity = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_PASSPORT_VALIDITY);
                                            curp     = getStringObjectJSON(dataObjectJSON, ApiConstants.ICAR_CURP);
                                            break;
                                    }
                                }
                                if ((!mrz.equals("") || !ocr.equals("")) && !name.equals("")) {
                                    resolution = true;
                                }

                                if (resolution) {
                                    auxCurp = curp;
                                    String jsonString = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, "{}");
                                    try {
                                        JSONObject jsonData = new JSONObject(jsonString);

                                        jsonData.put("name",  name);
                                        jsonData.put("appat", apPat);
                                        jsonData.put("apmat", apMat);

                                        if (!curp.equals(""))
                                            jsonData.put("curp", curp);

                                        jsonData.put("mrz", mrz);
                                        jsonData.put("ocr", ocr);
                                        jsonData.put("address", address);

                                        jsonData.put("street", street);
                                        jsonData.put("suburb", suburb);
                                        jsonData.put("zipCode", zipCode);
                                        jsonData.put("locality", locality);
                                        jsonData.put("state", state);

                                        if (!validity.equals("")) {

                                            String[] splited = validity.split("-");
                                            if (splited.length > 0) {
                                                jsonData.put("validity", splited[0]);
                                            }

                                        } else {
                                            jsonData.put("validity", validity);
                                        }

                                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, jsonData.toString());

                                        Log.d(CLASS_NAME,"JSON " + jsonData.toString());

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SCAN_SAVE_ID, ApiConstants.SCANAUX);

                        if (!auxCurp.equals("")) {
                            String jsonFormString =
                                    SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.JSON_INIT_FORM, "{}");
                            JSONObject jsonFormObject = null;
                            String formCurp = "";

                            try {
                                jsonFormObject = new JSONObject(jsonFormString);
                                formCurp = jsonFormObject.optString("curp");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SUCCESS_STORAGE_ID,ApiConstants.SUCCESS_STORAGE_ID);
                        ((IcarScanActivity) activityOrigin).deleteFile();

                    } else {

                        errorMessage = ApiConstants.managerErrorServices (Integer.parseInt(msgError),activityOrigin);

                        Log.i("Message credentials", "credentials: " + errorMessage);

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_ID, errorMessage);
                    }

                } else {

                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage = activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 400 && responseStatus < 500) {

                        if (responseStatus == 404) {

                            errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                        } else if (responseStatus == 422) {

                            if (response.body() == null) {

                                errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                            } else {

                                responseCredential = response.body();

                                if (responseCredential.getErrorMessage().length() > 6) {

                                    errorMessage = responseCredential.getErrorMessage();

                                } else {

                                    errorMessage = ApiConstants.managerErrorServices(Integer.parseInt(responseCredential.getErrorMessage()), activityOrigin);

                                }
                            }
                        } else {
                            errorMessage = activityOrigin.getString(R.string.message_ws_response_400);
                        }

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        if (responseStatus == 504) {

                            errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                        } else {

                            errorMessage = "Falla al procesar identificación.\nCapture la identificación nuevamente.";

                        }
                    }
                    SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_ID, errorMessage);
                }
            }

            @Override
            public void onFailure(Call<ResponseServicesBID> call, Throwable t) {

                t.printStackTrace();

                SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_ID, activityOrigin.getString(R.string.msg_error_conexion));

            }
        });

    }

    public String getStringObjectJSON(JSONObject jsonObject, String jsonName) {
        String objString = "";
        objString = jsonObject.optString(jsonName);
        return objString;
    }


    public void goNext() {
        ((BaseActivity) activityOrigin).goNext();
    }
}
