package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rgarciav on 14/03/2018.
 */

public class RequestFeldContractDTO {

    @SerializedName("operationId")
    Integer operationId;

    public RequestFeldContractDTO(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }
}
