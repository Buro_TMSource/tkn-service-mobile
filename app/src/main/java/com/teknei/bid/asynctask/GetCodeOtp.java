package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.dialogs.DataValidation;
import com.teknei.bid.dialogs.ProgressDialog;
import com.teknei.bid.domain.ResendOtpDTO;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCodeOtp extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "GetCodeOtp";

    private ProgressDialog  progressDialog;
    private ResendOtpDTO    valueDTO;
    private Activity        activityOrigin;
    private String          token;
    private Integer         responseStatus = 0;
    private long            endTime;

    public GetCodeOtp (Activity activityOrigin, String token, Integer idClient) {
        this.token          = token;
        this.activityOrigin = activityOrigin;
        this.valueDTO       = new ResendOtpDTO(idClient);
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog( activityOrigin, "Reenvio de código de validación");
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();

        endTime = System.currentTimeMillis() + 1000;
        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);
    }

    @Override
    protected Void doInBackground(String... strings) {
        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());

        String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

        Log.d(CLASS_NAME, "token        :" + token);
        Log.d(CLASS_NAME, "endpoint     :" + endPoint);
        Log.d(CLASS_NAME, "id operation :" + valueDTO.getIdClient());

        BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

        Call<ResponseBody> call = api.enrollmentMailVerificationOTPResend(token, valueDTO);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressDialog.dismiss();

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (responseStatus >= 200 && responseStatus < 300) {

                    DataValidation dataValidation;
                    dataValidation = new DataValidation(activityOrigin, "Reenvio de Código",
                            "Envio de código satisfactorio");
                    dataValidation.setCancelable(false);
                    dataValidation.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dataValidation.show();

                } else {

                    DataValidation dataValidation;
                    dataValidation = new DataValidation(activityOrigin, "Reenvio de Código",
                            "Envio de código erroneo, vuelva a intentarlo");
                    dataValidation.setCancelable(false);
                    dataValidation.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dataValidation.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(CLASS_NAME, activityOrigin.getString(R.string.message_ws_response_500));

                progressDialog.dismiss();

                t.printStackTrace();

                DataValidation dataValidation;
                dataValidation = new DataValidation(activityOrigin, "Reenvio de Código",
                        activityOrigin.getString(R.string.msg_error_conexion));
                dataValidation.setCancelable(false);
                dataValidation.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dataValidation.show();
            }
        });
        return null;
    }
}