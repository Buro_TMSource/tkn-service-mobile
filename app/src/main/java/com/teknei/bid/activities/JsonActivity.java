package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.teknei.bid.R;
import com.teknei.bid.asynctask.GetIneValidation;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class JsonActivity extends BaseActivity implements View.OnClickListener {

    TextView jsontext;
    Button btnJson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
             super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_json);

        String jsonString = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, "{}");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar calendar = Calendar.getInstance();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        Random rand = new Random();
        int randomNum = rand.nextInt((1500 - 1400) + 1) + 1400;
        String coincidence ="REF000" + randomNum ; //Nyumeros aleatorios de coinicidencia entre 1400 y 1500

        Log.d("-----------::",jsonString);
        String nombre="";
        String appat="";
        String apmat="";
        String ocr="";
        String numeroEmision="02";
        String claveElector="";
        String consentimiento="true";
        String idInstitucion="TEST_FMP";
        String idEstacion="ESTACION1";
        String fechaHora=simpleDateFormat.format(calendar.getTime()).toString();
        String referencia=coincidence;
        String latitud="33.679183";
        String longitud="-17.76792";
        String codigoPostal="11510";
        String ciudad="CDMX";
        String estado="09";
        String minucia2="";
        String minucia7="\":\"/6D/ogARAP8BngEGAkzlBDkFAv5t/6QAOgkHAAky0yY7AArg8xqEAQpB7/G7AQuOJ2U/AAvheaTdAAku/1XSAQr5M9G2AQvyhx82AAomd9oL/6UBhQWr4ANp0AN++gNp0AN++gNp0AN++gNp0AN++gN14gONdgN00QOMLgNzJgOKLgNv0QOGLwNiUgN1/ANuTgOE/6E=";
        String nombreMAFI="MAFI MOVIL";
        String versionMAFI="V1.2";
        String tipoLector="4F";
        String idParametros="1234";
        String tiempoLectura="5000";
        String intentosLectura="1";

        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            try {
            nombre= jsonObject.getString("name");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                appat= jsonObject.getString("appat");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                apmat= jsonObject.getString("apmat");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
            ocr= jsonObject.getString("ocr");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
            claveElector= jsonObject.getString("mrz");
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsontext = findViewById(R.id.json_text);
        btnJson = findViewById(R.id.json_btn_continue);


        String json = "{}";

        try {
            json = jsonRequestObject(nombre,appat, apmat,ocr,numeroEmision,claveElector,consentimiento,idInstitucion,idEstacion,fechaHora,referencia,latitud,longitud,codigoPostal,ciudad,estado,minucia2,minucia7,nombreMAFI,versionMAFI,tipoLector,idParametros,tiempoLectura,intentosLectura).toString();


        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsontext.setText(json);

        btnJson.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        new GetIneValidation(JsonActivity.this).execute();
    }

    private JSONObject jsonRequestObject(String nombre,String appat, String apmat,String ocr, String numeroEmision, String claveElector,String  consentimiento,String idInstitucion,        String idEstacion,        String fechaHora,        String referencia,        String latitud,        String longitud,        String codigoPostal,        String ciudad,        String estado,        String minucia2,        String minucia7, String nombreMAFI,       String         versionMAFI,       String tipoLector,       String idParametros,       String tiempoLectura,       String intentosLectura) throws JSONException {
        JSONObject jo = new JSONObject();
        JSONObject objversionJSON= new JSONObject();
        JSONObject objdatosCliente= new JSONObject();
        JSONObject objdatosInstitucion= new JSONObject();
        JSONObject objminucia2= new JSONObject();
        JSONObject objminucia7= new JSONObject();
        JSONObject objDatosMAFI= new JSONObject();

        objversionJSON.put("versionJSON", "3.0");

        objdatosCliente.put("nombre",nombre);
        objdatosCliente.put("apellidoPaterno",appat);
        objdatosCliente.put("apellidoMaterno",apmat);
        objdatosCliente.put("ocr", ocr);
        objdatosCliente.put("numeroEmision", claveElector);
        objdatosCliente.put("claveElector", claveElector);
        objdatosCliente.put("consentimiento", consentimiento);

        objdatosInstitucion.put("idInstitucion",idInstitucion);
        objdatosInstitucion.put("idEstacion",idEstacion);
        objdatosInstitucion.put("fechaHora",fechaHora);
        objdatosInstitucion.put("referencia",referencia);
        objdatosInstitucion.put("latitud",latitud);
        objdatosInstitucion.put("longitud",longitud);
        objdatosInstitucion.put("codigoPostal",codigoPostal);
        objdatosInstitucion.put("ciudad",ciudad);
        objdatosInstitucion.put("estado",estado);

        objminucia2.put("minucia2",minucia2);
        objminucia7.put("minucia7",minucia7);

        objDatosMAFI.put("nombreMAFI",nombreMAFI);
        objDatosMAFI.put("versionMAFI",versionMAFI);
        objDatosMAFI.put("tipoLector",tipoLector);
        objDatosMAFI.put("idParametros",idParametros);
        objDatosMAFI.put("tiempoLectura",tiempoLectura);
        objDatosMAFI.put("intentosLectura",intentosLectura);

        jo.put("versionJSON","3.0" );
        jo.put("datosCliente", objdatosCliente);
        jo.put("datosInstitucion", objdatosInstitucion);
        jo.put("minucia2", minucia2);
        jo.put("minucia7", minucia7);
        jo.put("datosMAFI", objDatosMAFI);

        return jo;
    }

    @Override
    public void goNext() {
        Intent i = new Intent(JsonActivity.this, IneConsultResultActivity.class);
        startActivity(i);
    }
}
