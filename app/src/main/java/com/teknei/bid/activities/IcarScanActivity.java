package com.teknei.bid.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.view.Gravity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.icarvision.icarsdk.IcarSDK_Settings;
import com.icarvision.icarsdk.idCloudConnection.IdCloud_LocalImages;
import com.icarvision.icarsdk.newCapture.IcarCapture;
import com.icarvision.icarsdk.newCapture.IcarCapture_Configuration;
import com.icarvision.icarsdk.utils.IcarSDK_Utils;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;
import com.teknei.bid.R;
import com.teknei.bid.asynctask.CredentialsCaptured;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.domain.SendIdOcrDTO;
import com.teknei.bid.tools.FrontCapture;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.AppDataCaptured;
import com.teknei.bid.utils.PermissionsUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import static com.icarvision.icarsdk.newCapture.IcarCapture_Configuration.IcarCaptureImage.IMAGE_BACK;
import static com.icarvision.icarsdk.newCapture.IcarCapture_Configuration.IcarCaptureImage.IMAGE_FRONT;

public class IcarScanActivity extends BaseActivity implements View.OnClickListener {

    final int REQUEST_CODE_ASK_PERMISSIONS                  = 3434;
    final int REQUEST_CODE_CAPTURE_IMAGE                    = 150;

    final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_FRONTAL   = 661;
    final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_POSTERIOR = 662;
    final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_EVIDENCE  = 663;
    final int CAPTURE_IMAGE_INGRESOS1 = 664;
    final int CAPTURE_IMAGE_INGRESOS2 = 665;
    final int CAPTURE_IMAGE_INGRESOS3 = 666;
    final int CAPTURE_IMAGE_INGRESOS4 = 667;
    final int CAPTURE_IMAGE_INGRESOS5 = 668;
    final int CAPTURE_IMAGE_INGRESOS6 = 669;
    final int CAPTURE_IMAGE_INGRESOS7 = 670;
    final int CAPTURE_IMAGE_INGRESOS8 = 671;
    int OK_todo = 10;

    final int QUALITY_COMPRESION                          = 100;

    int CURRENT_IMAGE_CAPTURE                       =1;

    IcarCapture_Configuration mIcarCapture_Conf = new IcarCapture_Configuration();

    Button continueButton;
    Button btnAnverso;
    Button btnReverso;
    Button btnEvidence;

    Button btn_ingresos;
    RadioGroup radioGrupo;
    RadioButton rb_semana, rb_quince, rb_mes;
    ImageView imageIngresos;
    ImageView imageIngresos2;
    ImageView imageIngresos3;
    ImageView imageIngresos4;
    ImageView imageIngresos5;
    ImageView imageIngresos6;
    ImageView imageIngresos7;
    ImageView imageIngresos8;
    HorizontalScrollView scrollimagenes;
    TextView volverCaptura;


    ImageView imageFront;
    ImageView imageBack;
    ImageView imageEvidence;
    TextView  instructionsTV;

    Uri uri;


    String strPathSourceFile = "";
    String operationID       = "";
    String idEnterprice      = "";
    String customerType      = "";
    String token             = "";
    String strCrentialType   = "";

    private byte[] photoBufferFront;
    private byte[] photoBufferBack;

    File imageFileBack;
    File imageFileFront;
    File imageFileEvidence;
    File imagen1;
    File imagen2;
    File imagen3;
    File imagen4;
    File imagen5;
    File imagen6;
    File imagen7;
    File imagen8;
    private byte[] photoBuffer;

    File fileJson;
    File FotosfileJson;
    List<File> fileList;

    int resolution   = 0;
    int code_image   = 0;
    int request_code = 0;
    int request_code2 = 0;

    boolean onePicture = false;
    private String jsonString = "";
    int REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_scan);

        IcarSDK_Settings.ICAR_LICENSE_KEY_RESULT result = IcarSDK_Settings.getInstance().setIcarLicenseKey(getBaseContext(), ApiConstants.ICAR_KEY);

        if (result != IcarSDK_Settings.ICAR_LICENSE_KEY_RESULT.VALID_LICENSE)
        {
            String message = "";
            switch (result)
            {
                case INVALID_CONTEXT:
                    message = "Contexto no valido";
                    break;
                case INVALID_DATE:
                    message = "Fecha no valida";
                    break;
                case INVALID_PACKAGE_NAME:
                    message = "Nombre de paquete no valido";
                    break;
                case INVALID_FORMAT:
                    message = "Formato no valido";
                    break;
            }

            new android.app.AlertDialog.Builder(IcarScanActivity.this)
                    .setTitle("Error con la licencia de ICAR")
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        continueButton    = (Button) findViewById(R.id.b_continue_id_scan);
        btnAnverso        = (Button) findViewById(R.id.ids_btn_front);
        btnReverso        = (Button) findViewById(R.id.ids_btn_back);
        btnEvidence       = (Button) findViewById(R.id.ids_btn_evidence);
        imageFront        = (ImageView) findViewById(R.id.imageViewIdFront);
        imageBack         = (ImageView) findViewById(R.id.imageViewIdBack);
        imageEvidence     = (ImageView) findViewById(R.id.imageViewIdEvidence);
        instructionsTV    = (TextView) findViewById(R.id.tv_id_scan_instructions);

        btn_ingresos = (Button) findViewById(R.id.btn_ingresos);
        radioGrupo = (RadioGroup) findViewById(R.id.radioGrupo);
        rb_semana = (RadioButton) findViewById(R.id.rb_semana);
        rb_quince = (RadioButton) findViewById(R.id.rb_quince);
        rb_mes = (RadioButton) findViewById(R.id.rb_mes);
        btn_ingresos.setOnClickListener(this);
        rb_semana.setOnClickListener(this);
        rb_quince.setOnClickListener(this);
        rb_mes.setOnClickListener(this);
        scrollimagenes = (HorizontalScrollView) findViewById(R.id.scrollimagenes);
        imageIngresos        = (ImageView) findViewById(R.id.ingresos1);
        imageIngresos2        = (ImageView) findViewById(R.id.ingresos2);
        imageIngresos3        = (ImageView) findViewById(R.id.ingresos3);
        imageIngresos4        = (ImageView) findViewById(R.id.ingresos4);
        imageIngresos5        = (ImageView) findViewById(R.id.ingresos5);
        imageIngresos6        = (ImageView) findViewById(R.id.ingresos6);
        imageIngresos7        = (ImageView) findViewById(R.id.ingresos7);
        imageIngresos8        = (ImageView) findViewById(R.id.ingresos8);
        volverCaptura = (TextView) findViewById(R.id.txt_volverCaptura);
        imageIngresos.setOnClickListener(this);
        imageIngresos2.setOnClickListener(this);
        imageIngresos3.setOnClickListener(this);
        imageIngresos4.setOnClickListener(this);
        imageIngresos5.setOnClickListener(this);
        imageIngresos6.setOnClickListener(this);
        imageIngresos7.setOnClickListener(this);
        imageIngresos8.setOnClickListener(this);
        scrollimagenes.setVisibility(View.INVISIBLE);
        imageIngresos.setVisibility(View.INVISIBLE);
        imageIngresos2.setVisibility(View.INVISIBLE);
        imageIngresos3.setVisibility(View.INVISIBLE);
        imageIngresos4.setVisibility(View.INVISIBLE);
        imageIngresos5.setVisibility(View.INVISIBLE);
        imageIngresos6.setVisibility(View.INVISIBLE);
        imageIngresos7.setVisibility(View.INVISIBLE);
        imageIngresos8.setVisibility(View.INVISIBLE);
        volverCaptura.setVisibility(View.INVISIBLE);


        btnEvidence.setEnabled(false);
        btnEvidence.setVisibility(View.GONE);

        continueButton.setOnClickListener(this);
        btnAnverso.setOnClickListener(this);
        btnReverso.setOnClickListener(this);
        btnEvidence.setOnClickListener(this);



        fileList = new ArrayList<File>();

        imageFileFront      = null;
        imageFileBack       = null;
        imageFileEvidence   = null;
        imagen1 = null;
        imagen2 = null;
        imagen3 = null;
        imagen4 = null;
        imagen5 = null;
        imagen6 = null;
        imagen7 = null;
        imagen8 = null;

        operationID     = SharedPreferencesUtils.readFromPreferencesString(IcarScanActivity.this, SharedPreferencesUtils.OPERATION_ID, "");
        idEnterprice    = SharedPreferencesUtils.readFromPreferencesString(IcarScanActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, "default");
        customerType    = SharedPreferencesUtils.readFromPreferencesString(IcarScanActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, "default");
        token           = SharedPreferencesUtils.readFromPreferencesString(IcarScanActivity.this, SharedPreferencesUtils.TOKEN_APP, "");
        strCrentialType = SharedPreferencesUtils.readFromPreferencesString(IcarScanActivity.this, SharedPreferencesUtils.TYPE_ID, "");

        modifyLayoutByIdSelected(strCrentialType);

        customizeIcarLayouts();

        //Check Permissions For Android 6.0 up
        PermissionsUtils.checkPermissionReadWriteExternalStorage(this);

        radioGrupo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if(checkedId == R.id.rb_semana){
                    Toast.makeText(getApplicationContext(),"Se requieren 8 fotos", Toast.LENGTH_SHORT).show();
                }else if(checkedId == R.id.rb_quince){
                    Toast.makeText(getApplicationContext(),"Se requieren 4 fotos", Toast.LENGTH_SHORT).show();
                }else if(checkedId == R.id.rb_mes){
                    Toast.makeText(getApplicationContext(),"Se requieren 2 fotos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ids_btn_front:
                if (hasCameraPermission()) {
                    request_code = CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_FRONTAL;
                    dispatchTakePictureIntent();
                }
                break;

            case R.id.ids_btn_back:
                if (hasCameraPermission()) {
                    request_code = CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_POSTERIOR;
                    dispatchTakePictureIntent();
                }
                break;

            case R.id.ids_btn_evidence:
                request_code = CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_EVIDENCE;
                break;

            case R.id.b_continue_id_scan:
                if (validarFotos() && validatePictureTake()) {
                    sendPetition();


                }
                break;

            case R.id.btn_ingresos:

                if(rb_semana.isChecked()) {

                    scrollimagenes.setVisibility(View.VISIBLE);
                    btn_ingresos.setVisibility(View.INVISIBLE);
                    btn_ingresos.getLayoutParams().height = 1;
                    rb_quince.setEnabled(false);
                    rb_mes.setEnabled(false);
                    imageIngresos.getLayoutParams().width = 250;
                    imageIngresos2.getLayoutParams().width = 250;
                    imageIngresos3.getLayoutParams().width = 250;
                    imageIngresos4.getLayoutParams().width = 250;
                    imageIngresos5.getLayoutParams().width = 250;
                    imageIngresos6.getLayoutParams().width = 250;
                    imageIngresos7.getLayoutParams().width = 250;
                    imageIngresos8.getLayoutParams().width = 250;
                    volverCaptura.setVisibility(View.VISIBLE);

                    if (CURRENT_IMAGE_CAPTURE == 1) {
                        request_code = CAPTURE_IMAGE_INGRESOS1;
                        Toast.makeText(this, "Foto 1 de 8", Toast.LENGTH_SHORT).show();
                        startScan(ScanConstants.OPEN_CAMERA);
                    }
                    imageIngresos.setVisibility(View.VISIBLE);
                    imageIngresos2.setVisibility(View.VISIBLE);
                    imageIngresos3.setVisibility(View.VISIBLE);
                    imageIngresos4.setVisibility(View.VISIBLE);
                    imageIngresos5.setVisibility(View.VISIBLE);
                    imageIngresos6.setVisibility(View.VISIBLE);
                    imageIngresos7.setVisibility(View.VISIBLE);
                    imageIngresos8.setVisibility(View.VISIBLE);

                }


                else if(rb_quince.isChecked()){

                    scrollimagenes.setVisibility(View.VISIBLE);
                    btn_ingresos.setVisibility(View.INVISIBLE);
                    btn_ingresos.getLayoutParams().height = 1;
                    rb_semana.setEnabled(false);
                    rb_mes.setEnabled(false);
                    imageIngresos.getLayoutParams().width = 250;
                    imageIngresos2.getLayoutParams().width = 250;
                    imageIngresos3.getLayoutParams().width = 250;
                    imageIngresos4.getLayoutParams().width = 250;
                    volverCaptura.setVisibility(View.VISIBLE);
                    btn_ingresos.setVisibility(View.INVISIBLE);
                    btn_ingresos.getLayoutParams().height = 1;

                    if (CURRENT_IMAGE_CAPTURE == 1) {
                        request_code = CAPTURE_IMAGE_INGRESOS1;
                        Toast.makeText(this, "Foto 1 de 4", Toast.LENGTH_SHORT).show();
                        startScan(ScanConstants.OPEN_CAMERA);

                    }
                    imageIngresos.setVisibility(View.VISIBLE);
                    imageIngresos2.setVisibility(View.VISIBLE);
                    imageIngresos3.setVisibility(View.VISIBLE);
                    imageIngresos4.setVisibility(View.VISIBLE);

                }
                else if(rb_mes.isChecked()){

                    scrollimagenes.setVisibility(View.VISIBLE);
                    btn_ingresos.setVisibility(View.INVISIBLE);
                    btn_ingresos.getLayoutParams().height = 1;
                    rb_semana.setEnabled(false);
                    rb_quince.setEnabled(false);
                    imageIngresos.getLayoutParams().width = 250;
                    imageIngresos2.getLayoutParams().width = 250;
                    volverCaptura.setVisibility(View.VISIBLE);
                    btn_ingresos.setVisibility(View.INVISIBLE);
                    btn_ingresos.getLayoutParams().height = 1;

                    if (CURRENT_IMAGE_CAPTURE == 1) {
                        request_code = CAPTURE_IMAGE_INGRESOS1;
                        Toast.makeText(this, "Foto 1 de 2", Toast.LENGTH_SHORT).show();
                        startScan(ScanConstants.OPEN_CAMERA);

                    }
                    imageIngresos.setVisibility(View.VISIBLE);
                    imageIngresos2.setVisibility(View.VISIBLE);

                }
                else{
                    Toast toast = Toast.makeText(this, "Seleccione una opcion porfavor", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                }
                break;


            case R.id.ingresos1:
                request_code = CAPTURE_IMAGE_INGRESOS1;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos2:
                request_code = CAPTURE_IMAGE_INGRESOS2;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos3:
                request_code = CAPTURE_IMAGE_INGRESOS3;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos4:
                request_code = CAPTURE_IMAGE_INGRESOS4;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos5:
                request_code = CAPTURE_IMAGE_INGRESOS5;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos6:
                request_code = CAPTURE_IMAGE_INGRESOS6;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos7:
                request_code = CAPTURE_IMAGE_INGRESOS7;
                startScan(ScanConstants.OPEN_CAMERA);
                break;

            case R.id.ingresos8:
                request_code = CAPTURE_IMAGE_INGRESOS8;
                startScan(ScanConstants.OPEN_CAMERA);
                break;
        }

    }


    protected void startScan(int preference) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        if(request_code == CAPTURE_IMAGE_INGRESOS1){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS1);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS2){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS2);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS3){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS3);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS4){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS4);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS5){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS5);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS6){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS6);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS7){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS7);
        }
        if(request_code == CAPTURE_IMAGE_INGRESOS8){
            startActivityForResult(intent, CAPTURE_IMAGE_INGRESOS8);
        }

    }

    @Override
    public void sendPetition() {
        String scanSave = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.SCAN_SAVE_ID, "");


        // NAPI
        if(scanSave.equals("")) {
            SendIdOcrDTO sendIdOcrDTO = new SendIdOcrDTO();

            sendIdOcrDTO.setId(Base64.encodeToString(photoBufferFront, Base64.DEFAULT));
            sendIdOcrDTO.setIdReverso(Base64.encodeToString(photoBufferBack, Base64.DEFAULT));

            sendIdOcrDTO.setContentType("");

            FrontCapture.getInstance().setBufferFrontCapture(photoBufferFront);

            fileList.clear();
            jsonString = buildJSON();
            fileList.add(fileJson);
            fileList.add(imageFileFront);
            fileList.add(imageFileBack);


            new CredentialsCaptured(IcarScanActivity.this, fileJson, sendIdOcrDTO).execute();

            goNext();
        }
    }


    @Override
    public void goNext() {
        String ByPasAPI = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.ByPasAPI, "");
        if (ByPasAPI == "SI" )
            new CredentialsCaptured(IcarScanActivity.this, token, jsonString, fileList, strCrentialType).execute();



        if (strCrentialType.equals(ApiConstants.STRING_PASSPORT)) {

        } else {

            startActivity(new Intent(IcarScanActivity.this, DocumentScanActivity.class));

        }
    }

    @Override
    public void deleteFile () {

        if (fileJson != null && fileJson.exists()) {
            fileJson.delete();
        }

        if (imageFileBack != null && imageFileBack.exists()) {
            imageFileBack.delete();
        }

        if (imageFileFront != null && imageFileFront.exists()) {
            imageFileFront.delete();
        }

        if (imageFileEvidence != null && imageFileEvidence.exists()) {
            imageFileEvidence.delete();
        }

        if (strPathSourceFile != null && !strPathSourceFile.equals("")) {
            File temp = new File(strPathSourceFile);
            temp.delete();
            try {

                int delete = getContentResolver().delete(uri, null, null);

            } catch (SecurityException e) {

                Log.d(" ", "Error al borrar el archivo de evidencias");

                e.printStackTrace();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.i_close_operation_menu) {
            AlertDialog dialogAlert;
            dialogAlert = new AlertDialog(IcarScanActivity.this, getString(R.string.message_close_operation_title), getString(R.string.message_close_operation_alert), ApiConstants.ACTION_CANCEL_OPERATION);
            dialogAlert.setCancelable(false);
            dialogAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogAlert.show();
        }
        if (id == R.id.i_log_out_menu) {
            AlertDialog dialogAlert;
            dialogAlert = new AlertDialog(IcarScanActivity.this, getString(R.string.message_title_logout), getString(R.string.message_message_logout), ApiConstants.ACTION_LOG_OUT);
            dialogAlert.setCancelable(false);
            dialogAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String backImgInBase64;

        switch (request_code) {
            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_FRONTAL:
                if (resultCode == RESULT_OK && data != null) {

                    //validacion si es INGRESO O si es IFE

                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;
                    try {
                        bitmap = AppDataCaptured.getInstance().localImages.getBitmapToShow(getBaseContext(), IMAGE_FRONT, IdCloud_LocalImages.SizeShow.BIG);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY_COMPRESION, baos);

                        backImgInBase64 = retrieveImageInBase64(IMAGE_FRONT);

                        photoBufferFront = Base64.decode (backImgInBase64, 2);

                        imageFront.setImageBitmap( BitmapFactory.decodeByteArray(photoBufferFront, 0, photoBufferFront.length));
                        imageFront.setScaleType(ImageView.ScaleType.CENTER_CROP);

                        Log.d("Resolution Front", "rResolution: w->" + bitmap.getWidth() + " , h->" + bitmap.getHeight());

                        resolution = bitmap.getWidth() * bitmap.getHeight();

                        Log.d("Resolution Front", "rResolution: total->" + resolution);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch(Exception e) {
                        Toast.makeText(IcarScanActivity.this, "Error al capturar imagen de identificación.", Toast.LENGTH_LONG).show();
                    }

                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "icar_front" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "icar_front" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBufferFront);
                        // remember close de FileOutput
                        fo.close();
                        imageFileFront = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imageFileFront = null;
                    }
                }

                //ELSE INGRESOS


                break;

            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_POSTERIOR:
                if (resultCode == RESULT_OK && data != null) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;
                    try {

                        bitmap = AppDataCaptured.getInstance().localImages.getBitmapToShow(getBaseContext(), IMAGE_BACK, IdCloud_LocalImages.SizeShow.BIG);

                        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY_COMPRESION, baos);

                        backImgInBase64 = retrieveImageInBase64(IMAGE_BACK);

                        photoBufferBack = Base64.decode (backImgInBase64, 2);

                        imageBack.setImageBitmap( BitmapFactory.decodeByteArray(photoBufferBack, 0, photoBufferBack.length));

                        imageBack.setScaleType(ImageView.ScaleType.CENTER_CROP);

                        Log.d("Resolution Back", "rResolution: w->" + bitmap.getWidth() + " , h->" + bitmap.getHeight());

                        resolution = bitmap.getWidth() * bitmap.getHeight();

                        Log.d("Resolution Back", "rResolution: total->" + resolution);

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch(Exception e) {
                        Toast.makeText(IcarScanActivity.this, "Error retrieving captured image!.", Toast.LENGTH_LONG).show();
                    }

                    //Guarda nueva imagen del rostro de la persona
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "icar_back" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "icar_back" + operationID + ".jpg");
                    }

                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBufferBack);
                        // remember close de FileOutput
                        fo.close();
                        imageFileBack = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imageFileBack = null;
                    }
                }
                break;

            case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_EVIDENCE: {

                if (resultCode == RESULT_OK) {

                    String resultado = data.getExtras().getString("EVIDENCE_FILE");

                    File f = new File(resultado);

                    if (!f.exists()) {

                        imageFileEvidence = null;

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(getApplicationContext(), "No se encontro evidencia, vuelva a capturarla", Toast.LENGTH_SHORT);
                                toast.show();
                            }
                        });

                    } else {
                        imageFileEvidence = f;

                        Bitmap myBitmap = BitmapFactory.decodeFile(imageFileEvidence.getAbsolutePath());

                        imageEvidence.setImageBitmap(myBitmap);

                    }
                }
            }
            break;

            case CAPTURE_IMAGE_INGRESOS1:

                if (requestCode == CAPTURE_IMAGE_INGRESOS1 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos.setImageBitmap(scaledBitmap);
                        photoBuffer = bitmapToByteArray(bitmap);


                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Primer_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Primer_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen1 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen1 = null;
                    }

                }
                break;

            case CAPTURE_IMAGE_INGRESOS2:
                if (requestCode == CAPTURE_IMAGE_INGRESOS2 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos2.setImageBitmap(scaledBitmap);

                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Segundo_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Segundo_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen2 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen2 = null;
                    }

                }
                break;
            case CAPTURE_IMAGE_INGRESOS3:
                if (requestCode == CAPTURE_IMAGE_INGRESOS3 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos3.setImageBitmap(scaledBitmap);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Tercer_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Tercer_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen3 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen3 = null;
                    }

                }
                break;
            case CAPTURE_IMAGE_INGRESOS4:
                if (requestCode == CAPTURE_IMAGE_INGRESOS4 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos4.setImageBitmap(scaledBitmap);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Cuarto_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Cuarto_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen4 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen4 = null;
                    }

                }
                break;
            case CAPTURE_IMAGE_INGRESOS5:
                if (requestCode == CAPTURE_IMAGE_INGRESOS5 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos5.setImageBitmap(scaledBitmap);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Quinto_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Quinto_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen5 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen5 = null;
                    }

                }
                break;
            case CAPTURE_IMAGE_INGRESOS6:
                if (requestCode == CAPTURE_IMAGE_INGRESOS6 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos6.setImageBitmap(scaledBitmap);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Sexto_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Sexto_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen6 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen6 = null;
                    }

                }
                break;
            case CAPTURE_IMAGE_INGRESOS7:
                if (requestCode == CAPTURE_IMAGE_INGRESOS7 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos7.setImageBitmap(scaledBitmap);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Siete_ingresos_" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Siete_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen7 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen7 = null;
                    }

                }
                break;
            case CAPTURE_IMAGE_INGRESOS8:
                if (requestCode == CAPTURE_IMAGE_INGRESOS8 && resultCode == Activity.RESULT_OK) {
                    Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
                    Bitmap bitmap;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        getContentResolver().delete(uri, null, null);
                        Double widthBitmap = (bitmap.getWidth() * .50);
                        Double heightBitmap = (bitmap.getHeight() * .50);
                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight() - heightBitmap.intValue()), true);
                        imageIngresos8.setImageBitmap(scaledBitmap);
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Guarda nueva imagen del documento: comprobante de domicilio
                    File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Octavo" + operationID + ".jpg");
                    if (f.exists()) {
                        f.delete();
                        f = new File(ApiConstants.FOLDER_IMGE + File.separator + "Octavo_ingresos_" + operationID + ".jpg");
                    }
                    try {
                        //write the bytes in file
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(photoBuffer);
                        // remember close de FileOutput
                        fo.close();
                        imagen8 = f;
                    } catch (IOException e) {
                        e.printStackTrace();
                        imagen8 = null;
                    }

                }
                break;
        }


    }

    public byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream); //Tipo de imagen
        return stream.toByteArray();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void modifyLayoutByIdSelected(String idType) {
        if (idType.equals(ApiConstants.STRING_PASSPORT)) {
            onePicture = true;
            btnReverso.setVisibility     (View.GONE);
            btnEvidence.setVisibility    (View.GONE);
            imageBack.setVisibility      (View.GONE);
            imageEvidence.setVisibility  (View.GONE);
            instructionsTV.setText(getString(R.string.id_scan_instructions_one_side));
        } else {
            btnEvidence.setVisibility     (View.GONE);
            instructionsTV.setText(getString(R.string.id_scan_instructions_both_sides));
        }
    }



    private void dispatchTakePictureIntent () {

        //First of all we clean all images captured previously.
        AppDataCaptured.getInstance().localImages.removeImage(getBaseContext(), IcarCapture_Configuration.IcarCaptureImage.IMAGE_FRONT);
        AppDataCaptured.getInstance().localImages.removeImage(getBaseContext(), IcarCapture_Configuration.IcarCaptureImage.IMAGE_BACK);

        mIcarCapture_Conf.m_eCaptureProcess                 = IcarCapture_Configuration.IcarCaptureProcess.CAPTURE_IMAGE;

        if (request_code == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE_POSTERIOR) {

            mIcarCapture_Conf.typeCaptureImage                  = IcarCapture_Configuration.IcarCaptureImage.IMAGE_BACK;
            mIcarCapture_Conf.customized_AutoFoto.text_Toolbar  = R.string.icar_sdk_placeBackInsideBox;

        } else {

            mIcarCapture_Conf.typeCaptureImage                  = IcarCapture_Configuration.IcarCaptureImage.IMAGE_FRONT;
            mIcarCapture_Conf.customized_AutoFoto.text_Toolbar  = R.string.icar_sdk_placeFrontInsideBox;

        }

        mIcarCapture_Conf.captureDocWithAutoFoto            = true;

        Intent intent = new Intent(IcarScanActivity.this, IcarCapture.class);
        intent.putExtra("ServerConfig", mIcarCapture_Conf);
        startActivityForResult(intent, REQUEST_CODE_CAPTURE_IMAGE);

    }

    private String retrieveImageInBase64(IcarCapture_Configuration.IcarCaptureImage imgType) {
        IdCloud_LocalImages images = AppDataCaptured.getInstance().localImages;
        String imageInBase64 = "";

        //1. First get image in bitmap format:
        Bitmap img = null;
        try{
            img = images.getBitmapToSend(this.getBaseContext(), imgType);
        }catch (OutOfMemoryError e)
        {
            Toast.makeText(this.getBaseContext(), "ERROR: OutOfMemory getBitmap_front", Toast.LENGTH_SHORT).show();
        }catch (Exception e)
        {
            Toast.makeText(this.getBaseContext(), "ERROR: GenericException getBitmap_front", Toast.LENGTH_SHORT).show();
        }

        //1. And then convert bitmap to base64 string format:
        imageInBase64 = IcarSDK_Utils.convertBitmapToBase64StringSafety(img, IcarSDK_Utils.QUALITY_COMPRESION);

        if (imageInBase64 == null)
        {
            imageInBase64 = "";
            Toast.makeText(this.getBaseContext(), "No se pudo capturar la imagen", Toast.LENGTH_SHORT).show();
        }

        Log.d("===========",imageInBase64);

        return imageInBase64;
    }

    public void saveIdValidation() {
        fileList.clear();
        //String jsonString = buildJSON();

        fileList.add(fileJson);
        fileList.add(imageFileEvidence);

        //new SendValidationId(IcarScanActivity.this, token, jsonString, fileList, strCrentialType).execute();
        deleteFile();
    }

    public void customizeIcarLayouts() {

        mIcarCapture_Conf.maxImageSize = 2.0f;

        mIcarCapture_Conf.customized_AutoFoto.time_ToDisappear_Template = -1; //never disappear
        mIcarCapture_Conf.customized_AutoFoto.color_BackgroundLayout    = Color.parseColor("#00A6DD");
        mIcarCapture_Conf.customized_AutoFoto.alpha_BackgroundLayout    = 0.7f; //70%
        mIcarCapture_Conf.customized_AutoFoto.color_Toolbar             = Color.parseColor("#FF55A6DD");
        mIcarCapture_Conf.customized_AutoFoto.color_Toolbar             = Color.parseColor("#00A6DD");
        mIcarCapture_Conf.customized_AutoFoto.alpha_Toolbar             = 1.0f; //

        mIcarCapture_Conf.customized_AutoFoto.color_DetectedFrames = Color.GREEN;
        mIcarCapture_Conf.customized_AutoFoto.text_Toolbar         = R.string.gis_message_toolbar;
        mIcarCapture_Conf.customized_AutoFoto.color_text_Toolbar   = Color.WHITE;
        mIcarCapture_Conf.customized_AutoFoto.show_HelpButton      = false;
        mIcarCapture_Conf.customized_CaptureDoc.text_Center        = R.string.gis_message_centerbox;
        mIcarCapture_Conf.customized_CaptureDoc.show_logoIcar      = false;
    }


    public String buildJSON() {

        // operationID="50214";

        Log.d("ICARSCANACtivity"," Operation  id " + operationID);
        Log.d("ICARSCANACtivity"," Enterprice id " + idEnterprice);
        Log.d("ICARSCANACtivity"," Customer Type " + customerType);

        //Construimos el JSON
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("emprId", idEnterprice);
            jsonObject.put("customerType", customerType);
            jsonObject.put("operationId", Integer.valueOf(operationID));
            jsonObject.put("credentialType", strCrentialType);
            jsonObject.put("imageResolution", 560);
            jsonObject.put("contentType", "image/jpeg");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String sendJSON = jsonObject.toString();
            sendJSON = sendJSON.replaceAll("\\\\", "");

            Writer output;
            fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "json" + ".json");
            if (fileJson.exists()) {
                fileJson.delete();
                fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "json" + ".json");
            }
            output = new BufferedWriter(new FileWriter(fileJson));
            output.write(sendJSON);
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }



    public boolean hasCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int ihasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA);
            if (ihasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_ASK_PERMISSIONS);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public boolean validarFotos(){
        boolean correcto = false;

        if(rb_semana.isChecked()){
            if(imagen1 != null && imagen2 != null && imagen3 != null && imagen4 != null && imagen5 != null && imagen6 != null && imagen7 != null && imagen8 != null){
                correcto = true;
            }else{
                Toast.makeText(this, "Verifique que haya capturado todas las fotos que se le piden", Toast.LENGTH_SHORT).show();
            }

        }else if(rb_quince.isChecked()){
            if(imagen1 != null && imagen2 != null && imagen3 != null && imagen4 != null){
                correcto = true;
            }else{
                Toast.makeText(this, "Verifique que haya capturado todas las fotos que se le piden", Toast.LENGTH_SHORT).show();
            }
        }else if(rb_mes.isChecked()){
            if(imagen1 != null && imagen2 != null){
                correcto = true;
            }else{
                Toast.makeText(this, "Verifique que haya capturado todas las fotos que se le piden", Toast.LENGTH_SHORT).show();
            }
        }else {
            correcto = false;
            Toast.makeText(this, "Ingrese comprobantes de pagos porfavor", Toast.LENGTH_SHORT).show();
        }
        return correcto;
    }

    public boolean validatePictureTake() {
        boolean bitMapTake = false;

        if (imageFileFront != null) {

            bitMapTake = true;

            if (!onePicture) {
                if (imageFileBack != null) {

                    if (imageFileEvidence == null) {

                        bitMapTake = true;

                    } else {

                        bitMapTake = true;

                    }
                } else {
                    bitMapTake = false;
                    Toast.makeText(this, getString(R.string.msg_error_capture_id_back), Toast.LENGTH_SHORT).show();
                }
            }

        } else {
            bitMapTake = false;
            Toast.makeText(this, getString(R.string.msg_error_capture_id_front), Toast.LENGTH_SHORT).show();
        }

        return bitMapTake;

    }

}