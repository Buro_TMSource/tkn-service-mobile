package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rgarciav on 20/03/2018.
 */

public class FieldSearchDetailDTO {

    @SerializedName("curp")
    String curp;

    @SerializedName("mail")
    String mail;

    @SerializedName("tel")
    String tel;

    public FieldSearchDetailDTO(String curp, String mail, String tel) {
        this.curp = curp;
        this.mail = mail;
        this.tel  = tel;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
