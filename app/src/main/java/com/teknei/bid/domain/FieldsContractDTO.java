package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rgarciav on 14/03/2018.
 */

public class FieldsContractDTO {

    @SerializedName("signCode")
    private String signCode;

    @SerializedName("signDesc")
    private String signDesc;

    @SerializedName("required")
    private Boolean required;

    @SerializedName("accepted")
    private Boolean accepted;

    @SerializedName("idClie")
    private Integer idClie;

    @SerializedName("usernameRequesting")
    private String usernameRequesting;

    public FieldsContractDTO() {
        signCode            = "";
        signDesc            = "";
        required            = false;
        accepted            = false;
        idClie              = 0;
        usernameRequesting  = "";
    }

    public String getSignCode() {
        return signCode;
    }

    public void setSignCode(String signCode) {
        this.signCode = signCode;
    }

    public String getSignDesc() {
        return signDesc;
    }

    public void setSignDesc(String signDesc) {
        this.signDesc = signDesc;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }

    public Integer getIdClie() {
        return idClie;
    }

    public void setIdClie(Integer idClie) {
        this.idClie = idClie;
    }

    public String getUsernameRequesting() {
        return usernameRequesting;
    }

    public void setUsernameRequesting(String usernameRequesting) {
        this.usernameRequesting = usernameRequesting;
    }
}
