package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.activities.AcceptContractActivity;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.dialogs.MsgGetFieldsContract;
import com.teknei.bid.domain.FieldsContractDTO;
import com.teknei.bid.domain.RequestFeldContractDTO;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rgarciav on 14/03/2018.
 */

public class GetListFieldsContract extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "GetListFieldsContract";

    private List<FieldsContractDTO> listValue;

    private Activity activityOrigin;
    private String   errorMessage;
    private String   token;
    private Integer  idOperation;

    private Integer responseStatus = 0;

    private long endTime;

    public GetListFieldsContract(Activity activityOrigin, String token, Integer idOperation) {
        this.token          = token;
        this.idOperation    = idOperation;
        this.activityOrigin = activityOrigin;
    }

    @Override
    protected void onPreExecute() {
        endTime = System.currentTimeMillis() + 2000;
        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);
    }

    @Override
    protected Void doInBackground(String... strings) {

            Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
            while (System.currentTimeMillis() < endTime) {
                //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
            }
            Log.i("Wait", "timer finish : " + System.currentTimeMillis());

            String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

            Log.d(CLASS_NAME, "token        :" + token);
            Log.d(CLASS_NAME, "idOperation  :" + idOperation);
            Log.d(CLASS_NAME, "endpoint     :" + endPoint);

            BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

            Call<List<FieldsContractDTO>> call = api.enrollmentGetContractAcceptance(token, new RequestFeldContractDTO(idOperation));

            call.enqueue(new Callback<List<FieldsContractDTO>>() {

                @Override
                public void onResponse(Call<List<FieldsContractDTO>> call, Response<List<FieldsContractDTO>> response) {

                    Log.d(CLASS_NAME, response.code() + " ");

                    responseStatus = response.code();

                    if (responseStatus >= 200 && responseStatus < 300) {

                        if (response.body()!= null) {

                            listValue = response.body();

                            ((AcceptContractActivity) activityOrigin).setListFieldsContract (listValue);

                        }

                    } else {

                        if (responseStatus >= 300 && responseStatus < 400) {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                        } else if (responseStatus >= 400 && responseStatus < 500) {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_400);

                        } else if (responseStatus >= 500 && responseStatus < 600) {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                        }

                        MsgGetFieldsContract dialogoAlert;
                        dialogoAlert = new MsgGetFieldsContract(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();
                    }
                }

                @Override
                public void onFailure(Call<List<FieldsContractDTO>> call, Throwable t) {
                    Log.d(CLASS_NAME, activityOrigin.getString(R.string.message_ws_response_500));

                    MsgGetFieldsContract dialogoAlert;
                    dialogoAlert = new MsgGetFieldsContract(activityOrigin, activityOrigin.getString(R.string.message_ws_notice),
                            activityOrigin.getString(R.string.msg_error_conexion));
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();

                    t.printStackTrace();
                }
            });
        return null;
    }
}

