package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.asynctask.LogIn;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

public class SplashActivity extends BaseActivity {

    String TAG = "SPLASHACTIVIY";

    //private String user = "alex10";
    private String user = "SBDOswaldo";
    //private String pass = "pasword";
    private String pass = "SabaPass2018";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        String uuid      = PhoneSimUtils.getSerialNuber();
        Log.d(TAG, "----> Number Serial " + uuid);


        SharedPreferencesUtils.cleanSharedPreferencesOperation(this);
        saveSharedPreferenceByDefault();

        SharedPreferencesUtils.saveToPreferencesString
                (SplashActivity.this,SharedPreferencesUtils.TYPE_PERSON, ApiConstants.TYPE_CUSTOMER+"");

        SharedPreferencesUtils.saveToPreferencesString
                (SplashActivity.this,SharedPreferencesUtils.CUSTOMER_TYPE,ApiConstants.TYPE_CUSTOMER+"");

        SharedPreferencesUtils.saveToPreferencesString
                (SplashActivity.this,SharedPreferencesUtils.CUSTOM_COMPANY,ApiConstants.CUSTOM_HSBC+"");

        SharedPreferencesUtils.saveToPreferencesString
                (SplashActivity.this,SharedPreferencesUtils.ByPasAPI,ApiConstants.ByPasAPI+"");

        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this,SharedPreferencesUtils.ID_DEVICE,uuid);

        sendPetition();
    }

    @Override
    public void sendPetition() {

        if (user.equals("admin") && (pass.equals("admin")) ) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
        } else {
            new LogIn(SplashActivity.this, user, pass, "", "").execute();
        }


    }

    @Override
    public void goNext() {
       // startActivity(new Intent(SplashActivity.this, SelectTaskActivity.class));
        startActivity(new Intent(SplashActivity.this, VideoCallActivity.class));

    }




    public void saveSharedPreferenceByDefault() {
        String urlIdScan         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_ID_SCAN, getString(R.string.default_url_id_scan));
        String licenseIdScan     = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.LICENSE_ID_SCAN, getString(R.string.default_license_id_scan));
        String urlTeknei         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_TEKNEI, getString(R.string.default_url_teknei));
        String urlMobbSign       = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_MOBBSIGN, getString(R.string.default_url_mobbsign));
        String licenseMobbSign   = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.MOBBSIGN_LICENSE, getString(R.string.default_license_mobbsign));
        String urlAuthAccess     = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.URL_AUTHACCESS, getString(R.string.default_url_oauthaccess));
        String fingerprintReader = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.FINGERPRINT_READER, getString(R.string.default_fingerprint_reader));
        String idCompany         = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.ID_ENTERPRICE, getString(R.string.default_id_company));
        String timeout           = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TIMEOUT_SERVICES, getString(R.string.default_timeout));

        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.URL_ID_SCAN, urlIdScan);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.LICENSE_ID_SCAN, licenseIdScan);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.URL_TEKNEI, urlTeknei);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.URL_MOBBSIGN, urlMobbSign);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.MOBBSIGN_LICENSE, licenseMobbSign);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.URL_AUTHACCESS, urlAuthAccess);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.FINGERPRINT_READER, fingerprintReader);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, idCompany);
        SharedPreferencesUtils.saveToPreferencesString(SplashActivity.this, SharedPreferencesUtils.TIMEOUT_SERVICES, timeout);
    }
}
