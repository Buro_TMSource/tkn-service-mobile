package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

public class ResendOtpDTO {

    @SerializedName("idClient")
    private Integer idClient;

    public ResendOtpDTO(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }
}
