package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.teknei.bid.R;
import com.teknei.bid.asynctask.GetListFieldsContract;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.dialogs.MessageDialog;
import com.teknei.bid.domain.FieldsContractDTO;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;

import java.util.ArrayList;
import java.util.List;

public class AcceptContractActivity extends BaseActivity implements View.OnClickListener {

    private String TAG = "AcceptContractActivity";

    private CheckBox checkBox01;
    private CheckBox checkBox02;
    private CheckBox checkBox03;
    private CheckBox checkBox04;
    private CheckBox checkBox05;
    private CheckBox checkBox06;
    private CheckBox checkBox07;
    private CheckBox checkBox08;
    private CheckBox checkBox09;
    private CheckBox checkBox10;
    private CheckBox checkBox11;
    private CheckBox checkBox12;

    private Button   btnContinue;

    private String token;
    private String idOperation;
    private String strErrorMsg;

    private List<FieldsContractDTO> listFieldsValue;
    private List<FieldsContractDTO> listSendFieldsValue;

    boolean arrayCheck [];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_contract);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ico_bid);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            invalidateOptionsMenu();
        }

        listFieldsValue = new ArrayList<FieldsContractDTO>();
        listSendFieldsValue = new ArrayList<FieldsContractDTO>();

        token = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");

       // idOperation="50214";
        idOperation   = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");

        checkBox01 = (CheckBox) findViewById(R.id.aca_chbox_product);
        checkBox02 = (CheckBox) findViewById(R.id.aca_chbox_general_info);
        checkBox03 = (CheckBox) findViewById(R.id.aca_chbox_beneficiary_data);
        checkBox04 = (CheckBox) findViewById(R.id.aca_chbox_app_movil);
        checkBox05 = (CheckBox) findViewById(R.id.aca_chbox_relationship_account);
        checkBox06 = (CheckBox) findViewById(R.id.aca_chbox_pay_sabadell);
        checkBox07 = (CheckBox) findViewById(R.id.aca_chbox_credit_information);
        checkBox08 = (CheckBox) findViewById(R.id.aca_chbox_share_information);
        checkBox09 = (CheckBox) findViewById(R.id.aca_chbox_marketing);
        checkBox10 = (CheckBox) findViewById(R.id.aca_chbox_contract_clause);
        checkBox11 = (CheckBox) findViewById(R.id.aca_chbox_register_format);
        checkBox12 = (CheckBox) findViewById(R.id.aca_chbox_format_fatca);
        btnContinue= (Button)   findViewById(R.id.aca_btn_continue);

        checkBox01.setOnClickListener(this);
        checkBox02.setOnClickListener(this);
        checkBox03.setOnClickListener(this);
        checkBox04.setOnClickListener(this);
        checkBox05.setOnClickListener(this);
        checkBox06.setOnClickListener(this);
        checkBox07.setOnClickListener(this);
        checkBox08.setOnClickListener(this);
        checkBox09.setOnClickListener(this);
        checkBox10.setOnClickListener(this);
        checkBox11.setOnClickListener(this);
        checkBox12.setOnClickListener(this);
        btnContinue.setOnClickListener(this);

        getListFieldsContract ();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.i_close_operation_menu) {
            AlertDialog dialogAlert;
            dialogAlert = new AlertDialog(AcceptContractActivity.this, getString(R.string.message_close_operation_title), getString(R.string.message_close_operation_alert), ApiConstants.ACTION_CANCEL_OPERATION);
            dialogAlert.setCancelable(false);
            dialogAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogAlert.show();
        }
        if (id == R.id.i_log_out_menu) {
            AlertDialog dialogAlert;
            dialogAlert = new AlertDialog(AcceptContractActivity.this, getString(R.string.message_title_logout), getString(R.string.message_message_logout), ApiConstants.ACTION_LOG_OUT);
            dialogAlert.setCancelable(false);
            dialogAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void goNext() {



        Intent i = new Intent(AcceptContractActivity.this, SigningContractKaralundiActivity.class);
        startActivity(i);
    }

    @Override
    public void sendPetition() {

        String scanSave = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.ACCEPT_CLAUSES, "");

        if (scanSave.equals("")) {

            for (FieldsContractDTO value : listFieldsValue) {

                if (value.getSignCode().equals(ApiConstants.SOLI_PROD)) {

                    if (checkBox01.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.DATO_GRAL)) {

                    if (checkBox02.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.DATO_BENE)) {

                    if (checkBox03.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.APP_MOVI)) {

                    if (checkBox04.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.CTA_RELA)) {

                    if (checkBox05.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.PAGA_SABA)) {

                    if (checkBox06.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.AUTO_INFO_CRED)) {

                    if (checkBox07.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.AUTO_COMP_INFO)) {

                    if (checkBox08.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.AUTO_FINE_MERC)) {

                    if (checkBox09.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.CLAU_CONT_MULT)) {

                    if (checkBox10.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.FORM_CTA_DEST)) {

                    if (checkBox11.isChecked()) {

                        value.setAccepted(true);

                    }

                } else if (value.getSignCode().equals(ApiConstants.FORM_FATC)) {

                    if (checkBox12.isChecked()) {

                        value.setAccepted(true);

                    }
                }

                value.setIdClie (Integer.parseInt(idOperation));

                listSendFieldsValue.add(value);
            }
            goNext();
        } else {

            goNext();

        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.aca_btn_continue:
                if (validateDataForm()) {

                    sendPetition();

                } else {
                    MessageDialog dialogoAlert;
                    dialogoAlert = new MessageDialog(AcceptContractActivity.this,
                            getString(R.string.message_ws_notice), strErrorMsg);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();
                }
                break;
            default:
                onCheckboxClicked(view);
                break;
        }
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();

        switch(view.getId()) {
            case R.id.aca_chbox_product:
                if (checked) {

                    checkBox02.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox02.setChecked(true);
                        }
                    });

                    checkBox03.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox03.setChecked(true);
                        }
                    });

                    checkBox04.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox04.setChecked(true);
                        }
                    });

                    checkBox05.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox05.setChecked(true);
                        }
                    });

                    checkBox06.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox06.setChecked(true);
                        }
                    });

                } else {
                    checkBox02.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox02.setChecked(false);
                        }
                    });

                    checkBox03.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox03.setChecked(false);
                        }
                    });

                    checkBox04.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox04.setChecked(false);
                        }
                    });

                    checkBox05.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox05.setChecked(false);
                        }
                    });

                    checkBox06.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox06.setChecked(false);
                        }
                    });
                }
                break;

            case R.id.aca_chbox_general_info:
            case R.id.aca_chbox_beneficiary_data:
            case R.id.aca_chbox_app_movil:
            case R.id.aca_chbox_relationship_account:
            case R.id.aca_chbox_pay_sabadell:

                if (checkBox02.isChecked() && checkBox03.isChecked() && checkBox04.isChecked() && checkBox05.isChecked() && checkBox06.isChecked()){
                    checkBox01.post(new Runnable() {
                        @Override
                        public void run() {
                            checkBox01.setChecked(true);
                        }
                    });
                } else if (!checked && checkBox01.isChecked()) {
                    checkBox01.setChecked(false);
                }

                break;
        }
    }

    private boolean validateDataForm () {

        if (!checkBox01.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_product_request);

            return false;

        } else if (!checkBox02.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_general_information);

            return false;

        } else if (!checkBox03.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_beneficiary_data);

            return false;

        } else if (!checkBox04.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_app_movil);

            return false;

        } else if (!checkBox05.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_relationship_account);

            return false;

        } else if (!checkBox06.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_pay_sabadell);

            return false;

        } else if (!checkBox10.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_contract_clause);

            return false;

        } else if (!checkBox11.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_register_format);

            return false;

        } else if (!checkBox12.isChecked()) {

            strErrorMsg = getResources().getString(R.string.aca_message_error_format_fatca);

            return false;
        }

        return true;
    }

    public void getListFieldsContract () {

        new GetListFieldsContract(this, token, Integer.parseInt(idOperation)).execute();

    }

    public void setListFieldsContract (List<FieldsContractDTO> listFieldsValue) {

        this.listFieldsValue = listFieldsValue;

    }
}
