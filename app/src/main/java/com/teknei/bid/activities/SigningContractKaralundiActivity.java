package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.identy.AppUtils;
import com.identy.IdentySdk;
import com.identy.WSQCompression;
import com.identy.encryption.FileCodecBase64;
import com.identy.enums.Finger;
import com.identy.enums.FingerDetectionMode;
import com.identy.enums.Hand;
import com.identy.enums.Template;
import com.teknei.bid.R;
import com.teknei.bid.asynctask.SendFingerToAuth;
import com.teknei.bid.asynctask.ValidateCURP;
import com.teknei.bid.domain.FieldSearchDetailDTO;
import com.teknei.bid.domain.FingerLoginDTO;
import com.teknei.bid.services.CipherFingerServices;
import com.teknei.bid.tools.Base64;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;

public class SigningContractKaralundiActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "SigningContractKaral";

    private FingerDetectionMode[] detectionModes;

    private LinearLayout principalCheck;
    private LinearLayout takeFingerCheck;
    private String base64FingerLogin ="";

    private Button    btnContinue;
    private ImageView imViewInitFinger;
    private ImageView imViewTakeFinger;

    private FingerLoginDTO fingerDTO;

    private boolean onSuccess;

    //Left Hand
    File imageFilePinkyLeft;
    File imageFileRingLeft;
    File imageFileMiddleLeft;
    File imageFileIndexLeft;
    File imageFileThumbLeft;
    //Right Hand
    File imageFilePinkyRight;
    File imageFileRingRight;
    File imageFileMiddleRight;
    File imageFileIndexRight;
    File imageFileThumbRight;

    File   fileImage;

    //Left Hand
    String base64PinkyLeft;
    String base64RingLeft;
    String base64MiddleLeft;
    String base64IndexLeft;
    String base64ThumbLeft;
    //Right Hand
    String base64PinkyRight;
    String base64RingRight;
    String base64MiddleRight;
    String base64IndexRight;
    String base64ThumbRight;

    private String operationID;
    private String formCurp = "";

    protected String  WSQfileName    = "ImageCaptured";
    protected boolean flagIndexLeft  = false;
    protected boolean flagIndexRight = false;

    boolean enableSpoofCheck        = true;
    boolean displayboxes            = true;
    boolean displayFingerDrawable   = false;
    boolean nfiqCheck               = false;

    String mode = "demo";
    WSQCompression compression = WSQCompression.WSQ_10_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signing_contrat);

        ArrayList<FingerDetectionMode> modes = new ArrayList<>();
        modes.add(FingerDetectionMode.L4F);
        detectionModes = modes.toArray(new FingerDetectionMode[modes.size()]);

        operationID  =
                SharedPreferencesUtils.readFromPreferencesString
                        (SigningContractKaralundiActivity.this, SharedPreferencesUtils.OPERATION_ID, "");


        String jsonInitForm = SharedPreferencesUtils.readFromPreferencesString(SigningContractKaralundiActivity.this, SharedPreferencesUtils.JSON_INIT_FORM, "{}");
        JSONObject jsonFormObject = null;

        try {
            jsonFormObject = new JSONObject(jsonInitForm);

            formCurp = jsonFormObject.getString("curp");
        }catch (JSONException e){
            e.getMessage();
        }

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ico_bid);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            invalidateOptionsMenu();
        }

        btnContinue      = findViewById(R.id.btn_user_signing_finish);

        principalCheck   = findViewById(R.id.lay_user_signing_check_principal);
        takeFingerCheck  = findViewById(R.id.lfm_lay_signing_fingerprint_section);

        imViewInitFinger = findViewById(R.id.btn_user_signing_init);
        imViewTakeFinger = findViewById(R.id.btn_user_signing_take);

        btnContinue.setOnClickListener(SigningContractKaralundiActivity.this);
        imViewInitFinger.setOnClickListener(SigningContractKaralundiActivity.this);
        imViewTakeFinger.setOnClickListener(SigningContractKaralundiActivity.this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_user_signing_init:
                if (!formCurp.equals("")) {
                    new ValidateCURP(SigningContractKaralundiActivity.this, new FieldSearchDetailDTO(formCurp,"",""),true).execute();
                } else {
                    Toast.makeText(SigningContractKaralundiActivity.this, "Fallo en enrolamiento: Cliente no obtenido", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.btn_user_signing_take:
                break;

            case R.id.btn_user_signing_finish:
                goNext();
                break;
        }
    }

    @Override
    public void onBackPressed() { }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void sendPetition() {

        fingerDTO    = new FingerLoginDTO();

        fingerDTO.setFingerIndex(base64IndexLeft);
        fingerDTO.setId         (operationID);
        fingerDTO.setContentType("image/wsq");

        new SendFingerToAuth(SigningContractKaralundiActivity.this, fingerDTO, true).execute();
    }

    @Override
    public void goNext() {
        Intent i = new Intent(SigningContractKaralundiActivity.this, ShowContractActivity.class);
        i.putExtra("tipoContrato","firmado");
        i.putExtra("fingerPrintString",base64FingerLogin);
        startActivity(i);
        deleteFile();
        finish();
    }

    @Override
    public void deleteFile () {
        if (imageFilePinkyLeft != null && imageFilePinkyLeft.exists()) {
            imageFilePinkyLeft.delete();
        }

        if (imageFileRingLeft != null && imageFileRingLeft.exists()) {
            imageFileRingLeft.delete();
        }

        if (imageFileMiddleLeft != null && imageFileMiddleLeft.exists()) {
            imageFileMiddleLeft.delete();
        }

        if (imageFileIndexLeft != null && imageFileIndexLeft.exists()) {
            imageFileIndexLeft.delete();
        }

        if (imageFileThumbLeft != null && imageFileThumbLeft.exists()) {
            imageFileThumbLeft.delete();
        }

        if (imageFilePinkyRight != null && imageFilePinkyRight.exists()) {
            imageFilePinkyRight.delete();
        }

        if (imageFileRingRight != null && imageFileRingRight.exists()) {
            imageFileRingRight.delete();
        }

        if (imageFileMiddleRight != null && imageFileMiddleRight.exists()) {
            imageFileMiddleRight.delete();
        }

        if (imageFileIndexRight != null && imageFileIndexRight.exists()) {
            imageFileIndexRight.delete();
        }

        if (imageFileThumbRight != null && imageFileThumbRight.exists()) {
            imageFileThumbRight.delete();
        }

        if (fileImage != null && fileImage.exists()) {
            fileImage.delete();
        }
    }

    public void showPanelTakeFinger () {
        try {
            final ArrayList<Template> requiredtemplates = new ArrayList<>();

            requiredtemplates.add(Template.WSQ);
            requiredtemplates.add(Template.RAW);
            requiredtemplates.add(Template.PNG);
            requiredtemplates.add(Template.ISO_19794_4);
            requiredtemplates.add(Template.ISO_19794_2);

            IdentySdk.newInstance(SigningContractKaralundiActivity.this, "com.teknei.bid2019-01-24.lic", new IdentySdk.InitializationListener<IdentySdk>() {
                @Override
                public void onInit(IdentySdk d) {
                    try {
                        ArrayList<Finger> qc = new ArrayList<Finger>();
                        qc.add(Finger.THUMB);
                        qc.add(Finger.INDEX);
                        qc.add(Finger.MIDDLE);
                        qc.add(Finger.RING);
                        qc.add(Finger.LITTLE);

                        d.set4FintroShow(false);
                        d.setThumbIntroShow(true);

                        d.setDisplayImages(mode.equals("demo"))
                                //.setDisplayImages(false).setFingerPrintDrawable(R.drawable.printColor128, displayFingerDrawable)
                                //.setDisplayImages(false).setFingerPrintDrawable(R.drawable.printcolor128, true)
                                .setMode(mode)
                                //.setEncryption(IdentyEncrytion.AES,"car")
                                .setAS(enableSpoofCheck)
                                .setDetectionMode(detectionModes)
                                .setRequiredTemplates(requiredtemplates)
                                .displayImages  (mode.equals("demo"))
                                .setDisplayBoxes(displayboxes)
                                .setWSQCompression(compression)
                                //.setEncryption(new IdentyEncrytion(),"bla")
                                .setCalculateNFIQ(true)
                                .qcList(qc)
                                .capture();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, previewListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    IdentySdk.IdentyResponseListener previewListener = new IdentySdk.IdentyResponseListener() {

        @Override
        public void onResponse(IdentySdk.IdentyResponse response) {
            boolean success = false;

            Log.e(TAG, "onResponse");

            JSONObject jo = response.toJson();
            Log.e(TAG, "Entro a enroll Listener preview");

            String finger = "";
            int    fingerSelect = 0;

            try {
                FileOutputStream o = new FileOutputStream(AppUtils.createExternalDirectory("json_output").toString() + "/preview_latest_output"+System.currentTimeMillis()+".txt");
                o.write(jo.toString().getBytes());
                o.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (Map.Entry<Pair<Hand, Finger>, IdentySdk.FingerOutput> o : response.getPrints().entrySet()) {

                Pair<Hand, Finger> handFinger = o.getKey();

                IdentySdk.FingerOutput fingerOutput = o.getValue();

                fileImage = new File(ApiConstants.FOLDER_IMGE + File.separator + WSQfileName + ".wsq");

                if (fileImage.exists()) {
                    fileImage.delete();
                }

                try {

                    Log.e("MainActivity", "File: " + fileImage.getName());

                    FileOutputStream outPut = new FileOutputStream(fileImage);
                    String base64Str = fingerOutput.getTemplates().get(Template.WSQ);
                    outPut.write(FileCodecBase64.decode(base64Str));
                    outPut.close();

                    if (handFinger.first.toString().equals("left")) {

                        if (handFinger.second.toString().equals("thumb")) {
                            finger = "I1";
                            fingerSelect = 6;
                            base64ThumbLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("index")) {
                            flagIndexLeft = true;
                            finger = "I2";
                            fingerSelect = 7;
                            base64IndexLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("middle")) {
                            finger = "I3";
                            fingerSelect = 8;
                            base64MiddleLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("ring")) {
                            finger = "I4";
                            fingerSelect = 9;
                            base64RingLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("little")) {
                            finger = "I5";
                            fingerSelect = 10;
                            base64PinkyLeft = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));
                        }

                    } else {

                        if (handFinger.second.toString().equals("thumb")) {
                            finger = "D1";
                            fingerSelect = 1;
                            base64ThumbRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("index")) {
                            flagIndexRight = true;
                            finger = "D2";
                            fingerSelect = 2;
                            base64IndexRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("middle")) {
                            finger = "D3";
                            fingerSelect = 3;
                            base64MiddleRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("ring")) {
                            finger = "D4";
                            fingerSelect = 4;
                            base64RingRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));

                        } else if (handFinger.second.toString().equals("little")) {
                            finger = "D5";
                            fingerSelect = 5;
                            base64PinkyRight = Base64.encode(CipherFingerServices.cipherFinger(operationID,FileCodecBase64.decode(base64Str)));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                File file = new File(ApiConstants.FOLDER_IMGE + File.separator + "finger_" + finger + "_" + operationID + ".jpg");

                if (file.exists()) {
                    file.delete();
                    file = new File(ApiConstants.FOLDER_IMGE + File.separator + "finger_" + finger + "_" + operationID + ".jpg");
                }

                try {

                    FileOutputStream outPut = new FileOutputStream(file);

                    String base64Str = fingerOutput.getTemplates().get(Template.WSQ);

                    outPut.write(FileCodecBase64.decode(base64Str));

                    outPut.close();

                    if (handFinger.first.toString().equals("left")) {

                        if (handFinger.second.toString().equals("thumb")) {
                            imageFileThumbLeft  = file;

                        } else if (handFinger.second.toString().equals("index")) {
                            imageFileIndexLeft  = file;

                        } else if (handFinger.second.toString().equals("ring")) {
                            imageFileRingLeft   = file;

                        } else if (handFinger.second.toString().equals("middle")) {
                            imageFileMiddleLeft = file;

                        } else if (handFinger.second.toString().equals("little")) {
                            imageFilePinkyLeft  = file;

                        }

                    } else {

                        if (handFinger.second.toString().equals("thumb")) {
                            imageFileThumbRight  = file;

                        } else if (handFinger.second.toString().equals("index")) {
                            imageFileIndexRight  = file;

                        } else if (handFinger.second.toString().equals("ring")) {
                            imageFileRingRight   = file;

                        } else if (handFinger.second.toString().equals("middle")) {
                            imageFileMiddleRight = file;

                        } else if (handFinger.second.toString().equals("little")) {
                            imageFilePinkyRight  = file;
                        }
                    }
                    success = true;
                } catch (Exception e) {
                    e.printStackTrace();
                    success = false;
                }
            }
            if (success)
                sendPetition();
        }

        @Override
        public void onErrorResponse(IdentySdk.IdentyError error) {

            Toast.makeText(getApplicationContext(), "No se encontro componente", Toast.LENGTH_LONG).show();

        }
    };

    public void hidePanelTakeFinger () {
        principalCheck.setVisibility(View.VISIBLE);
        takeFingerCheck.setVisibility(View.GONE);
    }

    public void resultSuccessToAuth () {
        hidePanelTakeFinger();
        btnContinue.setEnabled(true);
        imViewInitFinger.setBackgroundResource(R.drawable.selector_image_button_green);

    }

    public void resultFailToAuth () {
        hidePanelTakeFinger();
        btnContinue.setEnabled(false);
        imViewInitFinger.setBackgroundResource(R.drawable.selector_image_button_red);

    }

}
