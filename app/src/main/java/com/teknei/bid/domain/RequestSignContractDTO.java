package com.teknei.bid.domain;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class RequestSignContractDTO {


    @SerializedName("nombre")
    private String nombre;


    @SerializedName("curp")
    private String curp;


    @SerializedName("domicilio")
    private String domicilio;

    @SerializedName("huella")
    private String huella;

    @SerializedName("firmar")
    private boolean firmar;

    private JSONObject jsonObject;

    public RequestSignContractDTO() {
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getHuella() {
        return huella;
    }

    public void setHuella(String huella) {
        this.huella = huella;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }


    public boolean isFirmar() {
        return firmar;
    }

    public void setFirmar(boolean firmar) {
        this.firmar = firmar;
    }


}
