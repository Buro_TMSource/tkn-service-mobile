package com.teknei.bid.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.teknei.bid.BaseAction;
import com.teknei.bid.R;
import com.teknei.bid.utils.SharedPreferencesUtils;

//import com.teknei.bid.activities.FingerWatsonActivity;

public class DialogConfirmCaptureFinger extends Dialog implements View.OnClickListener {

    private final String CLASS_NAME = "DialogConfirmCaptureFinger";

    Button okButton;
    Button cancelButton;
    Button retryButton;

    TextView txvTitle;
    TextView txvMessage;
    Activity activityOrigin;

    String titleIn;
    String menssageIn;

    public DialogConfirmCaptureFinger(Activity context, String title, String message) {
        super(context);
        activityOrigin = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /** Design the dialog in main.xml file */
        setContentView(R.layout.alert_dialog);

        this.titleIn = title;
        this.menssageIn = message;

        txvTitle     = (TextView) findViewById(R.id.alert_title);
        txvMessage   = (TextView) findViewById(R.id.alert_message);
        okButton     = (Button) findViewById(R.id.ok_buttom);
        cancelButton = (Button) findViewById(R.id.cancel_buttom);
        retryButton  = (Button) findViewById(R.id.retry_buttom);

        okButton.setOnClickListener(this);
        retryButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        txvTitle.setText(titleIn);
        txvMessage.setText(menssageIn);

        okButton.setVisibility(View.VISIBLE);
        retryButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ok_buttom:
                dismiss();
                ((BaseAction) activityOrigin).sendPetition();

                break;

            case R.id.retry_buttom:
            case R.id.cancel_buttom:
                dismiss();
                break;
        }
    }
}

