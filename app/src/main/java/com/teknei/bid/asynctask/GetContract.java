package com.teknei.bid.asynctask;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.activities.ShowContractActivity;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.dialogs.GetContractDialog;
import com.teknei.bid.dialogs.ProgressDialog;
import com.teknei.bid.domain.MailVerificationOTPDTO;
import com.teknei.bid.domain.RequestSignContractDTO;
import com.teknei.bid.response.ResponseStartOpe;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetContract extends AsyncTask<String, Void, Void> {
    private final String CLASS_NAME = "GetContract";

    private String token;
    private int idOperation;
    private Activity activityOrigin;

    private String errorMessage;
    private ProgressDialog progressDialog;

    private boolean hasConecction = false;
    private Integer responseStatus = 0;

    private long endTime;
    private boolean firmar = false;
    private RequestSignContractDTO requestSignContractDTO;


    public GetContract(Activity context, String tokenOld,int idOperationIn) {
        this.activityOrigin = context;
        this.token = tokenOld;
        this.idOperation = idOperationIn;
    }

    public GetContract(Activity context, RequestSignContractDTO requestSignContractDTO) {
        this.activityOrigin = context;
        this.firmar = true;
        this.requestSignContractDTO = requestSignContractDTO;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activityOrigin, activityOrigin.getString(R.string.message_load_contract));
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();
        endTime = System.currentTimeMillis() + 2000;

        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);
        ConnectivityManager check = (ConnectivityManager) activityOrigin.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = check.getAllNetworkInfo();

        for (int i = 0; i < info.length; i++) {
            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                hasConecction = true;
            }
        }
    }

    @Override
    protected Void doInBackground(String... params) {
        if (hasConecction) {

            Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
            while (System.currentTimeMillis() < endTime) {
                //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
            }
            Log.i("Wait", "timer finish : " + System.currentTimeMillis());

            String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));


            int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

            Log.v(CLASS_NAME, "token        :" + token);
            Log.v(CLASS_NAME, "endpoint     :" + endPoint);
            Log.v(CLASS_NAME, "operation    :" + idOperation);

            BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);
            if(firmar)
                callSignedContratService(api);
            else
                callRegularContratService(api);

        } else {
            progressDialog.dismiss();

            ((BaseActivity) activityOrigin).showErrorConection();
        }
        return null;
    }

    public void callRegularContratService(BIDEndPointServices api){
        Call<ResponseBody> call = api.enrollmentCertContract(token, idOperation+"");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (responseStatus >= 200 && responseStatus < 300) {

                    SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SHOW_CONTRACT, "ok");

                    try {
                        InputStream input = response.body().byteStream();
                        //File path = Environment.getExternalStorageDirectory();
                        File file = new File(ApiConstants.FOLDER_IMGE
                                + File.separator, "contract_" + idOperation + ".pdf");

                        BufferedOutputStream output = new BufferedOutputStream(
                                new FileOutputStream(file));
                        byte data[] = new byte[1024];

                        long total = 0;
                        int count;
                        while ((count = input.read(data)) != -1) {
                            total += count;
                            output.write(data, 0, count);
                        }

                        output.flush();

                        output.close();
                    } catch (IOException e) {
                        String logTag = "TEMPTAG";
                        Log.e(logTag, "Error while writing file!");
                        Log.e(logTag, e.toString());
                    }

                    ((ShowContractActivity) activityOrigin).displayFromAsset("contract_" + idOperation + ".pdf");

                } else {
                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 400 && responseStatus < 500) {

                        if (responseStatus == 422) {

                            errorMessage = responseStatus + " - Error al generar contrato";

                        } else {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_400);

                        }

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                    }

                    GetContractDialog dialogoAlert;
                    dialogoAlert = new GetContractDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage, ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();

                t.printStackTrace();

                GetContractDialog dialogoAlert;
                dialogoAlert = new GetContractDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), activityOrigin.getString(R.string.msg_error_conexion), ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                dialogoAlert.setCancelable(false);
                dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogoAlert.show();
            }
        });
    }

    public void callSignedContratService (BIDEndPointServices api){

        Call<ResponseBody> call = api.enrollmentGetDemoContrat("application/json", requestSignContractDTO.getNombre(),requestSignContractDTO.getCurp(),requestSignContractDTO.isFirmar(),requestSignContractDTO.getDomicilio());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (responseStatus >= 200 && responseStatus < 300) {

                    SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SHOW_CONTRACT, "ok");

                    try {
                        InputStream input = response.body().byteStream();
                        File path = Environment.getExternalStorageDirectory();
                        File file = new File(ApiConstants.FOLDER_IMGE
                                + File.separator, "contract_" + idOperation + ".pdf");

                        BufferedOutputStream output = new BufferedOutputStream(
                                new FileOutputStream(file));
                        byte data[] = new byte[1024];

                        long total = 0;
                        int count;
                       while ((count = input.read(data)) != -1) {
                            total += count;
                            output.write(data, 0, count);
                        }

                        output.flush();

                        output.close();
                    } catch (IOException e) {
                        String logTag = "TEMPTAG";
                        Log.e(logTag, "Error while writing file!");
                        Log.e(logTag, e.toString());
                    }
                    confirmMail();
                    ((ShowContractActivity) activityOrigin).displayFromAsset("contract_" + idOperation + ".pdf");

                } else {
                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 400 && responseStatus < 500) {

                        if (responseStatus == 422) {

                            errorMessage = responseStatus + " - Error al generar contrato";

                        } else {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_400);

                        }

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                    }

                    GetContractDialog dialogoAlert;
                    dialogoAlert = new GetContractDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage, ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();

                t.printStackTrace();

                GetContractDialog dialogoAlert;
                dialogoAlert = new GetContractDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), activityOrigin.getString(R.string.msg_error_conexion), ApiConstants.ACTION_TRY_AGAIN_CANCEL);
                dialogoAlert.setCancelable(false);
                dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogoAlert.show();
            }
        });
    }

    public void confirmMail () {
        String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

        token = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.TOKEN_APP, "");
        String operationID = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.OPERATION_ID, "");

        MailVerificationOTPDTO validate = new MailVerificationOTPDTO(Long.parseLong(operationID));

        Log.v("confirmMail       : ", "token        :" + token);
        Log.v("confirmMail       : ", "endpoint     :" + endPoint);
        Log.v("confirmMail       : ", "{\"idOperation\":\""  + validate.getOperationID()+"\"}");

        BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

        Call<String> call = api.enrollmentMailVerificationOTP(token, validate);

        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressDialog.dismiss();

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (responseStatus == 200) {

                } else {

                    if (responseStatus >= 300 && responseStatus < 400) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                    } else if (responseStatus >= 400 && responseStatus < 500) {

                        String errorResponse = "";
                        errorResponse = activityOrigin.getString(R.string.message_ws_response_400);

                        if (responseStatus == 422){

                            if (response.body() != null)
                                errorResponse = errorResponse + response.body().toString();

                        }

                        errorMessage =  errorResponse;

                    } else if (responseStatus >= 500 && responseStatus < 600) {

                        errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressDialog.dismiss();

                Log.d(CLASS_NAME, activityOrigin.getString(R.string.message_ws_response_500));
                t.printStackTrace();
            }
        });
    }
}
