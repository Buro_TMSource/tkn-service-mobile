package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

public class FaceCompareNapiDTO {

    @SerializedName("credencial")
    private String credencial;

    @SerializedName("captura")
    private String captura;

    @SerializedName("tipo")
    private String tipo;

    @SerializedName("limiteInferior")
    private String limiteInferior;

    public String getCredencial() {
        return credencial;
    }

    public void setCredencial(String credencial) {
        this.credencial = credencial;
    }

    public String getCaptura() {
        return captura;
    }

    public void setCaptura(String captura) {
        this.captura = captura;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setLimiteInferior(String limiteInferior) {
        this.limiteInferior = limiteInferior;
    }
}
