package com.teknei.bid.asynctask;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.activities.DocumentScanActivity;
import com.teknei.bid.domain.NapiAddressDTO;
import com.teknei.bid.response.DomNapiResponse;
import com.teknei.bid.response.ResponseDocument;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.services.NapiEndPointServices;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import okhttp3.Credentials;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.MediaType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DocumentSend extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "DocumentSend";

    private String token;
    private String jsonS;
    private List<File> imageF;

    private Activity activityOrigin;
    private String errorMessage;

    private boolean hasConecction = false;
    private Integer responseStatus = 0;

    boolean resolution = false;

    private long endTime;

    private boolean isNapi = true;
    private NapiAddressDTO addressDTO;
    private File fileJson;

    public DocumentSend(Activity context, File fileJson, NapiAddressDTO addressDTO){
        this.addressDTO = addressDTO;
        this.activityOrigin = context;
        this.fileJson = fileJson;
        this.isNapi = true;

    }

    public DocumentSend(Activity context, String tokenOld, String jsonString, List<File> imageFile) {
        this.activityOrigin = context;
        this.token = tokenOld;
        this.jsonS = jsonString;
        this.imageF = imageFile;
        this.isNapi = false;
    }

    @Override
    protected void onPreExecute() {

        endTime = System.currentTimeMillis() + 1500;
        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);

        ConnectivityManager check = (ConnectivityManager) activityOrigin.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] info = check.getAllNetworkInfo();
        for (int i = 0; i < info.length; i++) {
            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                hasConecction = true;
            }
        }
    }

    @Override
    protected Void doInBackground(String... params) {
        if (hasConecction) {
            if(isNapi){
                callServicesNapi();
            }else {
                callServicesBid();
            }

        } else {
            ((BaseActivity) activityOrigin).showErrorConection();
        }
        return null;
    }

    private void callServicesNapi(){
        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());

        String endPoint = "https://www.napi.buroidentidad.com/";

        String stringAuthorization = Credentials.basic("buroidentidad","_9x3Ro.Xp");

        int    timeOut  = Integer.parseInt("120");

        Log.v("END POINT         :", "endpoint     :" + endPoint);

        NapiEndPointServices api = RetrofitSingleton.getInstance().build(endPoint, timeOut).create(NapiEndPointServices.class);

        Call<DomNapiResponse> call = api.servicesComprobanteDom("application/json",stringAuthorization, addressDTO);

        call.enqueue(new Callback<DomNapiResponse>() {
            @Override
            public void onResponse(Call<DomNapiResponse> call, Response<DomNapiResponse> response) {
                Log.d(CLASS_NAME, response.code()+" ");

                responseStatus = response.code();

                if(response.isSuccessful() && response.body()!=null){
                    DomNapiResponse napiResponse = new DomNapiResponse();
                    napiResponse = response.body();

                    if(napiResponse.getEstatus() ==null){


                        Log.d("Response Message", "json:" + new Gson().toJson(napiResponse));
                        String name = napiResponse.getNombre();
                        String apPat = "";
                        String apMat = "";
                        String street = napiResponse.getCalle();
                        String suburb = napiResponse.getColonia();
                        String zipCode = "";
                        String locality = napiResponse.getCiudad();
                        String state = "";

                        if(!napiResponse.getCalle().equals("")){
                            resolution = true;
                        }

                        if(resolution){
                            String jsonString = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                                    SharedPreferencesUtils.DOCUMENT_OPERATION, "{}");
                            try {
                                JSONObject jsonData = new JSONObject(jsonString);
                                jsonData.put("name"  , name);
                                jsonData.put("appat" , apPat);
                                jsonData.put("apmat" , apMat);

                                jsonData.put("street"  , street);
                                jsonData.put("suburb"  , suburb);
                                jsonData.put("zipCode" , zipCode);
                                jsonData.put("locality", locality);
                                jsonData.put("state"   , state);

                                SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.DOCUMENT_OPERATION, jsonData.toString());

                                Log.d("Response Message", "json:" + jsonData.toString());

                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }else {
                            errorMessage = responseStatus + " - " + "Error al obtener información de la imagen";
                            SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, errorMessage);

                        }




                    }else {
                        Log.d("Response Message", "complete:" + napiResponse.getMensaje());
                        Log.d("Response Message", "complete:" + response.code());
                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, activityOrigin.getString(R.string.msg_error_capture_address));

                    }
                } else {
                    Log.d("Response Message", "complete:" + response.message());
                    Log.d("Response Message", "complete:" + response.code());
                    SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, "Error en captura de comprobante.");

                }
            }

            @Override
            public void onFailure(Call<DomNapiResponse> call, Throwable t) {
                t.printStackTrace();
                SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, activityOrigin.getString(R.string.msg_error_conexion));

            }
        });

        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());
    }

    private void callServicesBid(){
        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());

        String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

        Log.v("token             :", "token        :" + token);
        Log.v("NUM FILES         :", "NUM FILES    :" + imageF.size());
        Log.v("json SEND NO File :", "json no file :" + jsonS);
        Log.v("END POINT         :", "endpoint     :" + endPoint);

        BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

        MultipartBody.Part jsonBody =
                MultipartBody.Part.createFormData("json", imageF.get(0).getName(),
                        RequestBody.create(MediaType.parse("application/json"), imageF.get(0)));

        MultipartBody.Part jsonDoc =
                MultipartBody.Part.createFormData("file", imageF.get(1).getName(),
                        RequestBody.create (MediaType.parse("image/jpg"), imageF.get(1)));

        Call<ResponseDocument> call = api.enrollmentAddressComprobanteParsed(token, jsonBody, jsonDoc);

        call.enqueue(new Callback<ResponseDocument>() {

            @Override
            public void onResponse(Call<ResponseDocument> call, Response<ResponseDocument> response) {

                Log.d(CLASS_NAME, response.code() + " ");

                responseStatus = response.code();

                if (response.isSuccessful() && response.body()!=null) {

                    ResponseDocument responseDoc = response.body();

                    if (responseDoc.isResultOK()) {


                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.SUCCESS_STORAGE_DOM,ApiConstants.SUCCESS_STORAGE_DOM);
                        ((DocumentScanActivity) activityOrigin).deleteFile();


                    } else {

                        Log.d("Response Message", "complete:" + response.message().toString());
                        Log.d("Response Message", "complete:" + response.code());

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, activityOrigin.getString(R.string.msg_error_capture_address));

                    }

                } else {

                    Log.d("Response Message", "complete:" + response.message().toString());
                    Log.d("Response Message", "complete:" + response.code());

                    SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, "Error en captura de comprobante.");

                }
            }

            @Override
            public void onFailure(Call<ResponseDocument> call, Throwable t) {
                t.printStackTrace();
                SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.ERROR_NAPI_OCR_DOM, activityOrigin.getString(R.string.msg_error_conexion));

            }
        });

        Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
        while (System.currentTimeMillis() < endTime) {
            //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
        }
        Log.i("Wait", "timer finish : " + System.currentTimeMillis());
    }
}
