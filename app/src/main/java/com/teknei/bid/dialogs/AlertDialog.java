package com.teknei.bid.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.utils.ApiConstants;

public class AlertDialog extends Dialog implements View.OnClickListener {

    private final String CLASS_NAME = "AlertDialog";

    Button okButton;
    Button cancelButton;
    Button retryButton;

    TextView txvTitle;
    TextView txvMessage;
    Activity activityOrigin;

    String titleIn;
    String menssageIn;
    int actionIn;
    int flowStep;
    private boolean esLogin = false;

    public AlertDialog(Activity context, String title, String message, int action) {
        super(context);
        activityOrigin = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /** Design the dialog in main.xml file */
        setContentView(R.layout.alert_dialog);

        this.titleIn = title;
        this.menssageIn = message;
        this.actionIn = action;

        txvTitle     = (TextView) findViewById(R.id.alert_title);
        txvMessage   = (TextView) findViewById(R.id.alert_message);
        okButton     = (Button) findViewById(R.id.ok_buttom);
        cancelButton = (Button) findViewById(R.id.cancel_buttom);
        retryButton  = (Button) findViewById(R.id.retry_buttom);

        okButton.setOnClickListener(this);
        retryButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        txvTitle.setText(titleIn);
        txvMessage.setText(menssageIn);


        switch (action) {

            case ApiConstants.ACTION_TRY_AGAIN:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_BLOCK_CANCEL_OPERATION:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_GFO_AGAIN_CANCEL:
            case ApiConstants.ACTION_TRY_AGAIN_CANCEL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_LOCAL :
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_BLOCK_CANCEL_OPERATION_LOCAL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CANCEL_LOCAL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE_DOC:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_LOG_OUT:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_CANCEL_OPERATION:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_LOG_OUT_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_CANCEL_OPERATION_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_GO_NEXT_LOCAL:
            case ApiConstants.ACTION_GO_NEXT:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                break;

            case ApiConstants.ACTION_CANCEL_CONTINUE:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    public AlertDialog(Activity context, String title, String message, int action, boolean esLogin) {
        super(context);
        activityOrigin = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /** Design the dialog in main.xml file */
        setContentView(R.layout.alert_dialog);

        this.titleIn = title;
        this.menssageIn = message;
        this.actionIn = action;
        this.esLogin = esLogin;

        txvTitle     = (TextView) findViewById(R.id.alert_title);
        txvMessage   = (TextView) findViewById(R.id.alert_message);
        okButton     = (Button) findViewById(R.id.ok_buttom);
        cancelButton = (Button) findViewById(R.id.cancel_buttom);
        retryButton  = (Button) findViewById(R.id.retry_buttom);

        okButton.setOnClickListener(this);
        retryButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        txvTitle.setText(titleIn);
        txvMessage.setText(menssageIn);


        switch (action) {

            case ApiConstants.ACTION_TRY_AGAIN:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_BLOCK_CANCEL_OPERATION:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_GFO_AGAIN_CANCEL:
            case ApiConstants.ACTION_TRY_AGAIN_CANCEL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_LOCAL :
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_BLOCK_CANCEL_OPERATION_LOCAL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CANCEL_LOCAL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE_DOC:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_LOG_OUT:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_CANCEL_OPERATION:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_LOG_OUT_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_CANCEL_OPERATION_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_GO_NEXT_LOCAL:
            case ApiConstants.ACTION_GO_NEXT:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                break;

            case ApiConstants.ACTION_CANCEL_CONTINUE:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    public AlertDialog(Activity context, String title, String message, int action, int flowStep) {
        super(context);
        activityOrigin = context;
        /** 'Window.FEATURE_NO_TITLE' - Used to hide the title */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /** Design the dialog in main.xml file */
        setContentView(R.layout.alert_dialog);

        this.titleIn = title;
        this.menssageIn = message;
        this.actionIn = action;
        this.flowStep = flowStep;

        txvTitle = (TextView) findViewById(R.id.alert_title);
        txvMessage = (TextView) findViewById(R.id.alert_message);
        okButton = (Button) findViewById(R.id.ok_buttom);
        cancelButton = (Button) findViewById(R.id.cancel_buttom);
        okButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        txvTitle.setText(titleIn);
        txvMessage.setText(menssageIn);


        switch (action) {

            case ApiConstants.ACTION_TRY_AGAIN:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_BLOCK_CANCEL_OPERATION:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CANCEL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_LOCAL :
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_BLOCK_CANCEL_OPERATION_LOCAL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CANCEL_LOCAL:
                okButton.setVisibility(View.GONE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_TRY_AGAIN_CONTINUE_DOC:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.VISIBLE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_LOG_OUT:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_CANCEL_OPERATION:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_LOG_OUT_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_CANCEL_OPERATION_LOCAL:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;

            case ApiConstants.ACTION_GO_STEP:
            case ApiConstants.ACTION_GO_NEXT_LOCAL:
            case ApiConstants.ACTION_GO_NEXT:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.GONE);
                break;

            case ApiConstants.ACTION_CANCEL_CONTINUE:
                okButton.setVisibility(View.VISIBLE);
                retryButton.setVisibility(View.GONE);
                cancelButton.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ok_buttom:
                dismiss();

                if (actionIn == ApiConstants.ACTION_LOG_OUT || actionIn == ApiConstants.ACTION_BLOCK_CANCEL_OPERATION) {
                    ((BaseActivity) activityOrigin).logOut();

                } else if (actionIn == ApiConstants.ACTION_LOG_OUT_LOCAL || actionIn == ApiConstants.ACTION_BLOCK_CANCEL_OPERATION_LOCAL) {

                } else if (actionIn == ApiConstants.ACTION_CANCEL_OPERATION) {
                    ((BaseActivity) activityOrigin).cancelOperation();

                } else if (actionIn == ApiConstants.ACTION_CANCEL_OPERATION_LOCAL) {

                } else if (actionIn == ApiConstants.ACTION_GO_NEXT || actionIn == ApiConstants.ACTION_CANCEL_CONTINUE) {
                        ((BaseActivity) activityOrigin).goNext();

                } else if (actionIn == ApiConstants.ACTION_GO_NEXT_LOCAL) {

                } else if (actionIn == ApiConstants.ACTION_GO_STEP) {

                } else if (actionIn == ApiConstants.ACTION_GO_STEP_LOCAL) {

                } else if (actionIn == ApiConstants.ACTION_TRY_AGAIN_CONTINUE) {
                    ((BaseActivity) activityOrigin).goNext();

                } else if (actionIn == ApiConstants.ACTION_TRY_AGAIN_CONTINUE_LOCAL) {
                }

                break;

            case R.id.retry_buttom:
                dismiss();

                if (actionIn == ApiConstants.ACTION_TRY_AGAIN || actionIn == ApiConstants.ACTION_TRY_AGAIN_CANCEL
                        || actionIn == ApiConstants.ACTION_TRY_AGAIN_CONTINUE) {
                        ((BaseActivity) activityOrigin).sendPetition();

                } else if (actionIn == ApiConstants.ACTION_TRY_AGAIN_LOCAL || actionIn == ApiConstants.ACTION_TRY_AGAIN_CANCEL_LOCAL
                        || actionIn == ApiConstants.ACTION_TRY_AGAIN_CONTINUE_LOCAL) {

                } else if (actionIn == ApiConstants.ACTION_TRY_AGAIN_CONTINUE_DOC) {
                    ((BaseActivity) activityOrigin).sendPetition();

                } else if (actionIn == ApiConstants.ACTION_TRY_GFO_AGAIN_CANCEL) {

                }

                break;

            case R.id.cancel_buttom:
                dismiss();
                break;
        }
    }
}
