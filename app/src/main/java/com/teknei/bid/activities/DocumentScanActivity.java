package com.teknei.bid.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.icarvision.icarsdk.IcarSDK_Settings;
import com.icarvision.icarsdk.newCapture.IcarCapture_Configuration;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;
import com.teknei.bid.R;
import com.teknei.bid.asynctask.DocumentSend;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.domain.NapiAddressDTO;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.PermissionsUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class DocumentScanActivity extends BaseActivity implements View.OnClickListener {

    final int REQUEST_CODE_ASK_PERMISSIONS                  = 3434;

    private static final int REQUEST_CODE = 99;

    Button    bContinue;
    Button    btnTakePic;
    ImageView iwDocument;

    private byte[] photoBuffer;

    File imageFile;
    File fileJson;

    List<File> fileList;

    private int typePerson;
    private String token;

    private String jsonString = "";
    private String operationID= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_scan);

        IcarSDK_Settings.ICAR_LICENSE_KEY_RESULT result = IcarSDK_Settings.getInstance().setIcarLicenseKey(getBaseContext(), ApiConstants.ICAR_KEY);

        if (result != IcarSDK_Settings.ICAR_LICENSE_KEY_RESULT.VALID_LICENSE)
        {
            String message = "";
            switch (result)
            {
                case INVALID_CONTEXT:
                    message = "Contexto no valido";
                    break;
                case INVALID_DATE:
                    message = "Fecha no valida";
                    break;
                case INVALID_PACKAGE_NAME:
                    message = "Nombre de paquete no valido";
                    break;
                case INVALID_FORMAT:
                    message = "Formato no valido";
                    break;
            }

            new android.app.AlertDialog.Builder(DocumentScanActivity.this)
                    .setTitle("Error con la licencia de ICAR")
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        typePerson  = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(DocumentScanActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, ""));
        operationID = SharedPreferencesUtils.readFromPreferencesString(DocumentScanActivity.this, SharedPreferencesUtils.OPERATION_ID, "");
        token       = SharedPreferencesUtils.readFromPreferencesString(DocumentScanActivity.this, SharedPreferencesUtils.TOKEN_APP, "");

        fileList = new ArrayList<File>();

        iwDocument  = (ImageView)   findViewById(R.id.imageViewDocument);
        bContinue   = (Button)      findViewById(R.id.b_continue_document_scan);
        btnTakePic  = (Button)      findViewById(R.id.docs_btn_take_picture);

        bContinue.setOnClickListener(this);
        btnTakePic.setOnClickListener(this);

        //Check Permissions For Android 6.0 up
        PermissionsUtils.checkPermissionReadWriteExternalStorage(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.docs_btn_take_picture:

                if (hasCameraPermission()) {
                    imageFile = null;
                    startScan(ScanConstants.OPEN_CAMERA);
                }

                break;

            case R.id.b_continue_document_scan:
                if (validatePictureTake()) {
                    sendPetition();
                }
                break;
        }
    }

    public boolean hasCameraPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int ihasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA);
            if (ihasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_ASK_PERMISSIONS);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public boolean validatePictureTake() {
        boolean bitMapTake = false;
        if (iwDocument.getDrawable() instanceof BitmapDrawable) {
            bitMapTake = true;
        } else {
            bitMapTake = false;
            Toast.makeText(this, "Debes tomar una fotografía del documento para continuar.", Toast.LENGTH_SHORT).show();
        }
        return bitMapTake;
    }

    protected void startScan(int preference) {
        Intent intent = new Intent(this, ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        startActivityForResult(intent, REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap;

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                getContentResolver().delete(uri, null, null);


                Double widthBitmap  = (bitmap.getWidth()*.50);

                Double heightBitmap = (bitmap.getHeight()*.50);

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,(bitmap.getWidth() - widthBitmap.intValue()), (bitmap.getHeight()-heightBitmap.intValue()),true);

                iwDocument.setImageBitmap(scaledBitmap);
                photoBuffer = bitmapToByteArray(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
            //Guarda nueva imagen del documento: comprobante de domicilio
            File f = new File(ApiConstants.FOLDER_IMGE + File.separator + "document_" + operationID + ".jpg");
            if (f.exists()) {
                f.delete();
                f = new File(ApiConstants.FOLDER_IMGE + File.separator + "document_" + operationID + ".jpg");
            }
            try {
                //write the bytes in file
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(photoBuffer);
                // remember close de FileOutput
                fo.close();
                imageFile = f;
            } catch (IOException e) {
                e.printStackTrace();
                imageFile = null;
            }
        }
    }

    public byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream); //Tipo de imagen
        return stream.toByteArray();
    }

    @Override
    public void sendPetition() {
        String documentOperation = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.DOCUMENT_OPERATION, "");


        if (documentOperation.equals("")) {
            fileList.clear();

            jsonString = buildJSON();

            fileList.add(fileJson);

            fileList.add(imageFile);

            NapiAddressDTO addressDTO = new NapiAddressDTO();
            addressDTO.setComprobante(com.teknei.bid.tools.Base64.encode(photoBuffer));

            new DocumentSend(DocumentScanActivity.this, fileJson, addressDTO).execute();

            goNext();
        }

    }

    @Override
    public void goNext() {
        String ByPasAPI = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.ByPasAPI, "");
        if(ByPasAPI =="SI")
            new DocumentSend(DocumentScanActivity.this, token, jsonString, fileList).execute();

        /*Enviar a 4F*/
        Intent i = new Intent(DocumentScanActivity.this, FingerIDKaralundiActivity.class);
        startActivity(i);
    }

    @Override
    public void deleteFile () {
        if (fileJson != null && fileJson.exists()) {
            fileJson.delete();
        }

        if (imageFile != null && imageFile.exists()) {
            imageFile.delete();
        }
    }

    public String buildJSON() {
        String idEnterprice = SharedPreferencesUtils.readFromPreferencesString(DocumentScanActivity.this, SharedPreferencesUtils.ID_ENTERPRICE, "default");
        String customerType = SharedPreferencesUtils.readFromPreferencesString(DocumentScanActivity.this, SharedPreferencesUtils.CUSTOMER_TYPE, "default");
        if(operationID=="")
        {

            operationID="1";
        }
        //Construimos el JSON
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("emprId", idEnterprice);
            jsonObject.put("customerType", customerType);
            jsonObject.put("operationId", Integer.valueOf(operationID));
            jsonObject.put("contentType", "image/jpeg");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Writer output = null;
            fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "document" + ".json");
            if (fileJson.exists()) {
                fileJson.delete();
                fileJson = new File(ApiConstants.FOLDER_IMGE + File.separator + "document" + ".json");
            }
            output = new BufferedWriter(new FileWriter(fileJson));
            output.write(jsonObject.toString());
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.WRITE_READ_EXTERNAL_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //Check AGAIN Permissions For Android 6.0 up
                    PermissionsUtils.checkPermissionWriteExternalStorage(DocumentScanActivity.this);
                }

                if (grantResults.length > 1
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //Check AGAIN Permissions For Android 6.0 up
                    PermissionsUtils.checkPermissionReadExternalStorage(DocumentScanActivity.this);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    //menu actions
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.i_close_operation_menu) {
            AlertDialog dialogoAlert;
            dialogoAlert = new AlertDialog(DocumentScanActivity.this, getString(R.string.message_close_operation_title), getString(R.string.message_close_operation_alert), ApiConstants.ACTION_CANCEL_OPERATION);
            dialogoAlert.setCancelable(false);
            dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogoAlert.show();
        }
        if (id == R.id.i_log_out_menu) {
            AlertDialog dialogoAlert;
            dialogoAlert = new AlertDialog(DocumentScanActivity.this, getString(R.string.message_title_logout), getString(R.string.message_message_logout), ApiConstants.ACTION_LOG_OUT);
            dialogoAlert.setCancelable(false);
            dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogoAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
    }

}
