package com.teknei.bid.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.teknei.bid.R;
import com.teknei.bid.utils.PermissionsUtils;


public class SelectTaskActivity extends BaseActivity implements View.OnClickListener{
    Button registerButton = null;
    Button matchingButton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_task);


        matchingButton = (Button) findViewById(R.id.task_select_id_type_matching);
        registerButton = (Button) findViewById(R.id.task_select_id_type_register);

        matchingButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);


        PermissionsUtils.checkPermissionCamera(this);
        PermissionsUtils.checkPermissionReadWriteExternalStorage(this);
    }

    @Override
    public void onClick(View view) {
        Intent i = null;

        if (view.getId() == registerButton.getId()) {
            i= new Intent(SelectTaskActivity.this, SelectActivity.class);
        }
        else if (view.getId() == matchingButton.getId()) {
            i= new Intent(SelectTaskActivity.this, UserKaralundiVerifiActivity.class);
        }
        startActivity(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermissionsUtils.CAMERA_REQUEST_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    SelectTaskActivity.this.onBackPressed();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case PermissionsUtils.WRITE_READ_EXTERNAL_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //Check AGAIN Permissions For Android 6.0 up
                    PermissionsUtils.checkPermissionWriteExternalStorage(SelectTaskActivity.this);
                }

                if (grantResults.length > 1
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    //Check AGAIN Permissions For Android 6.0 up
                    PermissionsUtils.checkPermissionReadExternalStorage(SelectTaskActivity.this);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onBackPressed() {
    }
}
