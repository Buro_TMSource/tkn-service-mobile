package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.teknei.bid.R;
import com.teknei.bid.tools.FrontCapture;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class EnrollmentResumeActivity  extends BaseActivity implements View.OnClickListener{

    private Button continueButton;
    private EditText txvName;
    private EditText txvApPat;
    private EditText txvApMat;
    private EditText txvCurp;
    private EditText txvOCR;
    private EditText txvValidity;
    private EditText txvStreet;
    private EditText txvSuburb;
    private EditText txvLocality;
    private TextView similitudNapi;
    private TextView tvIdStorageStatus;
    private TextView tvDomStorageStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** Design the dialog in main.xml file */
        setContentView(R.layout.enrollment_resume_dialog);

        InputFilter filterCURP = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.equals("")) { // for backspace
                    return source;
                }
                if (source.toString().matches("[A-Z0-9ÑÁÉÍÓÚ]+")) {
                    return source;
                }
                return "";
            }
        };

        InputFilter filterBasicAlfa = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.equals("")) { // for backspace
                    return source;
                }
                if (source.toString().matches("[A-Z .]+")) {
                    return source;
                }
                return "";
            }
        };

        InputFilter filterBasicNum = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.equals("")) { // for backspace
                    return source;
                }
                if (source.toString().matches("[0-9]+")) {
                    return source;
                }
                return "";
            }
        };

        String similitudPercent = FrontCapture.getInstance().getSimilitud();

        txvName     =  findViewById(R.id.tv_name_credential_enrollment_resume);
        txvApPat    =  findViewById(R.id.tv_appat_credential_enrollment_resume);
        txvApMat    =  findViewById(R.id.tv_apmat_credential_enrollment_resume);
        txvCurp     =  findViewById(R.id.tv_curp_credential_enrollment_resume);
        txvOCR      =  findViewById(R.id.tv_ocr_credential_enrollment_resume);
        txvValidity =  findViewById(R.id.tv_validity_credential_enrollment_resume);
        txvStreet   =  findViewById(R.id.tv_street_document_enrollment_resume);
        txvSuburb   =  findViewById(R.id.tv_suburb_document_enrollment_resume);
        txvLocality =  findViewById(R.id.tv_locality_document_enrollment_resume);

        similitudNapi = findViewById(R.id.coincidencia_napi_enrollment);
        tvIdStorageStatus = findViewById(R.id.tv_enrollment_id_status);
        tvDomStorageStatus = findViewById(R.id.tv_enrollment_dom_status);

        continueButton = findViewById(R.id.b_continue_credential_enrollment_resume);


        continueButton.setOnClickListener(this);

        String idStorageStatus = SharedPreferencesUtils.readFromPreferencesString(EnrollmentResumeActivity.this, SharedPreferencesUtils.SUCCESS_STORAGE_ID, "");
        String domStorageStatus = SharedPreferencesUtils.readFromPreferencesString(EnrollmentResumeActivity.this, SharedPreferencesUtils.SUCCESS_STORAGE_DOM, "");
        String jsonStringINE = SharedPreferencesUtils.readFromPreferencesString(EnrollmentResumeActivity.this, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, "{}");
        String jsonStringDOM = SharedPreferencesUtils.readFromPreferencesString(EnrollmentResumeActivity.this, SharedPreferencesUtils.DOCUMENT_OPERATION, "{}");

        String name = "";
        String apPat = "";
        String apMat = "";
        String curp = "";
        String ocr = "";
        String validity = "";
        String street   = "";
        String suburb   = "";
        String locality = "";


        try {
            JSONObject jsonObjectINE = new JSONObject(jsonStringINE);
            name = jsonObjectINE.optString("name");
            apPat = jsonObjectINE.optString("appat");
            apMat = jsonObjectINE.optString("apmat");
            curp = jsonObjectINE.optString("curp");
            ocr = jsonObjectINE.optString("ocr");
            validity = jsonObjectINE.optString("validity");

            JSONObject jsonObjectDOM = new JSONObject(jsonStringDOM);

            street   = jsonObjectDOM.optString("street");      // Calle
            suburb   = jsonObjectDOM.optString("suburb");      // Colonia
            locality = jsonObjectDOM.optString("locality");    // Localidad


        } catch (JSONException e) {
            e.printStackTrace();
        }

        txvName.setText(name);
        txvApPat.setText(apPat);
        txvApMat.setText(apMat);
        txvCurp.setText(curp);
        txvOCR.setText(ocr);
        txvValidity.setText(validity);

        txvStreet.setText(street);
        txvSuburb.setText(suburb);
        txvLocality.setText(locality);

        if(idStorageStatus.equals(ApiConstants.SUCCESS_STORAGE_ID))
            tvIdStorageStatus.setText(ApiConstants.document_storage_success);
        else
            tvIdStorageStatus.setText(ApiConstants.document_storage_failure);

        if(domStorageStatus.equals(ApiConstants.SUCCESS_STORAGE_DOM))
            tvDomStorageStatus.setText(ApiConstants.document_storage_success);
        else
            tvDomStorageStatus.setText(ApiConstants.document_storage_failure);


        String mensaje;
        if(similitudPercent != null){
            mensaje = similitudPercent + "%";
        }else {
            mensaje = "Similitud no encontrada.";
        }

        similitudNapi.setText(mensaje);

        txvName.setFilters(new InputFilter[]{filterBasicAlfa, new InputFilter.LengthFilter(30)});
        txvApPat.setFilters(new InputFilter[]{filterBasicAlfa, new InputFilter.LengthFilter(20)});
        txvApMat.setFilters(new InputFilter[]{filterBasicAlfa, new InputFilter.LengthFilter(20)});
        txvCurp.setFilters(new InputFilter[]{filterCURP, new InputFilter.LengthFilter(18)});
        txvOCR.setFilters(new InputFilter[]{filterBasicNum, new InputFilter.LengthFilter(20)});
        txvValidity.setFilters(new InputFilter[]{filterBasicNum, new InputFilter.LengthFilter(20)});
    }

    @Override
    public void onClick(View v) {


        try {

        if (v == continueButton){
           Intent i= new Intent(EnrollmentResumeActivity.this, JsonActivity.class);
           startActivity(i);

        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    }

    @Override
    public void sendPetition() {

    }

    @Override
    public void goNext() {

    }

    @Override
    public void onBackPressed() {
    }
}
