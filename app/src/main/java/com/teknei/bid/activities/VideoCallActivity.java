package com.teknei.bid.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.teknei.bid.R;

public  class VideoCallActivity  extends BaseActivity implements View.OnClickListener{

    private Button btnOpenVideoCall;
    private Button btn_next;
    String TAG = "[VideoCallActivity] ";

    private static  String url= "https://webdemo.buroidentidad.com/demo";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_videocall);

            try {

                btnOpenVideoCall = (Button)findViewById(R.id.btn_open_browser);
                btn_next = (Button)findViewById(R.id.btn_next);

                btnOpenVideoCall.setOnClickListener(this);
                btn_next.setOnClickListener(this);

            } catch (java.lang.Exception e){

                Log.d(TAG, "ERROR:"+e.getMessage());
            }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_open_browser:
                /*Abrir la URL con GoogleChrome*/
                Uri uri = Uri.parse("googlechrome://navigate?url=" + url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case R.id.btn_next:


                Intent i = new Intent(VideoCallActivity.this, ShowContractActivity.class);
                i.putExtra("tipoContrato","sinFirma");
                startActivity(i);
                break;


            default:
                Log.d(TAG, "DEBUG: onClick Default");

                break;
        }

    }
}