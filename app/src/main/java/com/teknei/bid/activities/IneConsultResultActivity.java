package com.teknei.bid.activities;


import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.teknei.bid.R;
import com.teknei.bid.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class IneConsultResultActivity  extends BaseActivity implements View.OnClickListener{

    private Button continueButton;
    private EditText txvName;
    private EditText txvApPat;
    private EditText txvApMat;
    private EditText txvOCR;

    private EditText etSimiScore;
    private TextView tvTimestamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** Design the dialog in main.xml file */
        setContentView(R.layout.consulta_ine_result);

        InputFilter filterBasicAlfa = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.equals("")) { // for backspace
                    return source;
                }
                if (source.toString().matches("[A-Z .]+")) {
                    return source;
                }
                return "";
            }
        };

        InputFilter filterBasicNum = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.equals("")) { // for backspace
                    return source;
                }
                if (source.toString().matches("[0-9]+")) {
                    return source;
                }
                return "";
            }
        };

        txvName     =  findViewById(R.id.et_name_ine_consult_result);
        txvApPat    =  findViewById(R.id.et_appat_ine_consult_result);
        txvApMat    =  findViewById(R.id.et_apmat_ine_consult_result);
        txvOCR      =  findViewById(R.id.et_ocr_ine_consult_result);

        txvName.setInputType(InputType.TYPE_NULL);
        txvApPat.setInputType(InputType.TYPE_NULL);
        txvApMat.setInputType(InputType.TYPE_NULL);
        txvOCR.setInputType(InputType.TYPE_NULL);


        etSimiScore = findViewById(R.id.et_simi_ine_consult_result);
        tvTimestamp  = findViewById(R.id.et_ine_consult_timestamp);
        tvTimestamp.setInputType(InputType.TYPE_NULL);
        continueButton = findViewById(R.id.b_continue_ine_consult_result);


        continueButton.setOnClickListener(this);

        String jsonStringINE = SharedPreferencesUtils.readFromPreferencesString(IneConsultResultActivity.this, SharedPreferencesUtils.JSON_CREDENTIALS_RESPONSE, "{}");

        String name = "";
        String apPat = "";
        String apMat = "";
        String ocr = "";


        try {
            JSONObject jsonObjectINE = new JSONObject(jsonStringINE);
            name = jsonObjectINE.optString("name");
            apPat = jsonObjectINE.optString("appat");
            apMat = jsonObjectINE.optString("apmat");
            ocr = jsonObjectINE.optString("ocr");



        } catch (JSONException e) {
            e.printStackTrace();
        }

        txvName.setText(name);
        txvApPat.setText(apPat);
        txvApMat.setText(apMat);
        txvOCR.setText(ocr);

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
        String date = sdf.format(calendar.getTime());


        tvTimestamp.setText(date);


        etSimiScore.setText("98%");

        txvName.setFilters(new InputFilter[]{filterBasicAlfa, new InputFilter.LengthFilter(30)});
        txvApPat.setFilters(new InputFilter[]{filterBasicAlfa, new InputFilter.LengthFilter(20)});
        txvApMat.setFilters(new InputFilter[]{filterBasicAlfa, new InputFilter.LengthFilter(20)});
        txvOCR.setFilters(new InputFilter[]{filterBasicNum, new InputFilter.LengthFilter(20)});
    }

    @Override
    public void onClick(View v) {


        try {

        if (v == continueButton){
           Intent i= new Intent(IneConsultResultActivity.this, kycActivity.class);
           startActivity(i);

        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    }

    @Override
    public void sendPetition() {

    }

    @Override
    public void goNext() {

    }

    @Override
    public void onBackPressed() {
    }
}
