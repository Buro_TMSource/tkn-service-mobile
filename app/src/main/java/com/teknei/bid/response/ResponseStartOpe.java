package com.teknei.bid.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rgarciav on 07/11/2017.
 */

public class ResponseStartOpe extends ResponseServicesBID {

    @SerializedName("operationId")
    private Long operationId;

    @SerializedName("obs")
    private String obs;

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }
}
