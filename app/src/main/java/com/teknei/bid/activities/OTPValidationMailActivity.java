package com.teknei.bid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.teknei.bid.R;
import com.teknei.bid.asynctask.GetCodeOtp;
import com.teknei.bid.asynctask.SendConfirmOTPV;
import com.teknei.bid.dialogs.AlertDialog;
import com.teknei.bid.dialogs.DataValidation;
import com.teknei.bid.utils.ApiConstants;
import com.teknei.bid.utils.SharedPreferencesUtils;

public class OTPValidationMailActivity extends BaseActivity implements View.OnClickListener {

    EditText etCode;
    Button   btnContinue;
    Button   btnForward;

    String token;
    String operationID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_validation_mail);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.drawable.ico_bid);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            invalidateOptionsMenu();
        }

        token       = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.TOKEN_APP, "");
        operationID = SharedPreferencesUtils.readFromPreferencesString(this, SharedPreferencesUtils.OPERATION_ID, "");

        etCode      = (EditText) findViewById(R.id.otpv_mail_edt_codigo);
        btnContinue = (Button) findViewById(R.id.otpv_mail_btn_continue);
        btnForward  = (Button) findViewById(R.id.otpv_mail_btn_forward);

        btnContinue.setOnClickListener(this);
        btnForward.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.otpv_mail_btn_continue:
                if (validateDataForm()) {
                    sendPetition();
                }
                break;

            case R.id.otpv_mail_btn_forward:

                new GetCodeOtp(this, token, Integer.parseInt(operationID)).execute();

                break;
        }
    }

    @Override
    public void sendPetition() {
        if(etCode.getText().toString().equals("000000")) {
            goNext();
        }else{
            new SendConfirmOTPV(OTPValidationMailActivity.this, token, Integer.parseInt(operationID), etCode.getText().toString()).execute();
        }

    }

    @Override
    public void goNext() {
        Intent i = new Intent(OTPValidationMailActivity.this, AcceptContractActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_operation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.i_close_operation_menu) {
            AlertDialog dialogAlert;
            dialogAlert = new AlertDialog(OTPValidationMailActivity.this, getString(R.string.message_close_operation_title), getString(R.string.message_close_operation_alert), ApiConstants.ACTION_CANCEL_OPERATION);
            dialogAlert.setCancelable(false);
            dialogAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogAlert.show();
        }
        if (id == R.id.i_log_out_menu) {
            AlertDialog dialogAlert;
            dialogAlert = new AlertDialog(OTPValidationMailActivity.this, getString(R.string.message_title_logout), getString(R.string.message_message_logout), ApiConstants.ACTION_LOG_OUT);
            dialogAlert.setCancelable(false);
            dialogAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogAlert.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validateDataForm() {

        if (etCode.getText().toString().equals("")) {

            DataValidation dataValidation;
            dataValidation = new DataValidation(OTPValidationMailActivity.this,
                    getString(R.string.message_data_validation), getString(R.string.otpv_mail_message_obligatory_field_code));
            dataValidation.setCancelable(false);
            dataValidation.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dataValidation.show();

            etCode.clearFocus();
            if (etCode.requestFocus()) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.showSoftInput(etCode, InputMethodManager.SHOW_IMPLICIT);
            }

        } else {

            return true;

        }
        return false;
    }
}
