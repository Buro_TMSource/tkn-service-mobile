package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.activities.SigningContractKaralundiActivity;
import com.teknei.bid.activities.UserKaralundiVerifiActivity;
import com.teknei.bid.dialogs.MessageDialog;
import com.teknei.bid.domain.DetailSearchDTO;
import com.teknei.bid.domain.FieldSearchDetailDTO;
import com.teknei.bid.services.BIDEndPointServices;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidateCURP extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "ValidateCURP";

    private Activity activityOrigin;
    private String   errorMessage;
    private Integer  responseStatus = 0;

    private long endTime;

    private DetailSearchDTO responseDTO;
    private FieldSearchDetailDTO valueDTO;
    private boolean firmar = false;
    private boolean login = false;

    public ValidateCURP(Activity context, FieldSearchDetailDTO valueDTO) {
        this.activityOrigin = context;
        this.valueDTO       = valueDTO;
    }

    public ValidateCURP(Activity context, FieldSearchDetailDTO valueDTO, boolean firmar) {
        this.activityOrigin = context;
        this.valueDTO       = valueDTO;
        if (firmar)
            this.firmar = true;
        else
            this.login = true;

    }

    @Override
    protected void onPreExecute() {
        endTime = System.currentTimeMillis() + 1000;
        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: "   + endTime);
    }

    @Override
    protected Void doInBackground(String... params) {
        if (PhoneSimUtils.isNetDisponible(activityOrigin)) {
            Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
            while (System.currentTimeMillis() < endTime) {
                //espera hasta que pasen los 2 segundos en caso de que halla terminado muy rapido el hilo
            }
            Log.i("Wait", "timer finish : " + System.currentTimeMillis());

            String endPoint = SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.URL_TEKNEI, activityOrigin.getString(R.string.default_url_teknei));

            int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

            Log.v(CLASS_NAME, "endpoint     :" + endPoint);
            Log.v(CLASS_NAME, "Curp         :" + valueDTO.getCurp());

            BIDEndPointServices api = RetrofitSingleton.getInstance().build(endPoint,timeOut).create(BIDEndPointServices.class);

            Call<DetailSearchDTO> call = api.enrollmentClientDetail(valueDTO);

            call.enqueue(new Callback<DetailSearchDTO>() {

                @Override
                public void onResponse(Call<DetailSearchDTO> call, Response<DetailSearchDTO> response) {

                    Log.d(CLASS_NAME, response.code() + " ");

                    responseStatus = response.code();

                    if (responseStatus >= 200 && responseStatus < 300) {
                        try {
                            responseDTO = response.body();

                            SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.OPERATION_ID, responseDTO.getId()+"");
                            SharedPreferencesUtils.saveToPreferencesString(activityOrigin, SharedPreferencesUtils.CLIENT_CURP, responseDTO.getCurp()+"");

                            if(firmar)
                                ((SigningContractKaralundiActivity) activityOrigin).showPanelTakeFinger();
                            else
                                ((UserKaralundiVerifiActivity) activityOrigin).showPanelTakeFinger();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        if (responseStatus >= 300 && responseStatus < 400) {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_300);

                        } else if (responseStatus >= 400 && responseStatus < 500) {

                            String errorResponse = activityOrigin.getString(R.string.message_ws_response_400);

                            if (responseStatus == 404) {

                                errorResponse = activityOrigin.getString(R.string.msg_error_form_val_data);

                            } if (responseStatus == 422) {

                                errorResponse   =  response.message();

                            }

                            errorMessage =  errorResponse;

                        } else if (responseStatus >= 500 && responseStatus < 600) {

                            errorMessage =  activityOrigin.getString(R.string.message_ws_response_500);

                        }

                        MessageDialog dialogoAlert;
                        dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();
                    }
                }

                @Override
                public void onFailure(Call<DetailSearchDTO> call, Throwable t) {

                    MessageDialog dialogoAlert;
                    dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice),
                            activityOrigin.getString(R.string.msg_error_conexion));
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();

                    t.printStackTrace();
                }
            });

        } else {
            ((BaseActivity) activityOrigin).showErrorConection();
        }
        return null;
    }
}