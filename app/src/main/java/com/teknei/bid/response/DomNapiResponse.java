package com.teknei.bid.response;

import com.google.gson.annotations.SerializedName;

public class DomNapiResponse {

    @SerializedName("estatus")
    private String estatus;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("nombre")
    private String nombre;

    @SerializedName("calle")
    private String calle;

    @SerializedName("referencia")
    private String referencia;

    @SerializedName("colonia")
    private String colonia;

    @SerializedName("ciudad")
    private String ciudad;

    @SerializedName("fecha")
    private String fecha;

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
