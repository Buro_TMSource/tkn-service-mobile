package com.teknei.bid.asynctask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.teknei.bid.R;
import com.teknei.bid.activities.BaseActivity;
import com.teknei.bid.dialogs.MessageDialog;
import com.teknei.bid.dialogs.ProgressDialog;
import com.teknei.bid.response.OAuthAccessToken;
import com.teknei.bid.response.ResponseDetailMe;
import com.teknei.bid.services.OAuthApi;
import com.teknei.bid.utils.PhoneSimUtils;
import com.teknei.bid.utils.SharedPreferencesUtils;
import com.teknei.bid.ws.RetrofitSingleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogIn extends AsyncTask<String, Void, Void> {

    private final String CLASS_NAME = "LoginIn";

    private long   endTime;
    private String token;
    private String authorization;

    private String userToCheck;
    private String passToCheck;

    private OAuthApi         api;
    private OAuthAccessToken accessToken;

    private Activity        activityOrigin;
    private ProgressDialog  progressDialog;

    public LogIn(Activity context, String userString,String passString, String tokenOld, String autho) {
        this.activityOrigin = context;
        this.userToCheck    = userString;
        this.passToCheck    = passString;
        this.token          = tokenOld;
        this.authorization  = autho;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog( activityOrigin, activityOrigin.getString(R.string.get_user_starting));
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.show();

        endTime = System.currentTimeMillis() + 1000;

        Log.i("Wait", "Timer Start: " + System.currentTimeMillis());
        Log.i("Wait", "Timer END: " + endTime);
    }

    @Override
    protected Void doInBackground(String... params) {
       if (PhoneSimUtils.isNetDisponible(activityOrigin)) {


            Log.i("Wait", "timer after DO: " + System.currentTimeMillis());
            while (System.currentTimeMillis() < endTime) {
                //espera hasta que pasen 1 segundo en caso de que halla terminado muy rapido el hilo
            }
            Log.i("Wait", "timer finish : " + System.currentTimeMillis());

            String urlAuthAccess   = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.URL_AUTHACCESS, activityOrigin.getString(R.string.default_url_oauthaccess));

            int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                    SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

            Log.v("END POINT         :", "endpoint     :" + urlAuthAccess);

            api = RetrofitSingleton.getInstance().build(urlAuthAccess,timeOut).create(OAuthApi.class);

            Call<OAuthAccessToken> call = api.getAccessTokenByPassword(userToCheck, passToCheck);

            call.enqueue(new Callback<OAuthAccessToken>() {

                @Override
                public void onResponse(Call<OAuthAccessToken> call, Response<OAuthAccessToken> response) {

                    if (response.isSuccessful()) {

                        accessToken = response.body();

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin,SharedPreferencesUtils.TOKEN_APP,"bearer "+accessToken.getAccessToken());
                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin,SharedPreferencesUtils.USERNAME,userToCheck);

                        obteinDataCustomer();

                    } else {

                        progressDialog.dismiss();

                        String errorMessage = activityOrigin.getString(R.string.msg_error_access);

                        MessageDialog dialogoAlert;
                        dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();
                    }
                }

                @Override
                public void onFailure(Call<OAuthAccessToken> call, Throwable t) {

                    progressDialog.dismiss();

                    t.printStackTrace();

                    String errorMessage = activityOrigin.getString(R.string.msg_error_conexion);

                    MessageDialog dialogoAlert;
                    dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();
                }
            });

        } else {
            progressDialog.dismiss();

            ((BaseActivity) activityOrigin).showErrorConection();
        }

        return null;
    }

    public void obteinDataCustomer () {
        String urlAuthAccess   = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.URL_AUTHACCESS, activityOrigin.getString(R.string.default_url_oauthaccess));
        String token           = SharedPreferencesUtils.readFromPreferencesString(activityOrigin, SharedPreferencesUtils.TOKEN_APP, "");

        int    timeOut = Integer.parseInt(SharedPreferencesUtils.readFromPreferencesString(activityOrigin,
                SharedPreferencesUtils.TIMEOUT_SERVICES, activityOrigin.getString(R.string.default_timeout)));

        Log.v("END POINT         :", "endpoint     :" + urlAuthAccess);

        api = RetrofitSingleton.getInstance().build(urlAuthAccess,timeOut).create(OAuthApi.class);

        Call<ResponseDetailMe> call = api.getOwnerInfo(token);

        call.enqueue(new Callback<ResponseDetailMe>() {

            @Override
            public void onResponse(Call<ResponseDetailMe> call, Response<ResponseDetailMe> response) {
                progressDialog.dismiss();

                if (response.isSuccessful() && response.body() != null) {

                    ResponseDetailMe responseCustomer = response.body();

                    if (responseCustomer.isAuthenticated()) {

                        Log.d(CLASS_NAME,responseCustomer.getClientId()+"");

                        String idClient =  responseCustomer.getClientId()+"";

                        SharedPreferencesUtils.saveToPreferencesString(activityOrigin,SharedPreferencesUtils.ID_CLIENT, idClient);

                        ((BaseActivity) activityOrigin).goNext();

                    } else {
                        String errorMessage = activityOrigin.getString(R.string.msg_error_access_data);

                        MessageDialog dialogoAlert;
                        dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                        dialogoAlert.setCancelable(false);
                        dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialogoAlert.show();
                    }

                } else {
                    String errorMessage = activityOrigin.getString(R.string.msg_error_access_data);

                    MessageDialog dialogoAlert;
                    dialogoAlert = new MessageDialog(activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                    dialogoAlert.setCancelable(false);
                    dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialogoAlert.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseDetailMe> call, Throwable t) {
                progressDialog.dismiss();

                t.printStackTrace();

                String errorMessage = activityOrigin.getString(R.string.msg_error_conexion);

                MessageDialog dialogoAlert;
                dialogoAlert = new MessageDialog (activityOrigin, activityOrigin.getString(R.string.message_ws_notice), errorMessage);
                dialogoAlert.setCancelable(false);
                dialogoAlert.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialogoAlert.show();
            }
        });
    }
}
