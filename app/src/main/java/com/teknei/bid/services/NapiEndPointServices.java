package com.teknei.bid.services;

import com.teknei.bid.domain.FaceCompareNapiDTO;
import com.teknei.bid.domain.NapiAddressDTO;
import com.teknei.bid.domain.SendIdOcrDTO;
import com.teknei.bid.response.DomNapiResponse;
import com.teknei.bid.response.NapiFacialResponse;
import com.teknei.bid.response.OcrNapiResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface NapiEndPointServices {

    @POST("ocr/obtener_datos")
    Call<OcrNapiResponse> servicesOCRgetData (@Header("Content-Type")   String content_type,
                                              @Header("Authorization") String authorization,
                                              @Body SendIdOcrDTO jsonFile);

    @POST("ocr/comprobante_domicilio")
    Call<DomNapiResponse> servicesComprobanteDom (@Header("Content-Type") String content_type,
                                                  @Header("Authorization") String authorization,
                                                  @Body NapiAddressDTO jsonFile);

    @POST("antifraude/reconocimiento_facial")
    Call<NapiFacialResponse> compareFacial (@Header("Content-Type") String content_type,
                                            @Header("Authorization") String authorization,
                                            @Body FaceCompareNapiDTO jsonFile);
}
