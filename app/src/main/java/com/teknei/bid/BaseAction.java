package com.teknei.bid;

import com.teknei.bid.domain.DetailSearchDTO;

import java.util.List;

/**
 * Created by Desarrollo on 10/07/2017.
 */

public interface BaseAction {
    void logOut();
    void goNext();
    void sendPetition();
    void cancelOperation();
    void deleteFile ();
    void showObtainedData   (DetailSearchDTO value);
    void showErrorConection ();
}
