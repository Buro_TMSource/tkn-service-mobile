package com.teknei.bid.utils;

//---Imports:------------------

import com.icarvision.icarsdk.idCloudConnection.IdCloud_LocalImages;
//-----------------------------


public class AppDataCaptured {

    private static AppDataCaptured ourInstance = new AppDataCaptured();

    public IdCloud_LocalImages localImages;

    public static AppDataCaptured getInstance() {
        return ourInstance;
    }

    private AppDataCaptured()
    {
        localImages         = new IdCloud_LocalImages();
    }
}



