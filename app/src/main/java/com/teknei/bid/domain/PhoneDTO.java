package com.teknei.bid.domain;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rgarciav on 20/03/2018.
 */

public class PhoneDTO {

    @SerializedName("tel")
    private String tel;

    @SerializedName("type")
    private Integer type;

    public String getTel() {
        return tel;
    }

    public PhoneDTO(Integer type, String tel) {
        this.tel = tel;
        this.type = type;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
